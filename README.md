This is a sample project for learning purposes. The project was built using TDD, MVVM, kotlin, dagger and retrofit.

![alt text](https://i.imgur.com/0gdcuo5.png)
![alt text](https://i.imgur.com/XDwqfT1.png)
![alt text](https://i.imgur.com/beEb0YO.png)
![alt text](https://i.imgur.com/AVb0Di7.png)