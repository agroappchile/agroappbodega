package cl.agroapp.agroappbodega

import android.app.Activity
import android.view.View
import android.widget.Checkable
import android.widget.SearchView
import android.widget.Spinner
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.matcher.BoundedMatcher
import androidx.test.espresso.matcher.ViewMatchers.isAssignableFrom
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.platform.app.InstrumentationRegistry.getInstrumentation
import androidx.test.runner.lifecycle.ActivityLifecycleMonitorRegistry
import androidx.test.runner.lifecycle.Stage
import org.hamcrest.*
import org.hamcrest.Matchers.allOf

class MatcherUtils {

    companion object {

        fun setChecked(checked: Boolean): ViewAction {
            return object : ViewAction {
                override fun getConstraints(): BaseMatcher<View> {
                    return object : BaseMatcher<View>() {
                        override fun matches(item: Any): Boolean {
                            return Matchers.isA(Checkable::class.java).matches(item)
                        }
                        override fun describeMismatch(item: Any, mismatchDescription: Description) {}
                        override fun describeTo(description: Description) {}
                    }
                }
                override fun getDescription(): String? {
                    return null
                }
                override fun perform(uiController: UiController, view: View) {
                    val checkableView = view as Checkable
                    checkableView.isChecked = checked
                }
            }
        }

        fun withIndex(matcher: Matcher<View>, index: Int): Matcher<View> {
            return object : TypeSafeMatcher<View>() {
                var currentIndex = 0
                override fun describeTo(description: Description) {
                    description.appendText("with index: ")
                    description.appendValue(index)
                    matcher.describeTo(description)
                }

                override fun matchesSafely(view: View): Boolean {
                    return matcher.matches(view) && currentIndex++ == index
                }
            }
        }

        fun withRecyclerViewSizeGreaterThan(size: Int): Matcher<View> {
            return object : TypeSafeMatcher<View>() {
                override fun matchesSafely(view: View): Boolean {
                    return (view as RecyclerView).adapter?.let {
                        it.itemCount > size
                    } ?: throw NullPointerException("RecyclerView Adapter is null")
                }
                override fun describeTo(description: Description) {
                    description.appendText("RecyclerView size should be greater than $size")
                }
            }
        }

        fun withRecyclerViewSize(size: Int): Matcher<View> {
            return object : TypeSafeMatcher<View>() {
                override fun matchesSafely(view: View): Boolean {
                    return (view as RecyclerView).adapter?.let {
                        it.itemCount == size
                    } ?: throw NullPointerException("RecyclerView Adapter is null")
                }
                override fun describeTo(description: Description) {
                    description.appendText("RecyclerView size should be equal to $size")
                }
            }
        }

        fun withSpinnerSizeGreaterThan(size: Int): Matcher<View> {
            return object : TypeSafeMatcher<View>() {
                override fun matchesSafely(view: View): Boolean {
                    return (view as Spinner).adapter.count > size
                }
                override fun describeTo(description: Description) {
                    description.appendText("Spinner size should be greater than $size")
                }
            }
        }

        fun withSpinnerSize(size: Int): Matcher<View> {
            return object : TypeSafeMatcher<View>() {
                override fun matchesSafely(view: View): Boolean {
                    return (view as Spinner).adapter.count == size
                }
                override fun describeTo(description: Description) {
                    description.appendText("Spinner size should be equal to $size")
                }
            }
        }

        fun withTextLengthGreaterThan(length: Int): Matcher<View> {
            return object : TypeSafeMatcher<View>() {
                override fun matchesSafely(view: View): Boolean {
                    return (view as TextView).length() > length
                }
                override fun describeTo(description: Description) {
                    description.appendText("TextView length should be greater than $length")
                }
            }
        }

        fun getText(matcher: Matcher<View>): String {
            var stringHolder = ""
            onView(matcher).perform(object : ViewAction {
                override fun getConstraints(): Matcher<View> {
                    return isAssignableFrom(TextView::class.java)
                }
                override fun getDescription(): String {
                    return "getting text from a TextView"
                }
                override fun perform(uiController: UiController, view: View) {
                    val tv = view as TextView //Save, because of check in getConstraints()
                    stringHolder = tv.text.toString()
                }
            })
            return stringHolder
        }

        fun hasItem(matcher: Matcher<View>): Matcher<View> {
            return object : BoundedMatcher<View, RecyclerView>(RecyclerView::class.java) {
                override fun describeTo(description: Description) {
                    description.appendText("has item: ")
                    matcher.describeTo(description)
                }

                override fun matchesSafely(view: RecyclerView): Boolean {
                    val adapter = view.adapter ?: throw NullPointerException("adapter is null")
                    for (position in 0 until adapter.itemCount) {
                        val type = adapter.getItemViewType(position)
                        val holder = adapter.createViewHolder(view, type)
                        adapter.onBindViewHolder(holder, position)
                        if (matcher.matches(holder.itemView)) {
                            return true
                        }
                    }
                    return false
                }
            }
        }

        fun allItems(matcher: Matcher<View>): Matcher<View> {
            return object : BoundedMatcher<View, RecyclerView>(RecyclerView::class.java) {
                override fun describeTo(description: Description) {
                    description.appendText("has item: ")
                    matcher.describeTo(description)
                }

                override fun matchesSafely(view: RecyclerView): Boolean {
                    val adapter = view.adapter ?: throw NullPointerException("adapter is null")
                    for (position in 0 until adapter.itemCount) {
                        val type = adapter.getItemViewType(position)
                        val holder = adapter.createViewHolder(view, type)
                        adapter.onBindViewHolder(holder, position)
                        if (!matcher.matches(holder.itemView)) {
                            return false
                        }
                    }
                    return true
                }
            }
        }

        fun typeSearchViewText(text: String): ViewAction {
            return object : ViewAction {
                override fun getDescription(): String {
                    return "Change view text"
                }

                override fun getConstraints(): Matcher<View> {
                    return allOf(isDisplayed(), isAssignableFrom(SearchView::class.java))
                }

                override fun perform(uiController: UiController?, view: View?) {
                    (view as SearchView).setQuery(text, false)
                }
            }
        }

        fun withSearchViewText(text: String): Matcher<View> {
            return object : TypeSafeMatcher<View>() {
                override fun matchesSafely(view: View): Boolean {
                    return (view as SearchView).query.toString() == text
                }
                override fun describeTo(description: Description) {
                    description.appendText("SearchView text should be $text")
                }
            }
        }

        fun getCurrentActivity(): Activity? {
            var currentActivity: Activity? = null
            getInstrumentation().runOnMainSync { run { currentActivity =
                ActivityLifecycleMonitorRegistry.getInstance()
                    .getActivitiesInStage(Stage.RESUMED).elementAtOrNull(0) } }
            return currentActivity
        }

    }

}