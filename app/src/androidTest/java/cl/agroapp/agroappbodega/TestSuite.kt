package cl.agroapp.agroappbodega

import cl.agroapp.agroappbodega.ui.category.CategoryFragmentTest
import cl.agroapp.agroappbodega.ui.entry.EntryActivityTest
import cl.agroapp.agroappbodega.ui.login.LoginActivityTest
import cl.agroapp.agroappbodega.ui.material.MaterialActivityTest
import cl.agroapp.agroappbodega.ui.menu.MenuActivityTest
import cl.agroapp.agroappbodega.ui.movement.MovementFragmentTest
import cl.agroapp.agroappbodega.ui.warehouse.WarehouseFragmentTest
import org.junit.runner.RunWith
import org.junit.runners.Suite

@RunWith(Suite::class)
@Suite.SuiteClasses(
    CategoryFragmentTest::class,
    LoginActivityTest::class,
    MaterialActivityTest::class,
    MenuActivityTest::class,
    MovementFragmentTest::class,
    WarehouseFragmentTest::class,
    EntryActivityTest::class
)
class TestSuite