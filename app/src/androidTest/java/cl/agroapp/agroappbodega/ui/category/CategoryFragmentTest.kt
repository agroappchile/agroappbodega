package cl.agroapp.agroappbodega.ui.category

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.platform.app.InstrumentationRegistry
import cl.agroapp.agroappbodega.MatcherUtils.Companion.getCurrentActivity
import cl.agroapp.agroappbodega.MatcherUtils.Companion.getText
import cl.agroapp.agroappbodega.MatcherUtils.Companion.hasItem
import cl.agroapp.agroappbodega.MatcherUtils.Companion.withIndex
import cl.agroapp.agroappbodega.MatcherUtils.Companion.withRecyclerViewSizeGreaterThan
import cl.agroapp.agroappbodega.MatcherUtils.Companion.withTextLengthGreaterThan
import cl.agroapp.agroappbodega.R
import cl.agroapp.agroappbodega.ui.menu.MenuActivity
import cl.agroapp.agroappbodega.utils.Constants
import kotlinx.android.synthetic.main.fragment_category.*
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.util.*

class CategoryFragmentTest {

    @Rule
    @JvmField
    val menuScenarioRule = ActivityScenarioRule(MenuActivity::class.java)

    private val context = InstrumentationRegistry.getInstrumentation().targetContext
    private val position = 0

    @Before
    fun setUp() {
        onView(withId(R.id.navigation_categoria)).perform(click())
    }

    @Test
    fun fragmentStart_loadCategories() {
        // Assert
        onView(withId(R.id.category_rv)).check(matches(withRecyclerViewSizeGreaterThan(0)))
    }

    @Test
    fun categoriesLoaded_categoryNameMatchesTextViewInAdapter() {
        // Arrange
        lateinit var categoryName: String
        menuScenarioRule.scenario.onActivity {
            categoryName = (it.category_rv.adapter as CategoryAdapter).categoryList[position].name
        }

        // Assert
        onView(withIndex(withId(R.id.category_name_tv), position))
            .check(matches(withText(categoryName)))
    }

    @Test
    fun categoriesLoaded_totalMaterialsMatchesTextViewInAdapter() {
        // Arrange
        var totalMaterials = 0
        menuScenarioRule.scenario.onActivity {
            totalMaterials = (it.category_rv.adapter as CategoryAdapter).categoryList[position]
                .materialList.size
        }

        // Assert
        onView(withIndex(withId(R.id.total_material_tv), position)).check(matches(withText(
            context.getString(R.string.total_materials, totalMaterials))))
    }

    @Test
    fun addCategoryClicked_showInputDialog() {
        // Act
        onView(withId(R.id.add_category_fab)).perform(click())

        // Assert
        onView(withId(R.id.title_tv)).check(matches(withText(R.string.add)))
        onView(withId(R.id.message_tv)).check(matches(withText(R.string.enter_category_name)))
    }

    @Test
    fun addNewCategory_showSuccessfulDialog() {
        // Arrange
        onView(withId(R.id.add_category_fab)).perform(click())
        onView(withId(R.id.value_et)).perform(
            typeText("New category from UI test ${Date().time}"),
            closeSoftKeyboard())

        // Act
        onView(withId(R.id.ok_btn)).perform(click())

        // Assert
        onView(withId(R.id.title_tv)).check(matches(withText(R.string.information)))
        onView(withId(R.id.message_tv)).check(matches(withText(R.string.category_created)))
    }

    @Test
    fun addNewCategory_categoryIsAdded() {
        // Arrange
        val categoryName = "New category from UI test ${Date().time}"
        onView(withId(R.id.add_category_fab)).perform(click())
        onView(withId(R.id.value_et)).perform(typeText(categoryName), closeSoftKeyboard())

        // Act
        onView(withId(R.id.ok_btn)).perform(click())
        onView(withId(R.id.ok_btn)).perform(click())

        // Assert
        onView(withId(R.id.category_rv)).check(matches(hasItem(hasDescendant(
            withText(categoryName)))))
    }

    @Test
    fun addExistingCategory_showErrorDialog() {
        // Arrange
        onView(withId(R.id.add_category_fab)).perform(click())
        onView(withId(R.id.value_et)).perform(
            typeText(Constants.EXISTING_CATEGORY_NAME),
            closeSoftKeyboard())

        // Act
        onView(withId(R.id.ok_btn)).perform(click())

        // Assert
        onView(withId(R.id.title_tv)).check(matches(withText(R.string.error)))
        onView(withId(R.id.message_tv)).check(matches(withTextLengthGreaterThan(0)))
    }

    @Test
    fun editCategoryClicked_showInputDialog() {
        // Arrange
        onView(withId(R.id.category_rv)).perform(
            actionOnItemAtPosition<RecyclerView.ViewHolder>(position, longClick()))

        // Act
        onView(withText(R.string.edit)).perform(click())

        // Assert
        onView(withId(R.id.title_tv)).check(matches(withText(R.string.edit)))
        onView(withId(R.id.message_tv)).check(matches(withText(R.string.enter_category_name)))
    }

    @Test
    fun editCategoryClicked_fillsInputDialogWithCurrentCategoryName() {
        // Arrange
        val categoryName = getText(withIndex(withId(R.id.category_name_tv), position))
        onView(withId(R.id.category_rv)).perform(
            actionOnItemAtPosition<RecyclerView.ViewHolder>(position, longClick()))

        // Act
        onView(withText(R.string.edit)).perform(click())

        // Assert
        onView(withId(R.id.value_et)).check(matches(withText(categoryName)))
    }

    @Test
    fun editCategoryToNewName_showSuccessfulDialog() {
        // Arrange
        onView(withId(R.id.category_rv)).perform(
            actionOnItemAtPosition<RecyclerView.ViewHolder>(position, longClick()))
        onView(withText(R.string.edit)).perform(click())
        onView(withId(R.id.value_et)).perform(clearText(),
            typeText("Updated category from ui test ${Date().time}"),
            closeSoftKeyboard())

        // Act
        onView(withId(R.id.ok_btn)).perform(click())

        // Assert
        onView(withId(R.id.title_tv)).check(matches(withText(R.string.information)))
        onView(withId(R.id.message_tv)).check(matches(withText(R.string.category_updated)))
    }

    @Test
    fun deleteCategory_showSuccessfulDialog() {
        // Arrange
        onView(withId(R.id.category_rv)).perform(
            actionOnItemAtPosition<RecyclerView.ViewHolder>(position, longClick()))

        // Act
        onView(withText(R.string.delete)).perform(click())

        // Assert
        onView(withId(R.id.title_tv)).check(matches(withText(R.string.information)))
        onView(withId(R.id.message_tv)).check(matches(withText(R.string.category_deleted)))
    }

    @Test
    fun categoryClicked_startMaterialActivity() {
        // Arrange
        val selectedCategoryName = getText(withIndex(withId(R.id.category_name_tv), position))

        // Act
        onView(withId(R.id.category_rv)).perform(
            actionOnItemAtPosition<RecyclerView.ViewHolder>(position, click()))
        val receivedCategoryName = getCurrentActivity()?.intent?.getStringExtra(
            Constants.CATEGORY_NAME)

        // Assert
        // Check that intent data was passed
        assertEquals(selectedCategoryName, receivedCategoryName)
        // Check that activity is displayed
        onView(withId(R.id.activity_material_container)).check(matches(isDisplayed()))
    }

}