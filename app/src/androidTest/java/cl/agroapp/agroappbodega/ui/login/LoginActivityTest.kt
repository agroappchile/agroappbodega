package cl.agroapp.agroappbodega.ui.login

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import cl.agroapp.agroappbodega.MatcherUtils.Companion.setChecked
import cl.agroapp.agroappbodega.MatcherUtils.Companion.withTextLengthGreaterThan
import cl.agroapp.agroappbodega.R
import cl.agroapp.agroappbodega.utils.Constants
import org.hamcrest.Matchers.not
import org.junit.Rule
import org.junit.Test

class LoginActivityTest {

    @Rule
    @JvmField
    val loginScenarioRule = ActivityScenarioRule(LoginActivity::class.java)

    @Test
    fun emptyCredentials_showErrorDialog() {
        // Arrange
        onView(withId(R.id.user_et)).perform(clearText(), closeSoftKeyboard())
        onView(withId(R.id.password_et)).perform(clearText(), closeSoftKeyboard())

        // Act
        onView(withId(R.id.login_btn)).perform(click())

        // Assert
        onView(withId(R.id.title_tv)).check(matches(withText(R.string.error)))
        onView(withId(R.id.message_tv)).check(matches(withText(R.string.bad_login_credentials)))
    }

    @Test
    fun validCredentials_startMenuActivity() {
        // Arrange
        onView(withId(R.id.user_et)).perform(clearText(),
            typeText(Constants.VALID_USER_TEST.user),
            closeSoftKeyboard())
        onView(withId(R.id.password_et)).perform(clearText(),
            typeText(Constants.VALID_USER_TEST.password),
            closeSoftKeyboard())

        // Act
        onView(withId(R.id.login_btn)).perform(click())

        // Assert
        onView(withId(R.id.activity_menu_container))
    }

    @Test
    fun invalidCredentials_showErrorDialog() {
        // Arrange
        onView(withId(R.id.user_et)).perform(clearText(),
            typeText(Constants.INVALID_USER_TEST.user),
            closeSoftKeyboard())
        onView(withId(R.id.password_et)).perform(clearText(),
            typeText(Constants.INVALID_USER_TEST.password),
            closeSoftKeyboard())

        // Act
        onView(withId(R.id.login_btn)).perform(click())

        // Assert
        onView(withId(R.id.title_tv)).check(matches(withText(R.string.error)))
        onView(withId(R.id.message_tv)).check(matches(withTextLengthGreaterThan(0)))
    }

    @Test
    fun rememberMeIsChecked_fillCredentialsWhenActivityStarts() {
        // Arrange
        onView(withId(R.id.user_et)).perform(clearText(),
            typeText(Constants.VALID_USER_TEST.user),
            closeSoftKeyboard())
        onView(withId(R.id.password_et)).perform(clearText(),
            typeText(Constants.VALID_USER_TEST.password),
            closeSoftKeyboard())
        onView(withId(R.id.remember_me_cb)).perform(scrollTo(), setChecked(true))
        onView(withId(R.id.login_btn)).perform(click())

        // Act
        ActivityScenario.launch(LoginActivity::class.java)

        // Assert
        onView(withId(R.id.user_et)).check(matches(withText(Constants.VALID_USER_TEST.user)))
        onView(withId(R.id.password_et)).check(matches(withText(Constants.VALID_USER_TEST.password)))
        onView(withId(R.id.remember_me_cb)).check(matches(isChecked()))
    }

    @Test
    fun rememberMeIsNotChecked_doNotFillCredentialsWhenActivityStarts() {
        // Arrange
        onView(withId(R.id.user_et)).perform(clearText(),
            typeText(Constants.VALID_USER_TEST.user),
            closeSoftKeyboard())
        onView(withId(R.id.password_et)).perform(clearText(),
            typeText(Constants.VALID_USER_TEST.password),
            closeSoftKeyboard())
        onView(withId(R.id.remember_me_cb)).perform(scrollTo(), setChecked(false))
        onView(withId(R.id.login_btn)).perform(click())

        // Act
        ActivityScenario.launch(LoginActivity::class.java)

        // Assert
        onView(withId(R.id.user_et)).check(matches(withText(Constants.EMPTY_STRING)))
        onView(withId(R.id.password_et)).check(matches(withText(Constants.EMPTY_STRING)))
        onView(withId(R.id.remember_me_cb)).check(matches(not(isChecked())))
    }

    @Test
    fun rememberMeTextViewClicked_performClickOnRememberMeCheckBox() {
        // Arrange
        onView(withId(R.id.remember_me_cb)).perform(scrollTo(), setChecked(false))

        // Act
        onView(withId(R.id.remember_me_tv)).perform(click())

        // Assert
        onView(withId(R.id.remember_me_cb)).check(matches(isChecked()))
    }

}