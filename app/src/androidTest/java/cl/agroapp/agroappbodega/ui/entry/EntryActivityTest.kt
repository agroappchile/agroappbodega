package cl.agroapp.agroappbodega.ui.entry

import android.app.Activity
import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.rules.ActivityScenarioRule
import cl.agroapp.agroappbodega.MatcherUtils.Companion.withSpinnerSizeGreaterThan
import cl.agroapp.agroappbodega.R
import cl.agroapp.agroappbodega.data.model.MaterialCategory
import cl.agroapp.agroappbodega.data.model.Warehouse
import kotlinx.android.synthetic.main.activity_entry.*
import org.hamcrest.Matchers.*
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test

class EntryActivityTest {

    @Rule
    @JvmField
    val scenarioRule = ActivityScenarioRule(EntryActivity::class.java)

    @Test
    fun activityStart_loadWarehousesAndCategories() {
        // Assert
        onView(withId(R.id.warehouse_sp)).check(matches(withSpinnerSizeGreaterThan(0)))
        onView(withId(R.id.material_sp)).check(matches(withSpinnerSizeGreaterThan(0)))
    }

    @Test
    fun invalidQuantity_showErrorDialog() {
        // Arrange
        onView(withId(R.id.quantity_et)).perform(typeText("0"), closeSoftKeyboard())

        // Act
        onView(withId(R.id.add_entry_fab)).perform(click())

        // Assert
        onView(withId(R.id.title_tv)).check(matches(withText(R.string.error)))
        onView(withId(R.id.message_tv)).check(matches(withText(R.string.invalid_quantity)))
    }

    @Test
    fun validQuantity_finishActivity() {
        // Arrange
        clickOnRandomMaterial()
        clickOnRandomWarehouse()
        val randomQuantity = (1..15).random()
        onView(withId(R.id.quantity_et)).perform(typeText(randomQuantity.toString()),
            closeSoftKeyboard())
        onView(withId(R.id.observation_et)).perform(typeText("Observation from UI test"),
            closeSoftKeyboard())

        // Act
        onView(withId(R.id.add_entry_fab)).perform(click())

        // Assert
        assertEquals(Activity.RESULT_OK, scenarioRule.scenario.result.resultCode)
    }

    private fun clickOnRandomMaterial() {
        onView(withId(R.id.material_sp)).perform(click())
        var randomPosition = 0
        scenarioRule.scenario.onActivity {
            randomPosition = (0 until it.material_sp.adapter.count).random()
        }
        onData(allOf(`is`(instanceOf(MaterialCategory::class.java)))).atPosition(randomPosition)
            .perform(click())
    }

    private fun clickOnRandomWarehouse() {
        onView(withId(R.id.warehouse_sp)).perform(click())
        var randomPosition = 0
        scenarioRule.scenario.onActivity {
            randomPosition = (0 until it.warehouse_sp.adapter.count).random()
        }
        onData(allOf(`is`(instanceOf(Warehouse::class.java)))).atPosition(randomPosition)
            .perform(click())
    }

}