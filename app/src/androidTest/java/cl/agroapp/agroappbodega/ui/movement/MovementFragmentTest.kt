package cl.agroapp.agroappbodega.ui.movement

import android.app.Activity
import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.longClick
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import cl.agroapp.agroappbodega.MatcherUtils.Companion.getCurrentActivity
import cl.agroapp.agroappbodega.MatcherUtils.Companion.withIndex
import cl.agroapp.agroappbodega.MatcherUtils.Companion.withRecyclerViewSizeGreaterThan
import cl.agroapp.agroappbodega.R
import cl.agroapp.agroappbodega.data.model.Movement
import cl.agroapp.agroappbodega.ui.menu.MenuActivity
import cl.agroapp.agroappbodega.utils.DateUtils.Companion.getShortDate
import kotlinx.android.synthetic.main.fragment_movement.*
import org.junit.Rule
import org.junit.Test

class MovementFragmentTest {

    @Rule
    @JvmField
    val menuScenarioRule = ActivityScenarioRule(MenuActivity::class.java)

    private val position = 0

    @Test
    fun fragmentStart_loadMovements() {
        // Assert
        onView(withId(R.id.movement_rv)).check(matches(withRecyclerViewSizeGreaterThan(0)))
    }

    @Test
    fun movementsLoaded_movementDataMatchesTextViewsInAdapter() {
        // Arrange
        lateinit var movement: Movement
        menuScenarioRule.scenario.onActivity {
            movement = (it.movement_rv.adapter as MovementAdapter).movementList[position]
        }

        // Assert
        // Matches id
        onView(withIndex(withId(R.id.movement_id_tv), position)).check(matches(withText(
            movement.movementId.toString())))
        // Matches type
        onView(withIndex(withId(R.id.movement_type_tv), position)).check(matches(withText(
            movement.getMovementType(ApplicationProvider.getApplicationContext()))))
        // Matches date
        onView(withIndex(withId(R.id.movement_date_tv), position)).check(matches(withText(
            getShortDate(movement.created))))
        // Matches summary
        onView(withIndex(withId(R.id.movement_detail_tv), position)).check(matches(withText(
            movement.getSummary(ApplicationProvider.getApplicationContext()))))
    }

    @Test
    fun addMovementFabClicked_setContextMenuTitle() {
        // Act
        onView(withId(R.id.add_movement_fab)).perform(click())

        // Assert
        onView(withText(R.string.choose_movement_type)).check(matches(isDisplayed()))
    }

    @Test
    fun addEntryClicked_startEntryActivity() {
        // Arrange
        onView(withId(R.id.add_movement_fab)).perform(click())

        // Act
        onView(withText(R.string.title_entry)).perform(click())

        // Assert
        onView(withId(R.id.activity_entry_container)).check(matches(isDisplayed()))
    }

    @Test
    fun entryActivityFinishedWithOkResult_showMovementCreatedDialog() {
        // Arrange
        onView(withId(R.id.add_movement_fab)).perform(click())
        onView(withText(R.string.title_entry)).perform(click())

        // Act
        getCurrentActivity()?.setResult(Activity.RESULT_OK)
        getCurrentActivity()?.finish()

        // Assert
        onView(withId(R.id.title_tv)).check(matches(withText(R.string.information)))
        onView(withId(R.id.message_tv)).check(matches(withText(R.string.movement_created)))
    }

    @Test
    fun entryActivityFinishedWithoutOkResult_doNotShowMovementCreatedDialog() {
        // Arrange
        onView(withId(R.id.add_movement_fab)).perform(click())
        onView(withText(R.string.title_entry)).perform(click())

        // Act
        getCurrentActivity()?.finish()

        // Assert
        onView(withId(R.id.title_tv)).check(doesNotExist())
        onView(withId(R.id.message_tv)).check(doesNotExist())
    }

    @Test
    fun deleteMovement_showSuccessfulDialog() {
        // Arrange
        onView(withId(R.id.movement_rv)).perform(
            actionOnItemAtPosition<RecyclerView.ViewHolder>(position, longClick()))

        // Act
        onView(withText(R.string.delete)).perform(click())

        // Assert
        onView(withId(R.id.title_tv)).check(matches(withText(R.string.information)))
        onView(withId(R.id.message_tv)).check(matches(withText(R.string.movement_deleted)))
    }

}