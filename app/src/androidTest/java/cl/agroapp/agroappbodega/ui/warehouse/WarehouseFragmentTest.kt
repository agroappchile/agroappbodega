package cl.agroapp.agroappbodega.ui.warehouse

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.matcher.RootMatchers.isDialog
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import cl.agroapp.agroappbodega.MatcherUtils.Companion.getText
import cl.agroapp.agroappbodega.MatcherUtils.Companion.hasItem
import cl.agroapp.agroappbodega.MatcherUtils.Companion.withIndex
import cl.agroapp.agroappbodega.MatcherUtils.Companion.withRecyclerViewSizeGreaterThan
import cl.agroapp.agroappbodega.MatcherUtils.Companion.withTextLengthGreaterThan
import cl.agroapp.agroappbodega.R
import cl.agroapp.agroappbodega.ui.menu.MenuActivity
import cl.agroapp.agroappbodega.utils.Constants
import kotlinx.android.synthetic.main.fragment_warehouse.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.util.*

class WarehouseFragmentTest {

    @Rule
    @JvmField
    val menuScenarioRule = ActivityScenarioRule(MenuActivity::class.java)

    private val position = 0

    @Before
    fun setUp() {
        onView(withId(R.id.navigation_warehouse)).perform(click())
    }

    @Test
    fun fragmentStart_loadWarehouses() {
        // Assert
        onView(withId(R.id.warehouse_rv)).check(matches(withRecyclerViewSizeGreaterThan(0)))
    }

    @Test
    fun warehousesLoaded_warehouseNameMatchesTextViewInAdapter() {
        // Arrange
        lateinit var warehouseName: String
        menuScenarioRule.scenario.onActivity {
            warehouseName = (it.warehouse_rv.adapter as WarehouseAdapter)
                .warehouseList[position].name
        }

        // Assert
        onView(withIndex(withId(R.id.warehouse_name_tv), position))
            .check(matches(withText(warehouseName)))
    }

    @Test
    fun addWarehouseClicked_showInputDialog() {
        // Act
        onView(withId(R.id.add_warehouse_fab)).perform(click())

        // Assert
        onView(withId(R.id.title_tv)).check(matches(withText(R.string.add)))
        onView(withId(R.id.message_tv)).check(matches(withText(R.string.enter_warehouse_name)))
    }

    @Test
    fun addNewWarehouse_showSuccessfulDialog() {
        // Arrange
        onView(withId(R.id.add_warehouse_fab)).perform(click())
        onView(withId(R.id.value_et)).perform(
            typeText("New warehouse from UI test ${Date().time}"),
            closeSoftKeyboard())

        // Act
        onView(withId(R.id.ok_btn)).perform(click())

        // Assert
        onView(withId(R.id.title_tv)).check(matches(withText(R.string.information)))
        onView(withId(R.id.message_tv)).check(matches(withText(R.string.warehouse_created)))
    }

    @Test
    fun addNewWarehouse_warehouseIsAdded() {
        // Arrange
        val warehouseName = "New warehouse from UI test ${Date().time}"
        onView(withId(R.id.add_warehouse_fab)).perform(click())
        onView(withId(R.id.value_et)).perform(typeText(warehouseName), closeSoftKeyboard())

        // Act
        onView(withId(R.id.ok_btn)).perform(click())
        onView(withId(R.id.ok_btn)).perform(click())

        // Assert
        onView(withId(R.id.warehouse_rv)).check(matches(hasItem(hasDescendant(
            withText(warehouseName)))))
    }

    @Test
    fun addExistingWarehouse_showErrorDialog() {
        // Arrange
        onView(withId(R.id.add_warehouse_fab)).perform(click())
        onView(withId(R.id.value_et)).perform(
            typeText(Constants.EXISTING_WAREHOUSE_NAME),
            closeSoftKeyboard())

        // Act
        onView(withId(R.id.ok_btn)).perform(click())

        // Assert
        onView(withId(R.id.title_tv)).check(matches(withText(R.string.error)))
        onView(withId(R.id.message_tv)).check(matches(withTextLengthGreaterThan(0)))
    }

    @Test
    fun editWarehouseClicked_showInputDialog() {
        // Arrange
        onView(withId(R.id.warehouse_rv)).perform(
            actionOnItemAtPosition<RecyclerView.ViewHolder>(position, longClick()))

        // Act
        onView(withText(R.string.edit)).perform(click())

        // Assert
        onView(withId(R.id.title_tv)).check(matches(withText(R.string.edit)))
        onView(withId(R.id.message_tv)).check(matches(withText(R.string.enter_warehouse_name)))
    }

    @Test
    fun editWarehouseClicked_fillsInputDialogWithCurrentWarehouseName() {
        // Arrange
        val warehouseName = getText(withIndex(withId(R.id.warehouse_name_tv), position))
        onView(withId(R.id.warehouse_rv)).perform(
            actionOnItemAtPosition<RecyclerView.ViewHolder>(position, longClick()))

        // Act
        onView(withText(R.string.edit)).perform(click())

        // Assert
        onView(withId(R.id.value_et)).inRoot(isDialog())
            .check(matches(withText(warehouseName)))
            .check(matches(isDisplayed()))
    }

    @Test
    fun editWarehouseToNewName_showSuccessfulDialog() {
        // Arrange
        onView(withId(R.id.warehouse_rv)).perform(
            actionOnItemAtPosition<RecyclerView.ViewHolder>(position, longClick()))
        onView(withText(R.string.edit)).perform(click())
        onView(withId(R.id.value_et)).perform(clearText(),
            typeText("Updated warehouse from ui test ${Date().time}"),
            closeSoftKeyboard())

        // Act
        onView(withId(R.id.ok_btn)).perform(click())

        // Assert
        onView(withId(R.id.title_tv)).check(matches(withText(R.string.information)))
        onView(withId(R.id.message_tv)).check(matches(withText(R.string.warehouse_updated)))
    }

    @Test
    fun deleteWarehouse_showSuccessfulDialog() {
        // Arrange
        onView(withId(R.id.warehouse_rv)).perform(
            actionOnItemAtPosition<RecyclerView.ViewHolder>(position, longClick()))

        // Act
        onView(withText(R.string.delete)).perform(click())

        // Assert
        onView(withId(R.id.title_tv)).check(matches(withText(R.string.information)))
        onView(withId(R.id.message_tv)).check(matches(withText(R.string.warehouse_deleted)))
    }

}