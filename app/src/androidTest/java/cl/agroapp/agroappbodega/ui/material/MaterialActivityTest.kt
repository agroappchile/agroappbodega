package cl.agroapp.agroappbodega.ui.material

import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.matcher.RootMatchers.isPlatformPopup
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import cl.agroapp.agroappbodega.MatcherUtils.Companion.allItems
import cl.agroapp.agroappbodega.MatcherUtils.Companion.getText
import cl.agroapp.agroappbodega.MatcherUtils.Companion.hasItem
import cl.agroapp.agroappbodega.MatcherUtils.Companion.typeSearchViewText
import cl.agroapp.agroappbodega.MatcherUtils.Companion.withIndex
import cl.agroapp.agroappbodega.MatcherUtils.Companion.withRecyclerViewSizeGreaterThan
import cl.agroapp.agroappbodega.MatcherUtils.Companion.withSpinnerSizeGreaterThan
import cl.agroapp.agroappbodega.MatcherUtils.Companion.withTextLengthGreaterThan
import cl.agroapp.agroappbodega.R
import cl.agroapp.agroappbodega.data.model.Category
import cl.agroapp.agroappbodega.utils.Constants
import org.hamcrest.Matchers.*
import org.junit.Rule
import org.junit.Test
import java.util.*

class MaterialActivityTest {

    @Rule
    @JvmField
    val scenarioRule = ActivityScenarioRule<MaterialActivity>(Intent(
        ApplicationProvider.getApplicationContext(), MaterialActivity::class.java)
        .putExtra(Constants.CATEGORY_NAME, Constants.EXISTING_CATEGORY_NAME))

    private val position = 0

    @Test
    fun activityStart_loadMaterials() {
        // Assert
        onView(withId(R.id.material_rv)).check(matches(withRecyclerViewSizeGreaterThan(0)))
    }

    @Test
    fun materialsLoaded_materialNameMatchesTextViewInAdapter() {
        // Arrange
        lateinit var materialName: String
        scenarioRule.scenario.onActivity {
            materialName = it.materialAdapter.adapterList[position].material.name
        }

        // Assert
        onView(withIndex(withId(R.id.material_tv), position)).check(matches(withText(materialName)))
    }

    @Test
    fun materialsLoaded_categoryNameMatchesTextViewInAdapter() {
        // Arrange
        lateinit var categoryName: String
        scenarioRule.scenario.onActivity {
            categoryName = it.materialAdapter.adapterList[position].category.name
        }

        // Assert
        onView(withIndex(withId(R.id.category_tv), position)).check(matches(withText(categoryName)))
    }

    @Test
    fun addMaterialClicked_showMaterialDialog() {
        // Act
        onView(withId(R.id.add_material_fab)).perform(click())

        // Assert
        onView(withId(R.id.title_tv)).check(matches(withText(R.string.add_material)))
        onView(withId(R.id.message_tv)).check(matches(withText(R.string.enter_material_name)))
        onView(withId(R.id.category_sp)).check(matches(withSpinnerSizeGreaterThan(0)))
    }

    @Test
    fun addNewMaterial_showSuccessfulDialog() {
        // Arrange
        clickOnRandomCategory()

        // Act
        onView(withId(R.id.value_et)).perform(typeText("New material from UI test ${Date().time}"),
            closeSoftKeyboard())
        onView(withId(R.id.ok_btn)).perform(click())

        // Assert
        onView(withId(R.id.title_tv)).check(matches(withText(R.string.information)))
        onView(withId(R.id.message_tv)).check(matches(withText(R.string.material_created)))
    }

    private fun clickOnRandomCategory() {
        onView(withId(R.id.add_material_fab)).perform(click())
        onView(withId(R.id.category_sp)).perform(click())
        var randomPosition = 0
        scenarioRule.scenario.onActivity {
            randomPosition = (0 until it.materialDialog.categoryList.size).random()
        }
        onData(allOf(`is`(instanceOf(Category::class.java)))).inRoot(isPlatformPopup())
            .atPosition(randomPosition).perform(click())
    }

    @Test
    fun addNewMaterial_materialIsAdded() {
        // Arrange
        onView(withId(R.id.search_view)).perform(typeSearchViewText(Constants.EMPTY_STRING))
        clickOnRandomCategory()
        val materialName = "New material from UI test ${Date().time}"

        // Act
        onView(withId(R.id.value_et)).perform(typeText(materialName), closeSoftKeyboard())
        onView(withId(R.id.ok_btn)).perform(click())
        onView(withId(R.id.ok_btn)).perform(click())

        // Assert
        onView(withId(R.id.material_rv)).check(matches(hasItem(hasDescendant(
            withText(materialName)))))
    }

    @Test
    fun addExistingMaterial_showErrorDialog() {
        // Arrange
        clickOnRandomCategory()

        // Act
        onView(withId(R.id.value_et)).perform(typeText(Constants.EXISTING_MATERIAL_NAME),
            closeSoftKeyboard())
        onView(withId(R.id.ok_btn)).perform(click())

        // Assert
        onView(withId(R.id.title_tv)).check(matches(withText(R.string.error)))
        onView(withId(R.id.message_tv)).check(matches(withTextLengthGreaterThan(0)))
    }

    @Test
    fun editMaterialClicked_showInputDialog() {
        // Arrange
        val materialName = getText(withIndex(withId(R.id.material_tv), position))
        onView(withId(R.id.material_rv)).perform(
            actionOnItemAtPosition<RecyclerView.ViewHolder>(position, longClick()))

        // Act
        onView(withText(R.string.edit)).perform(click())

        // Assert
        onView(withId(R.id.title_tv)).check(matches(withText(R.string.edit)))
        onView(withId(R.id.message_tv)).check(matches(withText(R.string.enter_material_name)))
        onView(withId(R.id.value_et)).check(matches(withText(materialName)))
    }

    @Test
    fun editMaterialToNewName_showSuccessfulDialog() {
        // Arrange
        onView(withId(R.id.material_rv)).perform(
            actionOnItemAtPosition<RecyclerView.ViewHolder>(position, longClick()))
        onView(withText(R.string.edit)).perform(click())
        onView(withId(R.id.value_et)).perform(clearText(),
            typeText("Updated material from ui test ${Date().time}"),
            closeSoftKeyboard())

        // Act
        onView(withId(R.id.ok_btn)).perform(click())

        // Assert
        onView(withId(R.id.title_tv)).check(matches(withText(R.string.information)))
        onView(withId(R.id.message_tv)).check(matches(withText(R.string.material_updated)))
    }

    @Test
    fun editMaterialToNewName_materialNameIsEdited() {
        // Arrange
        onView(withId(R.id.material_rv)).perform(
            actionOnItemAtPosition<RecyclerView.ViewHolder>(position, longClick()))
        onView(withText(R.string.edit)).perform(click())
        val materialName = "Updated material from ui test ${Date().time}"
        onView(withId(R.id.value_et)).perform(clearText(), typeText(materialName),
            closeSoftKeyboard())

        // Act
        onView(withId(R.id.ok_btn)).perform(click())
        onView(withId(R.id.ok_btn)).perform(click())

        // Assert
        onView(withId(R.id.material_rv)).check(matches(hasItem(hasDescendant(
            withText(materialName)))))
    }

    @Test
    fun deleteMaterial_showSuccessfulDialog() {
        // Arrange
        onView(withId(R.id.material_rv)).perform(
            actionOnItemAtPosition<RecyclerView.ViewHolder>(position, longClick()))

        // Act
        onView(withText(R.string.delete)).perform(click())

        // Assert
        onView(withId(R.id.title_tv)).check(matches(withText(R.string.information)))
        onView(withId(R.id.message_tv)).check(matches(withText(R.string.material_deleted)))
    }

    @Test
    fun searchByCategoryName_filterMaterialList() {
        // Act
        onView(withId(R.id.search_view)).perform(typeSearchViewText(
            Constants.EXISTING_CATEGORY_NAME2), closeSoftKeyboard())

        // Assert
        onView(withId(R.id.material_rv)).check(matches(allItems(hasDescendant(withText(
            Constants.EXISTING_CATEGORY_NAME2)))))
    }

    @Test
    fun activityStarts_filterMaterialList() {
        // Arrange
        lateinit var query: String
        scenarioRule.scenario.onActivity {
            query = it.intent.getStringExtra(Constants.CATEGORY_NAME)
                ?: throw NullPointerException("extra is null")
        }

        // Assert
        onView(withId(R.id.material_rv)).check(matches(allItems(hasDescendant(withText(query)))))
    }

}