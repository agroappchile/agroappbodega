package cl.agroapp.agroappbodega.ui.menu

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import cl.agroapp.agroappbodega.R
import org.junit.Rule
import org.junit.Test

class MenuActivityTest {

    @Rule
    @JvmField
    val menuScenarioRule = ActivityScenarioRule(MenuActivity::class.java)

    @Test
    fun categoryTabClicked_startCategoryFragment() {
        // Act
        onView(withId(R.id.navigation_categoria)).perform(click())

        // Assert
        onView(withId(R.id.fragment_category_container)).check(matches(isDisplayed()))
    }

    @Test
    fun warehouseTabClicked_startWarehouseFragment() {
        // Act
        onView(withId(R.id.navigation_warehouse)).perform(click())

        // Assert
        onView(withId(R.id.fragment_warehouse_container)).check(matches(isDisplayed()))
    }

    @Test
    fun movementTabClicked_startMovementFragment() {
        // Act
        onView(withId(R.id.navigation_movement)).perform(click())

        // Assert
        onView(withId(R.id.fragment_movement_container)).check(matches(isDisplayed()))
    }

}