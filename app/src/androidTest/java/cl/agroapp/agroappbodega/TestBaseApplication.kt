package cl.agroapp.agroappbodega

import cl.agroapp.agroappbodega.di.NetworkComponentFactory
import com.squareup.rx2.idler.Rx2Idler
import io.reactivex.plugins.RxJavaPlugins

class TestBaseApplication : BaseApplication() {

    override fun onCreate() {
        super.onCreate()
        RxJavaPlugins.setInitIoSchedulerHandler(Rx2Idler.create("RxJava 2.x IO Scheduler"))
    }

    override fun getNetworkComponent() = NetworkComponentFactory.newTestingNetworkComponent()

}