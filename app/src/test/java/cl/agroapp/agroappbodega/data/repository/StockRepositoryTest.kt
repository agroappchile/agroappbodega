package cl.agroapp.agroappbodega.data.repository

import cl.agroapp.agroappbodega.di.NetworkComponentFactory
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class StockRepositoryTest {

    private lateinit var stockRepository: StockRepository

    @Before
    fun setUp() {
        val networkComponent = NetworkComponentFactory.newTestingNetworkComponent()
        stockRepository = StockRepository(networkComponent.getStockService())
    }

    @Test
    fun getStock_listSizeIsGreaterThanZero() {
        // Act
        val stockResponse = stockRepository.getStock().test().values().first()

        // Assert
        assertEquals(true, stockResponse.stockList.size > 0)
    }

}