package cl.agroapp.agroappbodega.data.repository

import cl.agroapp.agroappbodega.data.model.Category
import cl.agroapp.agroappbodega.data.model.Material
import cl.agroapp.agroappbodega.di.NetworkComponentFactory
import cl.agroapp.agroappbodega.utils.Constants
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import retrofit2.HttpException
import java.util.*

// Integration test with a test server
class MaterialRepositoryTest {

    private lateinit var materialRepository: MaterialRepository

    @Before
    fun setUp() {
        val networkComponent = NetworkComponentFactory.newTestingNetworkComponent()
        materialRepository = MaterialRepository(networkComponent.getMaterialService())
    }

    @Test
    fun postNewMaterial_returnCategoryListWithNewMaterial() {
        // Arrange
        val material = Material(name = "New material from integration test ${Date().time}")
        val category = Category(id = Constants.EXISTING_CATEGORY_ID,
            materialList = arrayListOf(material))

        // Act
        val categoryFound = materialRepository.postMaterial(category).test().values().first()
            .categoryList.find { it.id == category.id }
            ?: throw NullPointerException("Category with id ${category.id} not found")

        // Assert
        assertEquals(material.name,
            categoryFound.materialList.find { it.name == material.name }?.name)
    }

    @Test
    fun postExistingMaterial_returnError() {
        // Arrange
        val material = Material(name = Constants.EXISTING_MATERIAL_NAME)
        val category = Category(id = Constants.EXISTING_CATEGORY_ID,
            materialList = arrayListOf(material))

        // Act + Assert
        materialRepository.postMaterial(category).test().assertError(HttpException::class.java)
    }

    @Test
    fun putNewMaterial_returnCategoryListWithUpdatedMaterial() {
        // Arrange
        val categoryId = Constants.EXISTING_CATEGORY_ID
        val material = Material(id = Constants.EXISTING_MATERIAL_ID,
            name = "Updated category from integration test ${Date().time}")

        // Act
        val categoryFound = materialRepository.putMaterial(material).test().values().first()
            .categoryList.find { it.id == categoryId }
            ?: throw NullPointerException("Category with id $categoryId not found")

        // Assert
        assertEquals(material.name, categoryFound.materialList.find { it.id == material.id }?.name)
    }

    @Test
    fun putExistingMaterial_returnError() {
        // Arrange
        val material = Material(id = Constants.EXISTING_MATERIAL_ID,
            name = Constants.EXISTING_MATERIAL_NAME)

        // Act + Assert
        materialRepository.putMaterial(material).test().assertError(HttpException::class.java)
    }

    @Test
    fun deleteMaterial_returnCategoryListWithoutMaterial() {
        // Arrange
        val material = Material(
            id = Constants.EXISTING_MATERIAL_ID,
            name = "Updated category from integration test ${Date().time}",
            active = Constants.INACTIVE
        )

        // Act
        val categoryList = materialRepository.putMaterial(material).test()
            .values().first().categoryList
        val allMaterials = arrayListOf<Material>()
        categoryList.forEach {
            allMaterials.addAll(it.materialList)
        }

        // Assert
        assertEquals(null, allMaterials.find { it.id == material.id })
    }

}