package cl.agroapp.agroappbodega.data.repository

import cl.agroapp.agroappbodega.data.model.Entry
import cl.agroapp.agroappbodega.data.model.Material
import cl.agroapp.agroappbodega.data.model.Warehouse
import cl.agroapp.agroappbodega.di.NetworkComponentFactory
import cl.agroapp.agroappbodega.utils.Constants
import org.junit.Before
import org.junit.Test

// Integration test with a test server
class EntryRepositoryTest {

    private lateinit var entryRepository: EntryRepository

    @Before
    fun setUp() {
        val networkComponent = NetworkComponentFactory.newTestingNetworkComponent()
        entryRepository = EntryRepository(networkComponent.getEntryService())
    }

    @Test
    fun postEntry_returnSuccess() {
        // Arrange
        val entry = Entry()
        entry.quantity = 1.25
        entry.observation = "Observation from integration test"
        entry.material = Material(id = Constants.EXISTING_MATERIAL_ID)
        entry.warehouse = Warehouse(id = Constants.EXISTING_WAREHOUSE_ID)

        // Act + Assert
        entryRepository.postEntry(entry).test().assertComplete()
    }

}