package cl.agroapp.agroappbodega.data.repository

import cl.agroapp.agroappbodega.data.model.Warehouse
import cl.agroapp.agroappbodega.di.NetworkComponentFactory
import cl.agroapp.agroappbodega.utils.Constants
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import java.util.*

// Integration test with a test server
class WarehouseRepositoryTest {

    private lateinit var warehouseRepository: WarehouseRepository

    @Before
    fun setUp() {
        val networkComponent = NetworkComponentFactory.newTestingNetworkComponent()
        warehouseRepository = WarehouseRepository(networkComponent.getWarehouseService())
    }

    @Test
    fun getWarehouses_listSizeIsGreaterThanZero() {
        // Act
        val warehouseResponse = warehouseRepository.getWarehouses().test().values().first()

        // Assert
        assertEquals(true, warehouseResponse.warehouseList.size > 0)
    }

    @Test
    fun postWarehouse_returnListWithNewWarehouse() {
        // Arrange
        val warehouse = Warehouse(name = "New warehouse from integration test ${Date().time}")

        // Act
        val warehouseList = warehouseRepository.postWarehouse(warehouse).test().values()
            .first().warehouseList

        // Assert
        assertEquals(warehouse.name, warehouseList.find { it.name == warehouse.name }?.name)
    }

    @Test
    fun putWarehouse_returnWarehouseListWithUpdatedWarehouse() {
        // Arrange
        val warehouse = Warehouse(id = Constants.EXISTING_WAREHOUSE_ID,
            name = "Updated warehouse from integration test ${Date().time}")

        // Act
        val warehouseList = warehouseRepository.putWarehouse(warehouse).test().values()
            .first().warehouseList

        // Assert
        assertEquals(warehouse.name, warehouseList.find { it.id == warehouse.id }?.name)
    }

    @Test
    fun deleteCategory_returnListWithoutCategory() {
        // Arrange
        val warehouse = Warehouse(id = Constants.EXISTING_WAREHOUSE_ID, active = Constants.INACTIVE)

        // Act
        val warehouseList = warehouseRepository.putWarehouse(warehouse).test().values()
            .first().warehouseList

        // Assert
        assertEquals(null, warehouseList.find { it.id == warehouse.id })
    }

}