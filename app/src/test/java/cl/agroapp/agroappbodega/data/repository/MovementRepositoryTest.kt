package cl.agroapp.agroappbodega.data.repository

import cl.agroapp.agroappbodega.data.model.Entry
import cl.agroapp.agroappbodega.data.model.Material
import cl.agroapp.agroappbodega.data.model.Warehouse
import cl.agroapp.agroappbodega.di.NetworkComponentFactory
import cl.agroapp.agroappbodega.utils.Constants
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

// Integration test with a test server
class MovementRepositoryTest {

    private lateinit var movementRepository: MovementRepository

    @Before
    fun setUp() {
        val networkComponent = NetworkComponentFactory.newTestingNetworkComponent()
        movementRepository = MovementRepository(networkComponent.getMovementService())
    }

    @Test
    fun getMovements_listSizeIsGreaterThanZero() {
        // Act
        val movementResponse = movementRepository.getMovements().test().values().first()

        // Assert
        assertEquals(true, movementResponse.movementList.size > 0)
    }

    @Test
    fun deleteMovement_assertComplete() {
        // Arrange
        val entry = Entry(id = Constants.EXISTING_ENTRY_ID)
        entry.warehouse = Warehouse(id = Constants.EXISTING_WAREHOUSE_ID)
        entry.material = Material(id = Constants.EXISTING_MATERIAL_ID)

        // Act + Assert
        movementRepository.deleteMovement(entry).test().assertComplete()
    }

}