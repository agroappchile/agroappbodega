package cl.agroapp.agroappbodega.data.repository

import cl.agroapp.agroappbodega.data.model.Category
import cl.agroapp.agroappbodega.di.NetworkComponentFactory
import cl.agroapp.agroappbodega.utils.Constants
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import retrofit2.HttpException
import java.util.*

// Integration test with a test server
class CategoryRepositoryTest {

    private lateinit var categoryRepository: CategoryRepository

    @Before
    fun setUp() {
        val networkComponent = NetworkComponentFactory.newTestingNetworkComponent()
        categoryRepository = CategoryRepository(networkComponent.getCategoryService())
    }

    @Test
    fun getCategories_listSizeIsGreaterThanZero() {
        // Act
        val categoryResponse = categoryRepository.getCategories().test().values().first()

        // Assert
        assertEquals(true, categoryResponse.categoryList.size > 0)
    }

    @Test
    fun postNewCategory_returnListWithNewCategory() {
        // Arrange
        val category = Category(name = "New category from integration test ${Date().time}")

        // Act
        val categoryList = categoryRepository.postCategory(category).test().values()
            .first().categoryList

        // Assert
        assertEquals(category.name, categoryList.find { it.name == category.name }?.name)
    }

    @Test
    fun postExistingCategory_returnError() {
        // Arrange
        val category = Category(name = Constants.EXISTING_CATEGORY_NAME)

        // Act + Assert
        categoryRepository.postCategory(category).test().assertError(HttpException::class.java)
    }

    @Test
    fun putNewCategory_returnCategoryListWithUpdatedCategory() {
        // Arrange
        val category = Category(id = Constants.EXISTING_CATEGORY_ID,
            name = "Updated category from integration test ${Date().time}")

        // Act
        val categoryList = categoryRepository.putCategory(category).test().values()
            .first().categoryList

        // Assert
        assertEquals(category.name, categoryList.find { it.id == category.id }?.name)
    }

    @Test
    fun putExistingCategory_returnError() {
        // Arrange
        val category = Category(id = Constants.EXISTING_CATEGORY_ID,
            name = Constants.EXISTING_CATEGORY_NAME)

        // Act + Assert
        categoryRepository.putCategory(category).test().assertError(HttpException::class.java)
    }

    @Test
    fun deleteCategory_returnListWithoutCategory() {
        // Arrange
        val category = Category(
            id = Constants.EXISTING_CATEGORY_ID,
            name = "Updated category from integration test ${Date().time}",
            active = Constants.INACTIVE
        )

        // Act
        val categoryList = categoryRepository.putCategory(category).test().values()
            .first().categoryList

        // Assert
        assertEquals(null, categoryList.find { it.id == category.id })
    }

}