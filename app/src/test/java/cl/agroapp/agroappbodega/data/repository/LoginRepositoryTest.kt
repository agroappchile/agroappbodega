package cl.agroapp.agroappbodega.data.repository

import cl.agroapp.agroappbodega.di.NetworkComponentFactory
import cl.agroapp.agroappbodega.utils.Constants
import org.junit.Before
import org.junit.Test
import retrofit2.HttpException

// Integration test with a test server
class LoginRepositoryTest {

    private lateinit var loginRepository: LoginRepository

    @Before
    fun setUp() {
        loginRepository = LoginRepository(NetworkComponentFactory.newTestingNetworkComponent()
            .getLoginService())
    }

    @Test
    fun validUser_returnSuccess() {
        // Assert
        loginRepository.login(Constants.VALID_USER_TEST).test().assertComplete()
    }

    @Test
    fun invalidUser_returnError() {
        // Assert
        loginRepository.login(Constants.INVALID_USER_TEST).test()
            .assertError(HttpException::class.java)
    }

}