package cl.agroapp.agroappbodega

import cl.agroapp.agroappbodega.utils.ApiUtils
import cl.agroapp.agroappbodega.utils.NetworkUtils
import io.reactivex.disposables.CompositeDisposable
import org.mockito.Mockito.mock

class LocalTestUtils {

    companion object {
        fun newNetworkUtils() = NetworkUtils(
            TrampolineSchedulerProvider(),
            mock(CompositeDisposable::class.java),
            mock(ApiUtils::class.java)
        )
    }

}