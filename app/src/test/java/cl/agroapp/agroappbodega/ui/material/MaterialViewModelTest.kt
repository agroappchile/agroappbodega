package cl.agroapp.agroappbodega.ui.material

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import cl.agroapp.agroappbodega.LocalTestUtils
import cl.agroapp.agroappbodega.R
import cl.agroapp.agroappbodega.data.model.Category
import cl.agroapp.agroappbodega.data.model.Material
import cl.agroapp.agroappbodega.data.model.MaterialCategory
import cl.agroapp.agroappbodega.data.network.model.CategoryResponse
import cl.agroapp.agroappbodega.data.repository.CategoryRepository
import cl.agroapp.agroappbodega.data.repository.MaterialRepository
import cl.agroapp.agroappbodega.utils.Constants
import cl.agroapp.agroappbodega.utils.MaterialCategoryUtils.Companion.transformToMaterialCategory
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.argumentCaptor
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class MaterialViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    private lateinit var categoryRepositoryMock: CategoryRepository
    @Mock
    private lateinit var materialRepositoryMock: MaterialRepository
    private val networkUtils = LocalTestUtils.newNetworkUtils()

    private val category1 = Category(name = "MEDICINES", materialList = arrayListOf(
        Material(name = "PARACETAMOL"),
        Material(name = "IBUPROFEN")
    ))
    private val category2 = Category(name = "TOOLS", materialList = arrayListOf(
        Material(name = "HAMMER"),
        Material(name = "SHOVEL")
    ))
    private val category3 = Category(name = "GROCERY", materialList = arrayListOf(
        Material(name = "RICE")
    ))
    private val getCategoriesResponse = CategoryResponse(arrayListOf(category1, category2))
    private val postMaterialResponse = CategoryResponse(arrayListOf(category2, category3))
    private val putMaterialResponse = CategoryResponse(arrayListOf(category3))

    private val categoryId = 1
    private val materialId = 1
    private val materialName = "Material name"
    private val error = "error"
    private val material = Material()

    private fun stubGetCategoriesResponse() {
        `when`(categoryRepositoryMock.getCategories())
            .thenReturn(Single.just(getCategoriesResponse))
    }

    private fun newMaterialViewModel() = MaterialViewModel(materialRepositoryMock,
        categoryRepositoryMock, networkUtils)

    @Test
    fun getAllCategoriesSuccess_setCategoryList() {
        // Arrange
        stubGetCategoriesResponse()

        // Act
        val materialViewModel = newMaterialViewModel()

        // Assert
        assertEquals(getCategoriesResponse.categoryList, materialViewModel.categoryList)
    }

    @Test
    fun getAllCategoriesSubscribe_loadingObservableIsTrue() {
        // Arrange
        val delayer = PublishSubject.create<Void>()
        `when`(categoryRepositoryMock.getCategories())
            .thenReturn(Single.just(getCategoriesResponse).delaySubscription(delayer))

        // Act
        val materialViewModel = newMaterialViewModel()

        // Assert
        assertEquals(true, materialViewModel.loadingObservable.value)
    }

    @Test
    fun getAllCategoriesFinally_loadingObservableIsFalse() {
        // Arrange
        stubGetCategoriesResponse()

        // Act
        val materialViewModel = newMaterialViewModel()

        // Assert
        assertEquals(false, materialViewModel.loadingObservable.value)
    }

    @Test
    fun getAllCategoriesFailed_setRepositoryErrorObservable() {
        // Arrange
        val throwableMock = mock(Throwable::class.java)
        `when`(categoryRepositoryMock.getCategories()).thenReturn(Single.error(throwableMock))
        `when`(networkUtils.getErrorMessage(throwableMock)).thenReturn(error)

        // Act
        val materialViewModel = newMaterialViewModel()

        // Assert
        assertEquals(error, materialViewModel.repositoryErrorObservable.value)
    }

    @Test
    fun newMaterial_callPostMaterial() {
        // Arrange
        stubGetCategoriesAndPostMaterialResponse()
        val captor = argumentCaptor<Category>()
        val spacedMaterialName = " $materialName "

        // Act
        val materialViewModel = newMaterialViewModel()
        materialViewModel.newMaterial(categoryId, spacedMaterialName)

        // Assert
        verify(materialRepositoryMock).postMaterial(captor.capture())
        assertEquals(categoryId, captor.firstValue.id)
        assertEquals(spacedMaterialName.trim(), captor.firstValue.materialList.first().name)
    }

    @Test
    fun postMaterialSuccess_setNotificationObservable() {
        // Arrange
        stubGetCategoriesAndPostMaterialResponse()

        // Act
        val materialViewModel = newMaterialViewModel()
        materialViewModel.newMaterial(categoryId, materialName)

        // Assert
        assertEquals(R.string.material_created, materialViewModel.notificationObservable.value)
    }

    @Test
    fun postMaterialSuccess_setCategoryList() {
        // Arrange
        stubGetCategoriesAndPostMaterialResponse()

        // Act
        val materialViewModel = newMaterialViewModel()
        materialViewModel.newMaterial(categoryId, materialName)

        // Assert
        assertEquals(postMaterialResponse.categoryList, materialViewModel.categoryList)
    }

    private fun stubGetCategoriesAndPostMaterialResponse() {
        stubGetCategoriesResponse()
        `when`(materialRepositoryMock.postMaterial(any()))
            .thenReturn(Single.just(postMaterialResponse))
    }

    @Test
    fun postMaterialSubscribe_loadingObservableIsTrue() {
        // Arrange
        stubGetCategoriesResponse()
        val delayer = PublishSubject.create<Void>()
        `when`(materialRepositoryMock.postMaterial(any()))
            .thenReturn(Single.just(postMaterialResponse).delaySubscription(delayer))

        // Act
        val materialViewModel = newMaterialViewModel()
        materialViewModel.newMaterial(categoryId, materialName)

        // Assert
        assertEquals(true, materialViewModel.loadingObservable.value)
    }

    @Test
    fun postMaterialFinally_loadingObservableIsFalse() {
        // Arrange
        stubGetCategoriesAndPostMaterialResponse()

        // Act
        val materialViewModel = newMaterialViewModel()
        materialViewModel.newMaterial(categoryId, materialName)

        // Assert
        assertEquals(false, materialViewModel.loadingObservable.value)
    }

    @Test
    fun postMaterialFailed_setRepositoryErrorObservable() {
        // Arrange
        stubGetCategoriesResponse()
        val throwableMock = mock(Throwable::class.java)
        `when`(materialRepositoryMock.postMaterial(any())).thenReturn(Single.error(throwableMock))
        `when`(networkUtils.getErrorMessage(throwableMock)).thenReturn(error)

        // Act
        val materialViewModel = newMaterialViewModel()
        materialViewModel.newMaterial(categoryId, materialName)

        // Assert
        assertEquals(error, materialViewModel.repositoryErrorObservable.value)
    }

    @Test
    fun updateMaterial_callPutMaterial() {
        // Arrange
        stubGetCategoriesAndPutMaterialResponse()
        val captor = argumentCaptor<Material>()
        val spacedMaterialName = " $materialName "

        // Act
        val materialViewModel = newMaterialViewModel()
        materialViewModel.updateMaterial(materialId, spacedMaterialName)

        // Assert
        verify(materialRepositoryMock).putMaterial(captor.capture())
        assertEquals(materialId, captor.firstValue.id)
        assertEquals(spacedMaterialName.trim(), captor.firstValue.name)
    }

    private fun stubGetCategoriesAndPutMaterialResponse() {
        stubGetCategoriesResponse()
        `when`(materialRepositoryMock.putMaterial(any()))
            .thenReturn(Single.just(putMaterialResponse))
    }

    @Test
    fun putMaterialSuccess_setNotificationObservable() {
        // Arrange
        stubGetCategoriesAndPutMaterialResponse()

        // Act
        val materialViewModel = newMaterialViewModel()
        materialViewModel.updateMaterial(materialId, materialName)

        // Assert
        assertEquals(R.string.material_updated, materialViewModel.notificationObservable.value)
    }

    @Test
    fun putMaterialSuccess_setCategoryList() {
        // Arrange
        stubGetCategoriesAndPutMaterialResponse()

        // Act
        val materialViewModel = newMaterialViewModel()
        materialViewModel.updateMaterial(materialId, materialName)

        // Assert
        assertEquals(putMaterialResponse.categoryList, materialViewModel.categoryList)
    }

    @Test
    fun putMaterialSubscribe_loadingObservableIsTrue() {
        // Arrange
        stubGetCategoriesResponse()
        val delayer = PublishSubject.create<Void>()
        `when`(materialRepositoryMock.putMaterial(any()))
            .thenReturn(Single.just(putMaterialResponse).delaySubscription(delayer))

        // Act
        val materialViewModel = newMaterialViewModel()
        materialViewModel.updateMaterial(materialId, materialName)

        // Assert
        assertEquals(true, materialViewModel.loadingObservable.value)
    }

    @Test
    fun putMaterialFinally_loadingObservableIsFalse() {
        // Arrange
        stubGetCategoriesAndPutMaterialResponse()

        // Act
        val materialViewModel = newMaterialViewModel()
        materialViewModel.updateMaterial(materialId, materialName)

        // Assert
        assertEquals(false, materialViewModel.loadingObservable.value)
    }

    @Test
    fun putMaterialFailed_setRepositoryErrorObservable() {
        // Arrange
        stubGetCategoriesResponse()
        val throwableMock = mock(Throwable::class.java)
        `when`(materialRepositoryMock.putMaterial(any())).thenReturn(Single.error(throwableMock))
        `when`(networkUtils.getErrorMessage(throwableMock)).thenReturn(error)

        // Act
        val materialViewModel = newMaterialViewModel()
        materialViewModel.updateMaterial(materialId, materialName)

        // Assert
        assertEquals(error, materialViewModel.repositoryErrorObservable.value)
    }

    @Test
    fun deleteMaterial_isInactive() {
        // Arrange
        stubGetCategoriesAndPutMaterialResponse()
        val captor = argumentCaptor<Material>()
        material.active = Constants.ACTIVE

        // Act
        val materialViewModel = newMaterialViewModel()
        materialViewModel.deleteMaterial(material)

        // Assert
        verify(materialRepositoryMock).putMaterial(captor.capture())
        assertEquals(Constants.INACTIVE, captor.firstValue.active)
    }

    @Test
    fun deleteMaterialSuccess_setNotificationObservable() {
        // Arrange
        stubGetCategoriesAndPutMaterialResponse()

        // Act
        val materialViewModel = newMaterialViewModel()
        materialViewModel.deleteMaterial(material)

        // Assert
        assertEquals(R.string.material_deleted, materialViewModel.notificationObservable.value)
    }

    @Test
    fun deleteMaterialSuccess_setCategoryList() {
        // Arrange
        stubGetCategoriesAndPutMaterialResponse()

        // Act
        val materialViewModel = newMaterialViewModel()
        materialViewModel.deleteMaterial(material)

        // Assert
        assertEquals(putMaterialResponse.categoryList, materialViewModel.categoryList)
    }

    @Test
    fun deleteMaterialSubscribe_loadingObservableIsTrue() {
        // Arrange
        stubGetCategoriesResponse()
        val delayer = PublishSubject.create<Void>()
        `when`(materialRepositoryMock.putMaterial(any()))
            .thenReturn(Single.just(putMaterialResponse).delaySubscription(delayer))

        // Act
        val materialViewModel = newMaterialViewModel()
        materialViewModel.deleteMaterial(material)

        // Assert
        assertEquals(true, materialViewModel.loadingObservable.value)
    }

    @Test
    fun deleteMaterialFinally_loadingObservableIsFalse() {
        // Arrange
        stubGetCategoriesAndPutMaterialResponse()

        // Act
        val materialViewModel = newMaterialViewModel()
        materialViewModel.deleteMaterial(material)

        // Assert
        assertEquals(false, materialViewModel.loadingObservable.value)
    }

    @Test
    fun deleteMaterialFailed_setRepositoryErrorObservable() {
        // Arrange
        stubGetCategoriesResponse()
        val throwableMock = mock(Throwable::class.java)
        `when`(materialRepositoryMock.putMaterial(any())).thenReturn(Single.error(throwableMock))
        `when`(networkUtils.getErrorMessage(throwableMock)).thenReturn(error)

        // Act
        val materialViewModel = newMaterialViewModel()
        materialViewModel.deleteMaterial(material)

        // Assert
        assertEquals(error, materialViewModel.repositoryErrorObservable.value)
    }

    @Test
    fun filterByCategoryName_setMaterialCategoryListObservable() {
        // Arrange
        stubGetCategoriesResponse()
        val category = getCategoriesResponse.categoryList.first()
        val query = category.name.substring(1, 4).toLowerCase()

        // Act
        val materialViewModel = newMaterialViewModel()
        materialViewModel.filterMaterialList(query)

        // Assert
        assertEquals(transformToMaterialCategory(arrayListOf(category)),
            materialViewModel.materialCategoryListObservable.value)
    }

    @Test
    fun filterBySpacedCategoryName_setMaterialCategoryListObservable() {
        // Arrange
        stubGetCategoriesResponse()
        val category = getCategoriesResponse.categoryList.first()
        val query = category.name.substring(1, 4).toLowerCase()
        val spacedQuery = " $query "

        // Act
        val materialViewModel = newMaterialViewModel()
        materialViewModel.filterMaterialList(spacedQuery)

        // Assert
        assertEquals(transformToMaterialCategory(arrayListOf(category)),
            materialViewModel.materialCategoryListObservable.value)
    }

    @Test
    fun filterByMaterialName_setMaterialCategoryListObservable() {
        // Arrange
        stubGetCategoriesResponse()
        val category = getCategoriesResponse.categoryList.first()
        val material = category.materialList.first()
        val query = material.name.substring(1, 4).toLowerCase()

        // Act
        val materialViewModel = newMaterialViewModel()
        materialViewModel.filterMaterialList(query)

        // Assert
        assertEquals(arrayListOf(MaterialCategory(category, material)),
            materialViewModel.materialCategoryListObservable.value)
    }

    @Test
    fun getAllCategoriesSuccess_repositorySuccessObservableIsTrue() {
        // Arrange
        stubGetCategoriesResponse()

        // Act
        val materialViewModel = newMaterialViewModel()

        // Assert
        assertEquals(true, materialViewModel.repositorySuccessObservable.value)
    }


}