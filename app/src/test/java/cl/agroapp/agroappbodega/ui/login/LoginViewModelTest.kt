package cl.agroapp.agroappbodega.ui.login

import android.content.SharedPreferences
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import cl.agroapp.agroappbodega.LocalTestUtils
import cl.agroapp.agroappbodega.R
import cl.agroapp.agroappbodega.data.model.LoginState
import cl.agroapp.agroappbodega.data.model.User
import cl.agroapp.agroappbodega.data.network.interceptor.AuthInterceptor
import cl.agroapp.agroappbodega.data.repository.LoginRepository
import cl.agroapp.agroappbodega.utils.Constants
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.argumentCaptor
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class LoginViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    private lateinit var loginRepositoryMock: LoginRepository
    @Mock
    private lateinit var sharedPrefsMock: SharedPreferences
    @Mock
    private lateinit var sharedPrefsEditorMock: SharedPreferences.Editor
    @Mock
    private lateinit var authInterceptorMock: AuthInterceptor
    private val networkUtils = LocalTestUtils.newNetworkUtils()
    private lateinit var loginViewModel: LoginViewModel

    private val username = "username"
    private val password = "password"
    private val empty = Constants.EMPTY_STRING
    private val userResponse = User(username, password)

    @Before
    fun setUp() {
        `when`(sharedPrefsMock.edit()).thenReturn(sharedPrefsEditorMock)
        loginViewModel = newLoginViewModel()
    }

    private fun newLoginViewModel() = LoginViewModel(loginRepositoryMock, networkUtils,
        sharedPrefsMock, authInterceptorMock)

    @Test
    fun emptyUsername_setNotificationObservable() {
        // Act
        loginViewModel.login(username = empty, password = password, rememberMe = false)

        // Assert
        assertEquals(R.string.bad_login_credentials, loginViewModel.notificationObservable.value)
    }

    @Test
    fun validCredentials_callRepositoryLogin() {
        // Arrange
        stubLoginResponse()
        val captor = argumentCaptor<User>()

        // Act
        loginViewModel.login(username = username, password = password, rememberMe = false)

        // Assert
        verify(loginRepositoryMock).login(captor.capture())
        assertEquals(User(username, password), captor.firstValue)
    }

    private fun stubLoginResponse() {
        `when`(loginRepositoryMock.login(any())).thenReturn(Single.just(userResponse))
    }

    @Test
    fun emptyPassword_setNotificationObservable() {
        // Act
        loginViewModel.login(username = username, password = empty, rememberMe = false)

        // Assert
        assertEquals(R.string.bad_login_credentials, loginViewModel.notificationObservable.value)
    }

    @Test
    fun spacedCredentials_trimmedCredentialsOnRepoLogin() {
        // Arrange
        stubLoginResponse()
        val spacedUser = " $username "
        val spacedPassword = " $password "
        val captor = argumentCaptor<User>()

        // Act
        loginViewModel.login(username = spacedUser, password = spacedPassword, rememberMe = false)

        // Assert
        verify(loginRepositoryMock).login(captor.capture())
        assertEquals(User(spacedUser.trim(), spacedPassword.trim()), captor.firstValue)
    }

    @Test
    fun loginSubscribe_loadingObservableIsTrue() {
        // Arrange
        val delayer = PublishSubject.create<Void>()
        `when`(loginRepositoryMock.login(any())).thenReturn(Single.just(userResponse)
            .delaySubscription(delayer))

        // Act
        loginViewModel.login(username, password, false)

        // Assert
        assertEquals(LoginState(true, R.string.login_in_progress),
            loginViewModel.loadingObservable.value)
    }

    @Test
    fun loginFinally_loadingObservableIsFalse() {
        // Arrange
        stubLoginResponse()

        // Act
        loginViewModel.login(username, password, false)

        // Assert
        assertEquals(LoginState(false, R.string.login), loginViewModel.loadingObservable.value)
    }

    @Test
    fun rememberMeIsTrueOnInit_setRememberMeObservable() {
        // Arrange
        `when`(sharedPrefsMock.getBoolean(Constants.SHARED_PREFS_REMEMBER_ME, false))
            .thenReturn(true)
        `when`(sharedPrefsMock.getString(Constants.SHARED_PREFS_USER, Constants.EMPTY_STRING))
            .thenReturn(username)
        `when`(sharedPrefsMock.getString(Constants.SHARED_PREFS_PASSWORD, Constants.EMPTY_STRING))
            .thenReturn(password)

        // Act
        val tempLoginViewModel = newLoginViewModel()

        // Assert
        assertEquals(User(username, password), tempLoginViewModel.rememberMeObservable.value)
    }

    @Test
    fun rememberMeIsFalseOnInit_rememberMeObservableIsNull() {
        // Arrange
        `when`(sharedPrefsMock.getBoolean(Constants.SHARED_PREFS_REMEMBER_ME, false))
            .thenReturn(false)

        // Act
        val tempLoginViewModel = newLoginViewModel()

        // Assert
        assertEquals(null, tempLoginViewModel.rememberMeObservable.value)
    }

    @Test
    fun loginSuccess_saveUserToSharedPreferences() {
        // Arrange
        stubLoginResponse()
        val inOrder = inOrder(sharedPrefsEditorMock)

        // Act
        loginViewModel.login(username, password, false)

        // Assert
        inOrder.verify(sharedPrefsEditorMock).putString(Constants.SHARED_PREFS_USER,
            userResponse.user)
        inOrder.verify(sharedPrefsEditorMock).putString(Constants.SHARED_PREFS_PASSWORD,
            userResponse.password)
        inOrder.verify(sharedPrefsEditorMock).apply()
    }

    @Test
    fun rememberMeIsTrueOnLogin_saveRememberMeToSharedPreferences() {
        // Arrange
        stubLoginResponse()
        val inOrder = inOrder(sharedPrefsEditorMock)

        // Act
        loginViewModel.login(username, password, true)

        // Assert
        inOrder.verify(sharedPrefsEditorMock).putBoolean(Constants.SHARED_PREFS_REMEMBER_ME, true)
        inOrder.verify(sharedPrefsEditorMock).apply()
    }

    @Test
    fun loginSuccess_callAuthInterceptorSetCredentials() {
        // Arrange
        stubLoginResponse()

        // Act
        loginViewModel.login(username, password, false)

        // Assert
        verify(authInterceptorMock).setCredentials(userResponse)
    }

    @Test
    fun loginFailed_setRepositoryErrorObservable() {
        // Arrange
        val error = "error"
        val throwableMock = mock(Throwable::class.java)
        `when`(loginRepositoryMock.login(any())).thenReturn(Single.error(throwableMock))
        `when`(networkUtils.getErrorMessage(throwableMock)).thenReturn(error)

        // Act
        loginViewModel.login(username, password, false)

        // Assert
        assertEquals(error, loginViewModel.repositoryErrorObservable.value)
    }

    @Test
    fun loginSuccess_repositorySuccessObservableIsTrue() {
        // Arrange
        stubLoginResponse()

        // Act
        loginViewModel.login(username, password, false)

        // Assert
        assertEquals(true, loginViewModel.repositorySuccessObservable.value)
    }

}