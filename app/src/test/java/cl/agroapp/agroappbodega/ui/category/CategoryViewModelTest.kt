package cl.agroapp.agroappbodega.ui.category

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import cl.agroapp.agroappbodega.LocalTestUtils
import cl.agroapp.agroappbodega.R
import cl.agroapp.agroappbodega.data.model.Category
import cl.agroapp.agroappbodega.data.network.model.CategoryResponse
import cl.agroapp.agroappbodega.data.repository.CategoryRepository
import cl.agroapp.agroappbodega.utils.Constants
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.argumentCaptor
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class CategoryViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    private lateinit var categoryRepositoryMock: CategoryRepository
    private val networkUtils = LocalTestUtils.newNetworkUtils()

    private val category1 = Category(name = "Category 1")
    private val category2 = Category(name = "Category 2")
    private val category3 = Category(name = "Category 3")
    private val getCategoriesResponse = CategoryResponse(arrayListOf(category1, category2))
    private val postCategoryResponse = CategoryResponse(arrayListOf(category2, category3))
    private val putCategoryResponse = CategoryResponse(arrayListOf(category3))

    private val categoryId = 1
    private val categoryName = "Category name"
    private val error = "error"
    private val category = Category(id = categoryId, name = categoryName)

    @Test
    fun getCategoriesSuccess_setCategoryListObservable() {
        // Arrange
        stubGetCategoriesResponse()

        // Act
        val categoryViewModel = newCategoryViewModel()

        // Assert
        assertEquals(getCategoriesResponse.categoryList,
            categoryViewModel.categoryListObservable.value)
    }

    private fun stubGetCategoriesResponse() {
        `when`(categoryRepositoryMock.getCategories())
            .thenReturn(Single.just(getCategoriesResponse))
    }

    private fun newCategoryViewModel() = CategoryViewModel(categoryRepositoryMock, networkUtils)

    @Test
    fun getCategoriesSubscribe_loadingObservableIsTrue() {
        // Arrange
        val delayer = PublishSubject.create<Void>()
        `when`(categoryRepositoryMock.getCategories())
            .thenReturn(Single.just(getCategoriesResponse).delaySubscription(delayer))

        // Act
        val categoryViewModel = newCategoryViewModel()

        // Assert
        assertEquals(true, categoryViewModel.loadingObservable.value)
    }

    @Test
    fun getCategoriesFinally_loadingObservableIsFalse() {
        // Arrange
        stubGetCategoriesResponse()

        // Act
        val categoryViewModel = newCategoryViewModel()

        // Assert
        assertEquals(false, categoryViewModel.loadingObservable.value)
    }

    @Test
    fun getCategoriesFailed_setRepositoryErrorObservable() {
        // Arrange
        val throwableMock = mock(Throwable::class.java)
        `when`(categoryRepositoryMock.getCategories()).thenReturn(Single.error(throwableMock))
        `when`(networkUtils.getErrorMessage(throwableMock)).thenReturn(error)

        // Act
        val categoryViewModel = newCategoryViewModel()

        // Assert
        assertEquals(error, categoryViewModel.repositoryErrorObservable.value)
    }

    @Test
    fun postCategorySuccess_setCategoryListObservable() {
        // Arrange
        stubGetAndPostCategoryResponse()

        // Act
        val categoryViewModel = newCategoryViewModel()
        categoryViewModel.newCategory(categoryName)

        // Assert
        assertEquals(postCategoryResponse.categoryList,
            categoryViewModel.categoryListObservable.value)
    }

    private fun stubGetAndPostCategoryResponse() {
        stubGetCategoriesResponse()
        `when`(categoryRepositoryMock.postCategory(any()))
            .thenReturn(Single.just(postCategoryResponse))
    }

    @Test
    fun spacedCategoryNameOnNewCategory_trimmedCategoryNameOnPostCategory() {
        // Arrange
        stubGetAndPostCategoryResponse()
        val spacedCategoryName = " $categoryName "
        val captor = argumentCaptor<Category>()

        // Act
        val categoryViewModel = newCategoryViewModel()
        categoryViewModel.newCategory(spacedCategoryName)

        // Assert
        verify(categoryRepositoryMock).postCategory(captor.capture())
        assertEquals(spacedCategoryName.trim(), captor.firstValue.name)
    }

    @Test
    fun postCategorySuccess_setNotificationObservable() {
        // Arrange
        stubGetAndPostCategoryResponse()

        // Act
        val categoryViewModel = newCategoryViewModel()
        categoryViewModel.newCategory(categoryName)

        // Assert
        assertEquals(R.string.category_created, categoryViewModel.notificationObservable.value)
    }

    @Test
    fun postCategorySubscribe_loadingObservableIsTrue() {
        // Arrange
        stubGetCategoriesResponse()
        val delayer = PublishSubject.create<Void>()
        `when`(categoryRepositoryMock.postCategory(any()))
            .thenReturn(Single.just(postCategoryResponse).delaySubscription(delayer))

        // Act
        val categoryViewModel = newCategoryViewModel()
        categoryViewModel.newCategory(categoryName)

        // Assert
        assertEquals(true, categoryViewModel.loadingObservable.value)
    }

    @Test
    fun postCategoryFinally_loadingObservableIsFalse() {
        // Arrange
        stubGetAndPostCategoryResponse()

        // Act
        val categoryViewModel = newCategoryViewModel()
        categoryViewModel.newCategory(categoryName)

        // Assert
        assertEquals(false, categoryViewModel.loadingObservable.value)
    }

    @Test
    fun postCategoryFailed_setRepositoryErrorObservable() {
        // Arrange
        stubGetCategoriesResponse()

        val throwableMock = mock(Throwable::class.java)
        `when`(categoryRepositoryMock.postCategory(any())).thenReturn(Single.error(throwableMock))
        `when`(networkUtils.getErrorMessage(throwableMock)).thenReturn(error)

        // Act
        val categoryViewModel = newCategoryViewModel()
        categoryViewModel.newCategory(categoryName)

        // Assert
        assertEquals(error, categoryViewModel.repositoryErrorObservable.value)
    }

    @Test
    fun updateCategory_callPutCategory() {
        // Arrange
        stubGetAndPutCategoryResponse()
        val captor = argumentCaptor<Category>()

        // Act
        val categoryViewModel = newCategoryViewModel()
        categoryViewModel.updateCategory(category.id, category.name)

        // Assert
        verify(categoryRepositoryMock).putCategory(captor.capture())
        assertEquals(category, captor.firstValue)
    }

    private fun stubGetAndPutCategoryResponse() {
        stubGetCategoriesResponse()
        `when`(categoryRepositoryMock.putCategory(any()))
            .thenReturn(Single.just(putCategoryResponse))
    }

    @Test
    fun putCategorySuccess_setCategoryListObservable() {
        // Arrange
        stubGetAndPutCategoryResponse()

        // Act
        val categoryViewModel = newCategoryViewModel()
        categoryViewModel.updateCategory(categoryId, categoryName)

        // Assert
        assertEquals(putCategoryResponse.categoryList,
            categoryViewModel.categoryListObservable.value)
    }

    @Test
    fun spacedCategoryNameOnUpdateCategory_trimmedCategoryNameOnPutCategory() {
        // Arrange
        stubGetAndPutCategoryResponse()
        val spacedCategoryName = " $categoryName "
        val captor = argumentCaptor<Category>()

        // Act
        val categoryViewModel = newCategoryViewModel()
        categoryViewModel.updateCategory(categoryId, spacedCategoryName)

        // Assert
        verify(categoryRepositoryMock).putCategory(captor.capture())
        assertEquals(spacedCategoryName.trim(), captor.firstValue.name)
    }

    @Test
    fun putCategorySuccess_setNotificationObservable() {
        // Arrange
        stubGetAndPutCategoryResponse()

        // Act
        val categoryViewModel = newCategoryViewModel()
        categoryViewModel.updateCategory(categoryId, categoryName)

        // Assert
        assertEquals(R.string.category_updated, categoryViewModel.notificationObservable.value)
    }

    @Test
    fun putCategorySubscribe_loadingObservableIsTrue() {
        // Arrange
        stubGetCategoriesResponse()
        val delayer = PublishSubject.create<Void>()
        `when`(categoryRepositoryMock.putCategory(any()))
            .thenReturn(Single.just(putCategoryResponse).delaySubscription(delayer))

        // Act
        val categoryViewModel = newCategoryViewModel()
        categoryViewModel.updateCategory(categoryId, categoryName)

        // Assert
        assertEquals(true, categoryViewModel.loadingObservable.value)
    }

    @Test
    fun putCategoryFinally_loadingObservableIsFalse() {
        // Arrange
        stubGetAndPutCategoryResponse()

        // Act
        val categoryViewModel = newCategoryViewModel()
        categoryViewModel.updateCategory(categoryId, categoryName)

        // Assert
        assertEquals(false, categoryViewModel.loadingObservable.value)
    }

    @Test
    fun putCategoryFailed_setRepositoryErrorObservable() {
        // Arrange
        stubGetCategoriesResponse()
        val throwableMock = mock(Throwable::class.java)
        `when`(categoryRepositoryMock.putCategory(any())).thenReturn(Single.error(throwableMock))
        `when`(networkUtils.getErrorMessage(throwableMock)).thenReturn(error)

        // Act
        val categoryViewModel = newCategoryViewModel()
        categoryViewModel.updateCategory(categoryId, categoryName)

        // Assert
        assertEquals(error, categoryViewModel.repositoryErrorObservable.value)
    }

    @Test
    fun deleteCategorySuccess_setCategoryListObservable() {
        // Arrange
        stubGetAndPutCategoryResponse()

        // Act
        val categoryViewModel = newCategoryViewModel()
        categoryViewModel.deleteCategory(category)

        // Assert
        assertEquals(putCategoryResponse.categoryList,
            categoryViewModel.categoryListObservable.value)
    }

    @Test
    fun deleteCategorySuccess_setNotificationObservable() {
        // Arrange
        stubGetAndPutCategoryResponse()

        // Act
        val categoryViewModel = newCategoryViewModel()
        categoryViewModel.deleteCategory(category)

        // Assert
        assertEquals(R.string.category_deleted, categoryViewModel.notificationObservable.value)
    }

    @Test
    fun deleteCategory_isInactive() {
        // Arrange
        stubGetAndPutCategoryResponse()
        category.active = Constants.ACTIVE

        // Act
        val categoryViewModel = newCategoryViewModel()
        categoryViewModel.deleteCategory(category)

        // Assert
        assertEquals(Constants.INACTIVE, category.active)
    }

    @Test
    fun deleteCategorySubscribe_loadingObservableIsTrue() {
        // Arrange
        stubGetCategoriesResponse()
        val delayer = PublishSubject.create<Void>()
        `when`(categoryRepositoryMock.putCategory(any()))
            .thenReturn(Single.just(putCategoryResponse).delaySubscription(delayer))

        // Act
        val categoryViewModel = newCategoryViewModel()
        categoryViewModel.deleteCategory(category)

        // Assert
        assertEquals(true, categoryViewModel.loadingObservable.value)
    }

    @Test
    fun deleteCategoryFinally_loadingObservableIsFalse() {
        // Arrange
        stubGetAndPutCategoryResponse()

        // Act
        val categoryViewModel = newCategoryViewModel()
        categoryViewModel.deleteCategory(category)

        // Assert
        assertEquals(false, categoryViewModel.loadingObservable.value)
    }

    @Test
    fun deleteCategoryFailed_setRepositoryErrorObservable() {
        // Arrange
        stubGetCategoriesResponse()
        val throwableMock = mock(Throwable::class.java)
        `when`(categoryRepositoryMock.putCategory(any())).thenReturn(Single.error(throwableMock))
        `when`(networkUtils.getErrorMessage(throwableMock)).thenReturn(error)

        // Act
        val categoryViewModel = newCategoryViewModel()
        categoryViewModel.deleteCategory(category)

        // Assert
        assertEquals(error, categoryViewModel.repositoryErrorObservable.value)
    }

}