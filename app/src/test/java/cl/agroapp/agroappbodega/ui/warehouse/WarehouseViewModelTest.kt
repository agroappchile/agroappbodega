package cl.agroapp.agroappbodega.ui.warehouse

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import cl.agroapp.agroappbodega.LocalTestUtils
import cl.agroapp.agroappbodega.R
import cl.agroapp.agroappbodega.data.model.Warehouse
import cl.agroapp.agroappbodega.data.network.model.WarehouseResponse
import cl.agroapp.agroappbodega.data.repository.WarehouseRepository
import cl.agroapp.agroappbodega.utils.Constants
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.argumentCaptor
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class WarehouseViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    private lateinit var warehouseRepositoryMock: WarehouseRepository
    private val networkUtils = LocalTestUtils.newNetworkUtils()

    private val warehouse1 = Warehouse(name = "Warehouse 1")
    private val warehouse2 = Warehouse(name = "Warehouse 2")
    private val warehouse3 = Warehouse(name = "Warehouse 3")
    private val getWarehousesResponse = WarehouseResponse(arrayListOf(warehouse1, warehouse2))
    private val postWarehouseResponse = WarehouseResponse(arrayListOf(warehouse2, warehouse3))
    private val putWarehouseResponse = WarehouseResponse(arrayListOf(warehouse3))

    private val warehouseId = 1
    private val warehouseName = "Category name"
    private val error = "error"
    private val warehouse = Warehouse(id = warehouseId, name = warehouseName)

    @Test
    fun getWarehousesSuccess_setWarehouseListObservable() {
        // Arrange
        stubGetWarehousesResponse()

        // Act
        val warehouseViewModel = newWarehouseViewModel()

        // Assert
        assertEquals(getWarehousesResponse.warehouseList,
            warehouseViewModel.warehouseListObservable.value)
    }

    private fun stubGetWarehousesResponse() {
        `when`(warehouseRepositoryMock.getWarehouses())
            .thenReturn(Single.just(getWarehousesResponse))
    }

    private fun newWarehouseViewModel() = WarehouseViewModel(warehouseRepositoryMock, networkUtils)

    @Test
    fun getWarehousesSubscribe_loadingObservableIsTrue() {
        // Arrange
        val delayer = PublishSubject.create<Void>()
        `when`(warehouseRepositoryMock.getWarehouses())
            .thenReturn(Single.just(getWarehousesResponse).delaySubscription(delayer))

        // Act
        val warehouseViewModel = newWarehouseViewModel()

        // Assert
        assertEquals(true, warehouseViewModel.loadingObservable.value)
    }

    @Test
    fun getWarehousesFinally_loadingObservableIsFalse() {
        // Arrange
        stubGetWarehousesResponse()

        // Act
        val warehouseViewModel = newWarehouseViewModel()

        // Assert
        assertEquals(false, warehouseViewModel.loadingObservable.value)
    }

    @Test
    fun getWarehousesFailed_setRepositoryErrorObservable() {
        // Arrange
        val throwableMock = mock(Throwable::class.java)
        `when`(warehouseRepositoryMock.getWarehouses()).thenReturn(Single.error(throwableMock))
        `when`(networkUtils.getErrorMessage(throwableMock)).thenReturn(error)

        // Act
        val warehouseViewModel = newWarehouseViewModel()

        // Assert
        assertEquals(error, warehouseViewModel.repositoryErrorObservable.value)
    }

    @Test
    fun postWarehouseSuccess_setWarehouseListObservable() {
        // Arrange
        stubGetAndPostCategoryResponse()

        // Act
        val warehouseViewModel = newWarehouseViewModel()
        warehouseViewModel.newWarehouse(warehouseName)

        // Assert
        assertEquals(postWarehouseResponse.warehouseList,
            warehouseViewModel.warehouseListObservable.value)
    }

    private fun stubGetAndPostCategoryResponse() {
        stubGetWarehousesResponse()
        `when`(warehouseRepositoryMock.postWarehouse(any()))
            .thenReturn(Single.just(postWarehouseResponse))
    }

    @Test
    fun spacedNameOnNewWarehouse_trimmedNameOnPostWarehouse() {
        // Arrange
        stubGetAndPostCategoryResponse()
        val spacedName = " $warehouseName "
        val captor = argumentCaptor<Warehouse>()

        // Act
        val warehouseViewModel = newWarehouseViewModel()
        warehouseViewModel.newWarehouse(spacedName)

        // Assert
        verify(warehouseRepositoryMock).postWarehouse(captor.capture())
        assertEquals(spacedName.trim(), captor.firstValue.name)
    }

    @Test
    fun postWarehouseSuccess_setNotificationObservable() {
        // Arrange
        stubGetAndPostCategoryResponse()

        // Act
        val warehouseViewModel = newWarehouseViewModel()
        warehouseViewModel.newWarehouse(warehouseName)

        // Assert
        assertEquals(R.string.warehouse_created, warehouseViewModel.notificationObservable.value)
    }

    @Test
    fun postWarehouseSubscribe_loadingObservableIsTrue() {
        // Arrange
        stubGetWarehousesResponse()
        val delayer = PublishSubject.create<Void>()
        `when`(warehouseRepositoryMock.postWarehouse(any()))
            .thenReturn(Single.just(postWarehouseResponse).delaySubscription(delayer))

        // Act
        val warehouseViewModel = newWarehouseViewModel()
        warehouseViewModel.newWarehouse(warehouseName)

        // Assert
        assertEquals(true, warehouseViewModel.loadingObservable.value)
    }

    @Test
    fun postWarehouseFinally_loadingObservableIsFalse() {
        // Arrange
        stubGetAndPostCategoryResponse()

        // Act
        val warehouseViewModel = newWarehouseViewModel()
        warehouseViewModel.newWarehouse(warehouseName)

        // Assert
        assertEquals(false, warehouseViewModel.loadingObservable.value)
    }

    @Test
    fun postWarehouseFailed_setRepositoryErrorObservable() {
        // Arrange
        stubGetWarehousesResponse()
        val throwableMock = mock(Throwable::class.java)
        `when`(warehouseRepositoryMock.postWarehouse(any())).thenReturn(Single.error(throwableMock))
        `when`(networkUtils.getErrorMessage(throwableMock)).thenReturn(error)

        // Act
        val warehouseViewModel = newWarehouseViewModel()
        warehouseViewModel.newWarehouse(warehouseName)

        // Assert
        assertEquals(error, warehouseViewModel.repositoryErrorObservable.value)
    }

    @Test
    fun updateWarehouse_callPutWarehouse() {
        // Arrange
        stubGetAndPutCategoryResponse()
        val captor = argumentCaptor<Warehouse>()

        // Act
        val warehouseViewModel = newWarehouseViewModel()
        warehouseViewModel.updateWarehouse(warehouse.id, warehouse.name)

        // Assert
        verify(warehouseRepositoryMock).putWarehouse(captor.capture())
        assertEquals(warehouse, captor.firstValue)
    }

    private fun stubGetAndPutCategoryResponse() {
        stubGetWarehousesResponse()
        `when`(warehouseRepositoryMock.putWarehouse(any()))
            .thenReturn(Single.just(putWarehouseResponse))
    }

    @Test
    fun putWarehouseSuccess_setWarehouseListObservable() {
        // Arrange
        stubGetAndPutCategoryResponse()

        // Act
        val warehouseViewModel = newWarehouseViewModel()
        warehouseViewModel.updateWarehouse(warehouseId, warehouseName)

        // Assert
        assertEquals(putWarehouseResponse.warehouseList,
            warehouseViewModel.warehouseListObservable.value)
    }

    @Test
    fun spacedNameOnUpdateWarehouse_trimmedNameOnPutWarehouse() {
        // Arrange
        stubGetAndPutCategoryResponse()
        val spacedName = " $warehouseName "
        val captor = argumentCaptor<Warehouse>()

        // Act
        val warehouseViewModel = newWarehouseViewModel()
        warehouseViewModel.updateWarehouse(warehouseId, spacedName)

        // Assert
        verify(warehouseRepositoryMock).putWarehouse(captor.capture())
        assertEquals(spacedName.trim(), captor.firstValue.name)
    }

    @Test
    fun putWarehouseSuccess_setNotificationObservable() {
        // Arrange
        stubGetAndPutCategoryResponse()

        // Act
        val warehouseViewModel = newWarehouseViewModel()
        warehouseViewModel.updateWarehouse(warehouseId, warehouseName)

        // Assert
        assertEquals(R.string.warehouse_updated, warehouseViewModel.notificationObservable.value)
    }

    @Test
    fun putWarehouseSubscribe_loadingObservableIsTrue() {
        // Arrange
        stubGetWarehousesResponse()
        val delayer = PublishSubject.create<Void>()
        `when`(warehouseRepositoryMock.putWarehouse(any()))
            .thenReturn(Single.just(putWarehouseResponse).delaySubscription(delayer))

        // Act
        val warehouseViewModel = newWarehouseViewModel()
        warehouseViewModel.updateWarehouse(warehouseId, warehouseName)

        // Assert
        assertEquals(true, warehouseViewModel.loadingObservable.value)
    }

    @Test
    fun putWarehouseFinally_loadingObservableIsFalse() {
        // Arrange
        stubGetAndPutCategoryResponse()

        // Act
        val warehouseViewModel = newWarehouseViewModel()
        warehouseViewModel.updateWarehouse(warehouseId, warehouseName)

        // Assert
        assertEquals(false, warehouseViewModel.loadingObservable.value)
    }

    @Test
    fun putWarehouseFailed_setRepositoryErrorObservable() {
        // Arrange
        stubGetWarehousesResponse()
        val throwableMock = mock(Throwable::class.java)
        `when`(warehouseRepositoryMock.putWarehouse(any())).thenReturn(Single.error(throwableMock))
        `when`(networkUtils.getErrorMessage(throwableMock)).thenReturn(error)

        // Act
        val warehouseViewModel = newWarehouseViewModel()
        warehouseViewModel.updateWarehouse(warehouseId, warehouseName)

        // Assert
        assertEquals(error, warehouseViewModel.repositoryErrorObservable.value)
    }

    @Test
    fun deleteWarehouse_callPutWarehouse() {
        // Arrange
        stubGetAndPutCategoryResponse()
        val captor = argumentCaptor<Warehouse>()

        // Act
        val warehouseViewModel = newWarehouseViewModel()
        warehouseViewModel.deleteWarehouse(warehouse)

        // Assert
        verify(warehouseRepositoryMock).putWarehouse(captor.capture())
        assertEquals(warehouse, captor.firstValue)
    }

    @Test
    fun deleteWarehouse_isInactive() {
        // Arrange
        stubGetAndPutCategoryResponse()
        warehouse.active = Constants.ACTIVE

        // Act
        val warehouseViewModel = newWarehouseViewModel()
        warehouseViewModel.deleteWarehouse(warehouse)

        // Assert
        assertEquals(Constants.INACTIVE, warehouse.active)
    }

    @Test
    fun deleteWarehouseSuccess_setWarehouseListObservable() {
        // Arrange
        stubGetAndPutCategoryResponse()

        // Act
        val warehouseViewModel = newWarehouseViewModel()
        warehouseViewModel.deleteWarehouse(warehouse)

        // Assert
        assertEquals(putWarehouseResponse.warehouseList,
            warehouseViewModel.warehouseListObservable.value)
    }

    @Test
    fun deleteWarehouseSuccess_setNotificationObservable() {
        // Arrange
        stubGetAndPutCategoryResponse()

        // Act
        val warehouseViewModel = newWarehouseViewModel()
        warehouseViewModel.deleteWarehouse(warehouse)

        // Assert
        assertEquals(R.string.warehouse_deleted, warehouseViewModel.notificationObservable.value)
    }

    @Test
    fun deleteWarehouseSubscribe_loadingObservableIsTrue() {
        // Arrange
        stubGetWarehousesResponse()
        val delayer = PublishSubject.create<Void>()
        `when`(warehouseRepositoryMock.putWarehouse(any()))
            .thenReturn(Single.just(putWarehouseResponse).delaySubscription(delayer))

        // Act
        val warehouseViewModel = newWarehouseViewModel()
        warehouseViewModel.deleteWarehouse(warehouse)

        // Assert
        assertEquals(true, warehouseViewModel.loadingObservable.value)
    }

    @Test
    fun deleteWarehouseFinally_loadingObservableIsFalse() {
        // Arrange
        stubGetAndPutCategoryResponse()

        // Act
        val warehouseViewModel = newWarehouseViewModel()
        warehouseViewModel.deleteWarehouse(warehouse)

        // Assert
        assertEquals(false, warehouseViewModel.loadingObservable.value)
    }

    @Test
    fun deleteWarehouseFailed_setRepositoryErrorObservable() {
        // Arrange
        stubGetWarehousesResponse()
        val throwableMock = mock(Throwable::class.java)
        `when`(warehouseRepositoryMock.putWarehouse(any())).thenReturn(Single.error(throwableMock))
        `when`(networkUtils.getErrorMessage(throwableMock)).thenReturn(error)

        // Act
        val warehouseViewModel = newWarehouseViewModel()
        warehouseViewModel.deleteWarehouse(warehouse)

        // Assert
        assertEquals(error, warehouseViewModel.repositoryErrorObservable.value)
    }

}