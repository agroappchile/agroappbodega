package cl.agroapp.agroappbodega.ui.movement

import android.app.Activity
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import cl.agroapp.agroappbodega.LocalTestUtils
import cl.agroapp.agroappbodega.R
import cl.agroapp.agroappbodega.data.model.Entry
import cl.agroapp.agroappbodega.data.model.Movement
import cl.agroapp.agroappbodega.data.model.Transfer
import cl.agroapp.agroappbodega.data.model.Withdraw
import cl.agroapp.agroappbodega.data.network.model.MovementResponse
import cl.agroapp.agroappbodega.data.repository.MovementRepository
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.argumentCaptor
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class MovementViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    private lateinit var movementRepositoryMock: MovementRepository
    private val networkUtils = LocalTestUtils.newNetworkUtils()

    private val movementResponse = MovementResponse(arrayListOf(Entry(), Withdraw(), Transfer()))
    private val newMovementResponse = MovementResponse(arrayListOf(Transfer(), Withdraw()))
    private val deleteMovementResponse = MovementResponse(arrayListOf(Withdraw(), Entry(), Entry()))

    @Mock
    private lateinit var throwableMock: Throwable
    private val error = "error"
    private val entry = Entry(id = 1)

    @Test
    fun getMovementsSuccess_setMovementListObservable() {
        // Arrange
        stubGetMovementsResponse()

        // Act
        val movementViewModel = newMovementViewModel()

        // Assert
        assertEquals(movementResponse.movementList, movementViewModel.movementListObservable.value)
    }

    private fun stubGetMovementsResponse() {
        `when`(movementRepositoryMock.getMovements()).thenReturn(Single.just(movementResponse))
    }

    private fun newMovementViewModel() = MovementViewModel(movementRepositoryMock, networkUtils)

    @Test
    fun getMovementsSubscribe_loadingObservableIsTrue() {
        // Arrange
        val delayer = PublishSubject.create<Void>()
        `when`(movementRepositoryMock.getMovements()).thenReturn(Single.just(movementResponse)
            .delaySubscription(delayer))

        // Act
        val movementViewModel = newMovementViewModel()

        // Assert
        assertEquals(true, movementViewModel.loadingObservable.value)
    }

    @Test
    fun getMovementsFinally_loadingObservableIsFalse() {
        // Arrange
        stubGetMovementsResponse()

        // Act
        val movementViewModel = newMovementViewModel()

        // Assert
        assertEquals(false, movementViewModel.loadingObservable.value)
    }

    @Test
    fun getMovementsFailed_setRepositoryErrorObservable() {
        // Arrange
        `when`(movementRepositoryMock.getMovements()).thenReturn(Single.error(throwableMock))
        `when`(networkUtils.getErrorMessage(throwableMock)).thenReturn(error)

        // Act
        val movementViewModel = newMovementViewModel()

        // Assert
        assertEquals(error, movementViewModel.repositoryErrorObservable.value)
    }

    @Test
    fun checkIfMovementCreatedWithOkResult_setMovementListObservable() {
        // Arrange
        stubGetMovementsResponse()

        // Act
        val movementViewModel = newMovementViewModel()
        stubNewMovementResponse()
        movementViewModel.checkIfMovementWasCreated(Activity.RESULT_OK)

        // Assert
        assertEquals(newMovementResponse.movementList,
            movementViewModel.movementListObservable.value)
    }

    private fun stubNewMovementResponse() {
        `when`(movementRepositoryMock.getMovements()).thenReturn(Single.just(newMovementResponse))
    }

    @Test
    fun checkIfMovementCreatedWithOkResult_setNotificationObservable() {
        // Arrange
        stubGetMovementsResponse()

        // Act
        val movementViewModel = newMovementViewModel()
        stubNewMovementResponse()
        movementViewModel.checkIfMovementWasCreated(Activity.RESULT_OK)

        // Assert
        assertEquals(R.string.movement_created, movementViewModel.notificationObservable.value)
    }

    @Test
    fun checkIfMovementCreatedWithoutOkResult_doNothing() {
        // Arrange
        stubGetMovementsResponse()

        // Act
        val movementViewModel = newMovementViewModel()
        movementViewModel.checkIfMovementWasCreated(Activity.RESULT_CANCELED)

        // Assert
        verify(movementRepositoryMock, times(1)).getMovements()
        assertEquals(null, movementViewModel.notificationObservable.value)
    }

    @Test
    fun deleteMovement_callDeleteMovement() {
        // Arrange
        stubGetAndDeleteMovementResponse()
        val captor = argumentCaptor<Movement>()

        // Act
        val movementViewModel = newMovementViewModel()
        movementViewModel.deleteMovement(entry)

        // Assert
        verify(movementRepositoryMock).deleteMovement(captor.capture())
        assertEquals(entry, captor.firstValue)
    }

    private fun stubGetAndDeleteMovementResponse() {
        stubGetMovementsResponse()
        `when`(movementRepositoryMock.deleteMovement(any())).thenReturn(
            Single.just(deleteMovementResponse))
    }

    @Test
    fun deleteMovementSuccess_setMovementListObservable() {
        // Arrange
        stubGetAndDeleteMovementResponse()

        // Act
        val movementViewModel = newMovementViewModel()
        movementViewModel.deleteMovement(entry)

        // Assert
        assertEquals(deleteMovementResponse.movementList, movementViewModel.movementListObservable
            .value)
    }

    @Test
    fun deleteMovementSuccess_setNotificationObservable() {
        // Arrange
        stubGetAndDeleteMovementResponse()

        // Act
        val movementViewModel = newMovementViewModel()
        movementViewModel.deleteMovement(entry)

        // Assert
        assertEquals(R.string.movement_deleted, movementViewModel.notificationObservable.value)
    }

    @Test
    fun deleteMovementSubscribe_loadingObservableIsTrue() {
        // Arrange
        stubGetMovementsResponse()
        val delayer = PublishSubject.create<Void>()
        `when`(movementRepositoryMock.deleteMovement(any())).thenReturn(Single.just(
            deleteMovementResponse).delaySubscription(delayer))

        // Act
        val movementViewModel = newMovementViewModel()
        movementViewModel.deleteMovement(entry)

        // Assert
        assertEquals(true, movementViewModel.loadingObservable.value)
    }

    @Test
    fun deleteMovementFinally_loadingObservableIsFalse() {
        // Arrange
        stubGetAndDeleteMovementResponse()

        // Act
        val movementViewModel = newMovementViewModel()
        movementViewModel.deleteMovement(entry)

        // Assert
        assertEquals(false, movementViewModel.loadingObservable.value)
    }

    @Test
    fun deleteMovementFailed_setRepositoryErrorObservable() {
        // Arrange
        stubGetMovementsResponse()
        `when`(movementRepositoryMock.deleteMovement(any())).thenReturn(Single.error(throwableMock))
        `when`(networkUtils.getErrorMessage(throwableMock)).thenReturn(error)

        // Act
        val movementViewModel = newMovementViewModel()
        movementViewModel.deleteMovement(entry)

        // Assert
        assertEquals(error, movementViewModel.repositoryErrorObservable.value)
    }

}