package cl.agroapp.agroappbodega.ui.entry

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import cl.agroapp.agroappbodega.LocalTestUtils
import cl.agroapp.agroappbodega.R
import cl.agroapp.agroappbodega.data.model.Category
import cl.agroapp.agroappbodega.data.model.Entry
import cl.agroapp.agroappbodega.data.model.Material
import cl.agroapp.agroappbodega.data.model.Warehouse
import cl.agroapp.agroappbodega.data.network.model.CategoryResponse
import cl.agroapp.agroappbodega.data.network.model.WarehouseResponse
import cl.agroapp.agroappbodega.data.repository.CategoryRepository
import cl.agroapp.agroappbodega.data.repository.EntryRepository
import cl.agroapp.agroappbodega.data.repository.WarehouseRepository
import cl.agroapp.agroappbodega.utils.MaterialCategoryUtils.Companion.sortByMaterialName
import cl.agroapp.agroappbodega.utils.MaterialCategoryUtils.Companion.transformToMaterialCategory
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.argumentCaptor
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class EntryViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    private lateinit var warehouseRepositoryMock: WarehouseRepository
    @Mock
    private lateinit var categoryRepositoryMock: CategoryRepository
    @Mock
    private lateinit var entryRepositoryMock: EntryRepository
    private val networkUtils = LocalTestUtils.newNetworkUtils()

    private val warehouseResponse = WarehouseResponse(arrayListOf(
        Warehouse(name = "Warehouse 1"),
        Warehouse(name = "Warehouse 2")
    ))
    private val categoryResponse = CategoryResponse(arrayListOf(
        Category(name = "MEDICINES", materialList = arrayListOf(
            Material(name = "PARACETAMOL"),
            Material(name = "IBUPROFEN")
        )),
        Category(name = "TOOLS", materialList = arrayListOf(
            Material(name = "HAMMER")
        ))
    ))

    private val materialId = 1
    private val warehouseId = 2
    private val quantity = "3.5"
    private val observation = "Some observations"
    private val error = "error"

    @Test
    fun getWarehousesAndMaterialSuccess_setWarehouseAndMaterialListObservable() {
        // Arrange
        stubGetWarehousesAndMaterialsResponse()

        // Act
        val entryViewModel = newEntryViewModel()
        val expectedMaterialList = transformToMaterialCategory(categoryResponse.categoryList)
        sortByMaterialName(expectedMaterialList)

        // Assert
        assertEquals(warehouseResponse.warehouseList, entryViewModel.warehouseListObservable.value)
        assertEquals(expectedMaterialList, entryViewModel.materialListObservable.value)
    }

    private fun stubGetWarehousesAndMaterialsResponse() {
        `when`(warehouseRepositoryMock.getWarehouses()).thenReturn(Single.just(warehouseResponse))
        stubGetMaterialsResponse()
    }

    private fun stubGetMaterialsResponse() {
        `when`(categoryRepositoryMock.getCategories()).thenReturn(Single.just(categoryResponse))
    }

    private fun newEntryViewModel() = EntryViewModel(warehouseRepositoryMock,
        categoryRepositoryMock, entryRepositoryMock, networkUtils)

    @Test
    fun getWarehousesAndMaterialsSubscribe_loadingObservableIsTrue() {
        // Arrange
        stubGetMaterialsResponse()
        val delayer = PublishSubject.create<Void>()
        `when`(warehouseRepositoryMock.getWarehouses()).thenReturn(Single.just(warehouseResponse)
            .delaySubscription(delayer))

        // Act
        val entryViewModel = newEntryViewModel()

        // Assert
        assertEquals(true, entryViewModel.loadingObservable.value)
    }

    @Test
    fun getWarehousesAndMaterialsFinally_loadingObservableIsFalse() {
        // Arrange
        stubGetWarehousesAndMaterialsResponse()

        // Act
        val entryViewModel = newEntryViewModel()

        // Assert
        assertEquals(false, entryViewModel.loadingObservable.value)
    }

    @Test
    fun getWarehousesAndMaterialsFailed_setRepositoryErrorObservable() {
        // Arrange
        stubGetMaterialsResponse()
        val throwableMock = mock(Throwable::class.java)
        `when`(warehouseRepositoryMock.getWarehouses()).thenReturn(Single.error(throwableMock))
        `when`(networkUtils.getErrorMessage(throwableMock)).thenReturn(error)

        // Act
        val entryViewModel = newEntryViewModel()

        // Assert
        assertEquals(error, entryViewModel.repositoryErrorObservable.value)
    }

    @Test
    fun invalidQuantity_setNotificationObservable() {
        // Arrange
        stubGetWarehousesAndMaterialsResponse()

        // Act
        val entryViewModel = newEntryViewModel()
        entryViewModel.newEntry(
            materialId = materialId,
            warehouseId = warehouseId,
            quantity = "0",
            observation = observation
        )

        // Assert
        assertEquals(R.string.invalid_quantity, entryViewModel.notificationObservable.value)
    }

    @Test
    fun validInputs_callPostEntry() {
        // Arrange
        stubGetWarehousesAndMaterialsResponse()
        stubPostEntryResponse()
        val captor = argumentCaptor<Entry>()

        // Act
        val entryViewModel = newEntryViewModel()
        entryViewModel.newEntry(
            materialId = materialId,
            warehouseId = warehouseId,
            quantity = quantity,
            observation = observation
        )

        // Assert
        verify(entryRepositoryMock).postEntry(captor.capture())
        val entry = captor.firstValue
        assertEquals(materialId, entry.material.id)
        assertEquals(warehouseId, entry.warehouse.id)
        assertEquals(quantity.toDouble(), entry.quantity, 0.0)
        assertEquals(observation, entry.observation)
    }

    private fun stubPostEntryResponse() {
        `when`(entryRepositoryMock.postEntry(any())).thenReturn(Completable.complete())
    }

    @Test
    fun spacedObservationOnNewEntry_trimmedObservationOnPostEntry() {
        // Arrange
        stubGetWarehousesAndMaterialsResponse()
        stubPostEntryResponse()
        val captor = argumentCaptor<Entry>()
        val spacedObservation = " $observation "

        // Act
        val entryViewModel = newEntryViewModel()
        entryViewModel.newEntry(
            materialId = materialId,
            warehouseId = warehouseId,
            quantity = quantity,
            observation = spacedObservation
        )

        // Assert
        verify(entryRepositoryMock).postEntry(captor.capture())
        assertEquals(spacedObservation.trim(), captor.firstValue.observation)
    }

    @Test
    fun postEntrySuccess_repositorySuccessObservableIsTrue() {
        // Arrange
        stubGetWarehousesAndMaterialsResponse()
        stubPostEntryResponse()

        // Act
        val entryViewModel = newEntryViewModel()
        entryViewModel.newEntry(
            materialId = materialId,
            warehouseId = warehouseId,
            quantity = quantity,
            observation = observation
        )

        // Assert
        assertEquals(true, entryViewModel.repositorySuccessObservable.value)
    }

    @Test
    fun postEntrySubscribe_loadingObservableIsTrue() {
        // Arrange
        stubGetWarehousesAndMaterialsResponse()
        val delayer = PublishSubject.create<Void>()
        `when`(entryRepositoryMock.postEntry(any())).thenReturn(delayer.ignoreElements())

        // Act
        val entryViewModel = newEntryViewModel()
        entryViewModel.newEntry(
            materialId = materialId,
            warehouseId = warehouseId,
            quantity = quantity,
            observation = observation
        )

        // Assert
        assertEquals(true, entryViewModel.loadingObservable.value)
    }

    @Test
    fun postEntryFinally_loadingObservableIsFalse() {
        // Arrange
        stubGetWarehousesAndMaterialsResponse()
        stubPostEntryResponse()

        // Act
        val entryViewModel = newEntryViewModel()
        entryViewModel.newEntry(
            materialId = materialId,
            warehouseId = warehouseId,
            quantity = quantity,
            observation = observation
        )

        // Assert
        assertEquals(false, entryViewModel.loadingObservable.value)
    }

    @Test
    fun postEntryFailed_setRepositoryErrorObservable() {
        // Arrange
        stubGetWarehousesAndMaterialsResponse()
        val throwableMock = mock(Throwable::class.java)
        `when`(entryRepositoryMock.postEntry(any())).thenReturn(Completable.error(throwableMock))
        `when`(networkUtils.getErrorMessage(throwableMock)).thenReturn(error)

        // Act
        val entryViewModel = newEntryViewModel()
        entryViewModel.newEntry(
            materialId = materialId,
            warehouseId = warehouseId,
            quantity = quantity,
            observation = observation
        )

        // Assert
        assertEquals(error, entryViewModel.repositoryErrorObservable.value)
    }

}