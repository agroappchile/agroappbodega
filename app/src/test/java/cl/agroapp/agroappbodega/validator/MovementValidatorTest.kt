package cl.agroapp.agroappbodega.validator

import cl.agroapp.agroappbodega.validator.MovementValidator.Companion.isQuantityValid
import org.junit.Assert.assertEquals
import org.junit.Test

class MovementValidatorTest {

    @Test
    fun isString_returnFalse() {
        // Assert
        assertEquals(false, isQuantityValid("some string"))
    }

    @Test
    fun negativeNumber_returnFalse() {
        // Assert
        assertEquals(false, isQuantityValid("-1"))
    }

    @Test
    fun positiveNumber_returnTrue() {
        // Assert
        assertEquals(true, isQuantityValid("1.5"))
    }

    @Test
    fun zero_returnFalse() {
        // Assert
        assertEquals(false, isQuantityValid("0"))
    }

}