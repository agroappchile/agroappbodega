package cl.agroapp.agroappbodega.utils

import cl.agroapp.agroappbodega.data.model.Category
import cl.agroapp.agroappbodega.data.model.Material
import cl.agroapp.agroappbodega.data.model.MaterialCategory
import cl.agroapp.agroappbodega.utils.MaterialCategoryUtils.Companion.sortByMaterialName
import cl.agroapp.agroappbodega.utils.MaterialCategoryUtils.Companion.transformToMaterialCategory
import org.junit.Assert.assertEquals
import org.junit.Test

class MaterialCategoryUtilsTest {

    @Test
    fun transformCategoryList_returnMaterialCategoryList() {
        // Arrange
        val material1 = Material(id = 1)
        val material2 = Material(id = 2)
        val material3 = Material(id = 3)
        val category1 = Category(id = 1, materialList = arrayListOf(material1, material2))
        val category2 = Category(id = 2, materialList = arrayListOf(material3))
        val categoryList = arrayListOf(category1, category2)
        val expectedList = arrayListOf(
            MaterialCategory(category1, material1),
            MaterialCategory(category1, material2),
            MaterialCategory(category2, material3)
        )

        // Act
        val actualList = transformToMaterialCategory(categoryList)

        // Assert
        assertEquals(expectedList, actualList)
    }

    @Test
    fun sortByMaterialName_returnSortedList() {
        // Arrange
        val category = Category()
        val materialCategory1 = MaterialCategory(category, Material(name = "IbuPROFen"))
        val materialCategory2 = MaterialCategory(category, Material(name = "rIcE"))
        val materialCategory3 = MaterialCategory(category, Material(name = "hamMER"))
        val actualList = arrayListOf(
            materialCategory1,
            materialCategory2,
            materialCategory3
        )
        val sortedList = arrayListOf(
            materialCategory3,
            materialCategory1,
            materialCategory2
        )

        // Act
        sortByMaterialName(actualList)

        // Assert
        assertEquals(sortedList, actualList)
    }

}