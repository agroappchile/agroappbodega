package cl.agroapp.agroappbodega.utils

import cl.agroapp.agroappbodega.utils.StringUtils.Companion.containsIgnoreCase
import org.junit.Assert.assertEquals
import org.junit.Test

class StringUtilsTest {

    @Test
    fun containsSubstring_returnTrue() {
        // Act
        val contains = containsIgnoreCase("LOREM IPSUM", "ips")

        // Assert
        assertEquals(true, contains)
    }

    @Test
    fun doesNotContainSubstring_returnFalse() {
        // Act
        val contains = containsIgnoreCase("LOREM IPSUM", "hello")

        // Assert
        assertEquals(false, contains)
    }

}