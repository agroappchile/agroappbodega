package cl.agroapp.agroappbodega.utils

import android.content.Context
import cl.agroapp.agroappbodega.R
import com.nhaarman.mockitokotlin2.any
import okhttp3.ResponseBody
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.HttpException
import retrofit2.Response
import java.net.SocketTimeoutException

@RunWith(MockitoJUnitRunner::class)
class ApiUtilsTest {

    @Mock
    private lateinit var context: Context
    @Mock
    private lateinit var jsonUtils: JsonUtils
    private lateinit var apiUtils: ApiUtils

    @Mock
    private lateinit var responseBodyMock: ResponseBody
    @Mock
    private lateinit var httpExceptionMock: HttpException

    @Before
    fun setUp() {
        apiUtils = ApiUtils(context, jsonUtils)
        val responseMock = mock(Response::class.java)
        `when`(httpExceptionMock.response()).thenReturn(responseMock)
        `when`(responseMock.errorBody()).thenReturn(responseBodyMock)
    }

    @Test
    fun httpExceptionWithBody_returnBodyMessage() {
        // Arrange
        val expectedMessage = "API error message"
        val errorBody = "{\"${apiUtils.apiErrorKey}\":\"$expectedMessage\"}"
        `when`(responseBodyMock.string()).thenReturn(errorBody)
        `when`(jsonUtils.isJsonValid(any())).thenReturn(true)

        // Act
        val actualMessage = apiUtils.getErrorMessage(httpExceptionMock)

        // Assert
        assertEquals(expectedMessage, actualMessage)
    }

    @Test
    fun httpExceptionWithMalformedBody_returnInvalidJsonMessage() {
        // Arrange
        val malformedJson = "{abc"
        `when`(responseBodyMock.string()).thenReturn(malformedJson)
        val expectedMessage = "invalid json"
        `when`(context.getString(R.string.invalid_json)).thenReturn(expectedMessage)
        `when`(jsonUtils.isJsonValid(any())).thenReturn(false)

        // Act
        val actualMessage = apiUtils.getErrorMessage(httpExceptionMock)

        // Assert
        assertEquals(expectedMessage, actualMessage)
    }

    @Test
    fun httpExceptionWithNoBody_returnNoContentMessage() {
        // Arrange
        val expectedMessage = "no error content"
        `when`(context.getString(R.string.no_error_content)).thenReturn(expectedMessage)

        // Act
        val actualMessage = apiUtils.getErrorMessage(httpExceptionMock)

        // Assert
        assertEquals(expectedMessage, actualMessage)
    }

    @Test
    fun timeoutException_returnTimeoutMessage() {
        // Arrange
        val timeoutException = mock(SocketTimeoutException::class.java)
        val expectedMessage = "timeout"
        `when`(context.getString(R.string.timeout)).thenReturn(expectedMessage)

        // Act
        val actualMessage = apiUtils.getErrorMessage(timeoutException)

        // Assert
        assertEquals(expectedMessage, actualMessage)
    }

    @Test
    fun genericException_returnGenericMessage() {
        // Arrange
        val genericException = mock(Exception::class.java)
        val expectedMessage = "generic exception"
        val message = "message"
        `when`(genericException.message).thenReturn(message)
        `when`(context.getString(R.string.unexpected_error, genericException.message))
            .thenReturn(expectedMessage)

        // Act
        val actualMessage = apiUtils.getErrorMessage(genericException)

        // Assert
        assertEquals(expectedMessage, actualMessage)
    }

}