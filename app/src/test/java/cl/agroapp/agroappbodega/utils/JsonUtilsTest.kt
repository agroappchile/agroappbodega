package cl.agroapp.agroappbodega.utils

import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class JsonUtilsTest {

    private lateinit var jsonUtils: JsonUtils

    @Before
    fun setUp() {
        jsonUtils = JsonUtils()
    }

    @Test
    fun validJson_returnTrue() {
        // Assert
        assertEquals(true, jsonUtils.isJsonValid("{\"key\":\"value\"}"))
    }

    @Test
    fun invalidJson_returnFalse() {
        // Assert
        assertEquals(false, jsonUtils.isJsonValid("{abc"))
    }

}