package cl.agroapp.agroappbodega.utils

import cl.agroapp.agroappbodega.data.model.User

class Constants {

    companion object {
        // Common
        const val SHARED_PREFS_NAME = "agroapp_bodega"
        const val SHARED_PREFS_USER = "user"
        const val SHARED_PREFS_PASSWORD = "password"
        const val SHARED_PREFS_REMEMBER_ME = "remember_me"
        const val EMPTY_STRING = ""
        // TODO enum for ACTIVE INACTIVE
        const val ACTIVE = "Y"
        const val INACTIVE = "N"
        const val INVALID_ID = -1

        // Category
        const val MAX_CATEGORY_LENGTH = 80
        const val CATEGORY_NAME = "category_name"

        // Material
        const val MAX_MATERIAL_LENGTH = 80

        // Warehouse
        const val MAX_WAREHOUSE_LENGTH = 50

        // Retrofit
        const val CATEGORY = "Categoria"
        const val MATERIAL = "Material"
        const val WAREHOUSE = "Almacen"
        const val MOVEMENT = "Movimiento"

        // Used in integration/ui tests
        val VALID_USER_TEST = User("dsantamaria", "123456")
        val INVALID_USER_TEST = User("invalid_user", "??????")
        const val EXISTING_MATERIAL_NAME = "LECHUGA"
        const val EXISTING_WAREHOUSE_NAME = "OSORNO"
        const val EXISTING_CATEGORY_NAME = "ABARROTE"
        const val EXISTING_CATEGORY_NAME2 = "ANALGÉSICO"
        const val EXISTING_CATEGORY_ID = 399
        const val EXISTING_MATERIAL_ID = 313
        const val EXISTING_WAREHOUSE_ID = 43
        const val EXISTING_ENTRY_ID = 42
    }

}