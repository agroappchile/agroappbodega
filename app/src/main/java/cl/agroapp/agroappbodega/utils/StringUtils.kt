package cl.agroapp.agroappbodega.utils

class StringUtils {

    companion object {
        fun containsIgnoreCase(string: String, substring: String): Boolean {
            if (string.contains(substring, ignoreCase = true)) {
                return true
            }
            return false
        }
    }

}