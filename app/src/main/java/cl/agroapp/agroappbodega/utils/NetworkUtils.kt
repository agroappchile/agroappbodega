package cl.agroapp.agroappbodega.utils

import cl.agroapp.agroappbodega.BaseSchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class NetworkUtils @Inject constructor(
    private val schedulerProvider: BaseSchedulerProvider,
    private val disposable: CompositeDisposable,
    private val apiUtils: ApiUtils
) {

    fun getIoScheduler() = schedulerProvider.io()

    fun getUiScheduler() = schedulerProvider.ui()

    fun addDisposable(disposable: Disposable) {
        this.disposable.add(disposable)
    }

    fun dispose() {
        disposable.dispose()
    }

    fun getErrorMessage(throwable: Throwable) = apiUtils.getErrorMessage(throwable)

}