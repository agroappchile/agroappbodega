package cl.agroapp.agroappbodega.utils

import android.content.Context
import cl.agroapp.agroappbodega.R
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import retrofit2.HttpException
import java.net.SocketTimeoutException
import javax.inject.Inject

class ApiUtils @Inject constructor(
    private val context: Context,
    private val jsonUtils: JsonUtils
) {

    val apiErrorKey = "message"

    fun getErrorMessage(throwable: Throwable): String {
        throwable.printStackTrace()
        return when (throwable) {
            is HttpException -> getHttpExceptionMessage(throwable)
            is SocketTimeoutException -> getTimeoutExceptionMessage()
            else -> getGenericExceptionMessage(throwable)
        }
    }

    private fun getHttpExceptionMessage(httpException: HttpException): String {
        return getErrorBody(httpException)?.let { errorBody ->
            if (jsonUtils.isJsonValid(errorBody)) {
                getErrorMessage(parseJson(errorBody))
            } else {
                context.getString(R.string.invalid_json)
            }
        } ?: context.getString(R.string.no_error_content)
    }

    private fun getErrorBody(httpException: HttpException) =
        httpException.response()?.errorBody()?.string()

    private fun getErrorMessage(json: JsonObject) = json[apiErrorKey]?.asString

    private fun parseJson(jsonString: String) = JsonParser().parse(jsonString).asJsonObject

    private fun getTimeoutExceptionMessage() = context.getString(R.string.timeout)

    private fun getGenericExceptionMessage(throwable: Throwable) = context.getString(
        R.string.unexpected_error,
        throwable.message?.let { it } ?: Constants.EMPTY_STRING
    )

}