package cl.agroapp.agroappbodega.utils

import java.text.SimpleDateFormat
import java.util.*

class DateUtils {

    companion object {
        fun getShortDate(date: Date): String = SimpleDateFormat("dd MMM", Locale.getDefault())
            .format(date)
    }

}