package cl.agroapp.agroappbodega.utils

import cl.agroapp.agroappbodega.data.model.Category
import cl.agroapp.agroappbodega.data.model.MaterialCategory
import java.util.*
import kotlin.collections.ArrayList

class MaterialCategoryUtils {

    companion object {
        fun transformToMaterialCategory(categoryList: ArrayList<Category>):
                ArrayList<MaterialCategory> {
            val materialCategoryList = arrayListOf<MaterialCategory>()
            for (category in categoryList) {
                for (material in category.materialList) {
                    materialCategoryList.add(MaterialCategory(category, material))
                }
            }
            return materialCategoryList
        }

        fun sortByMaterialName(materialCategoryList: ArrayList<MaterialCategory>) {
            materialCategoryList.sortBy { it.material.name.toUpperCase(Locale.getDefault()) }
        }
    }

}