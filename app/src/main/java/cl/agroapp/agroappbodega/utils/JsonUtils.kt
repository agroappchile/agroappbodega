package cl.agroapp.agroappbodega.utils

import com.google.gson.JsonParser
import com.google.gson.JsonSyntaxException
import javax.inject.Inject

class JsonUtils @Inject constructor() {

    fun isJsonValid(jsonString: String): Boolean {
        return try {
            JsonParser().parse(jsonString)
            true
        } catch (jsonException: JsonSyntaxException) {
            false
        }
    }

}