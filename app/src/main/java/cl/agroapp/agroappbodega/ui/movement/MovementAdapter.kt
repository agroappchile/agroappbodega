package cl.agroapp.agroappbodega.ui.movement

import android.view.*
import androidx.recyclerview.widget.RecyclerView
import cl.agroapp.agroappbodega.R
import cl.agroapp.agroappbodega.data.model.Movement
import cl.agroapp.agroappbodega.utils.DateUtils.Companion.getShortDate
import kotlinx.android.synthetic.main.adapter_movement.view.*
import javax.inject.Inject

class MovementAdapter @Inject constructor() :
    RecyclerView.Adapter<MovementAdapter.MovementHolder>() {

    var onDeleteItemClick: ((Movement) -> (Unit))? = null
    var movementList = ArrayList<Movement>()
        set (value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovementHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.adapter_movement, parent, false)
        return MovementHolder(view)
    }

    override fun onBindViewHolder(holder: MovementHolder, position: Int) {
        val movement = getMovement(position)
        holder.itemView.movement_id_tv.text = movement.movementId.toString()
        holder.itemView.movement_type_tv.text = movement.getMovementType(holder.itemView.context)
        holder.itemView.movement_date_tv.text = getShortDate(movement.created)
        holder.itemView.movement_detail_tv.text = movement.getSummary(holder.itemView.context)
    }

    private fun getMovement(position: Int) = movementList[position]

    override fun getItemCount() = movementList.size

    inner class MovementHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            setContextMenu()
        }

        private fun setContextMenu() {
            itemView.setOnCreateContextMenuListener { contextMenu, _, _ ->
                addDeleteMenu(contextMenu)
            }
        }

        private fun addDeleteMenu(contextMenu: ContextMenu) {
            val deleteMenu = contextMenu.add(Menu.NONE, Menu.NONE, 0,
                itemView.context.getString(R.string.delete))
            setDeleteMenuClickListener(deleteMenu)
        }

        private fun setDeleteMenuClickListener(deleteMenu: MenuItem) {
            deleteMenu.setOnMenuItemClickListener {
                onDeleteItemClick?.invoke(getMovement())
                true
            }
        }

        private fun getMovement() = movementList[adapterPosition]
    }

}