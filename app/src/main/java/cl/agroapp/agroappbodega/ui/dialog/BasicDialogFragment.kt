package cl.agroapp.agroappbodega.ui.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import cl.agroapp.agroappbodega.R
import cl.agroapp.agroappbodega.data.model.BasicDialog
import kotlinx.android.synthetic.main.dialog_basic.view.*
import javax.inject.Inject

class BasicDialogFragment @Inject constructor() : DialogFragment() {

    companion object {
        val TAG = BasicDialogFragment::class.java.simpleName
    }

    private val basicDialogKey = "basic_dialog_key"
    private var basicDialog = BasicDialog()

    fun setTitle(title: String) {
        basicDialog.title = title
    }

    fun setMessage(message: String) {
        basicDialog.message = message
    }

    override fun show(manager: FragmentManager, tag: String?) {
        setArguments()
        super.show(manager, tag)
    }

    private fun setArguments() {
        trimBasicDialogValues()
        validateBasicDialog()
        val bundle = Bundle()
        bundle.putParcelable(basicDialogKey, basicDialog)
        arguments = bundle
    }

    private fun trimBasicDialogValues() {
        basicDialog.title = basicDialog.title.trim()
        basicDialog.message = basicDialog.message.trim()
    }

    private fun validateBasicDialog() {
        checkIfTitleIsSet()
        checkIfMessageIsSet()
    }

    private fun checkIfTitleIsSet() {
        check(basicDialog.title.isNotEmpty()) {
            "Must set title"
        }
    }

    private fun checkIfMessageIsSet() {
        check(basicDialog.message.isNotEmpty()) {
            "Must set message"
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        restoreBasicDialog()
    }

    private fun restoreBasicDialog() {
        basicDialog = arguments?.getParcelable<BasicDialog>(basicDialogKey)?.let { it }
            ?: throw NullPointerException("Data in arguments is null")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.dialog_basic, container, false)
        setTitle(view)
        setMessage(view)
        setOkBtnClickListener(view)
        return view
    }

    private fun setTitle(view: View) {
        view.title_tv.text = basicDialog.title
    }

    private fun setMessage(view: View) {
        view.message_tv.text = basicDialog.message
    }

    private fun setOkBtnClickListener(view: View) {
        view.ok_btn.setOnClickListener {
            dismiss()
        }
    }

}