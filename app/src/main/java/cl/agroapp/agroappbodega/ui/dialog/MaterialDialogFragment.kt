package cl.agroapp.agroappbodega.ui.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import cl.agroapp.agroappbodega.R
import cl.agroapp.agroappbodega.data.model.Category
import cl.agroapp.agroappbodega.utils.Constants
import kotlinx.android.synthetic.main.dialog_material.*
import kotlinx.android.synthetic.main.dialog_material.view.*
import javax.inject.Inject

class MaterialDialogFragment @Inject constructor() : DialogFragment() {

    companion object {
        val TAG = MaterialDialogFragment::class.java.simpleName
    }

    interface Listener {
        fun onConfirmClick(categoryId: Int, materialName: String)
    }

    lateinit var categoryList: ArrayList<Category>

    private val categoryListKey = "category_list_key"
    private lateinit var listener: Listener

    override fun show(manager: FragmentManager, tag: String?) {
        setArguments()
        super.show(manager, tag)
    }

    private fun setArguments() {
        checkIfCategoryListIsInitialized()
        val bundle = Bundle()
        bundle.putParcelableArrayList(categoryListKey, categoryList)
        arguments = bundle
    }

    private fun checkIfCategoryListIsInitialized() {
        check(::categoryList.isInitialized) {
            "Must set categoryList"
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        restoreCategoryList()
        setListener()
    }

    private fun restoreCategoryList() {
        categoryList = arguments?.getParcelableArrayList<Category>(categoryListKey)?.let { it }
            ?: throw NullPointerException("Data in arguments is null")
    }

    private fun setListener() {
        try {
            listener = activity as Listener
        } catch (e: ClassCastException) {
            throw ClassCastException("Activity must implement listener")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.dialog_material, container, false)
        setCategorySpAdapter(view)
        setOkBtnClickListener(view)
        setCancelBtnClickListener(view)
        return view
    }

    private fun setCategorySpAdapter(view: View) {
        view.category_sp.adapter = ArrayAdapter(requireContext(), R.layout.list_dropdown_item,
            categoryList)
    }

    private fun setOkBtnClickListener(view: View) {
        view.ok_btn.setOnClickListener {
            listener.onConfirmClick(getCategoryId(), getMaterialName())
            clearValueEt()
            dismiss()
        }
    }

    private fun getCategoryId() =
        category_sp.selectedItem?.let { (it as Category).id } ?: Constants.INVALID_ID

    private fun getMaterialName() = value_et.text.toString().trim()

    private fun clearValueEt() {
        value_et.setText(Constants.EMPTY_STRING)
    }

    private fun setCancelBtnClickListener(view: View) {
        view.cancel_btn.setOnClickListener {
            clearValueEt()
            dismiss()
        }
    }

}