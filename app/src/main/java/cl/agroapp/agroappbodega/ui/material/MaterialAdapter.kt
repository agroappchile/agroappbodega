package cl.agroapp.agroappbodega.ui.material

import android.view.*
import androidx.recyclerview.widget.RecyclerView
import cl.agroapp.agroappbodega.R
import cl.agroapp.agroappbodega.data.model.Material
import cl.agroapp.agroappbodega.data.model.MaterialCategory
import kotlinx.android.synthetic.main.adapter_material.view.*
import javax.inject.Inject

class MaterialAdapter @Inject constructor() :
    RecyclerView.Adapter<MaterialAdapter.MaterialHolder>() {

    var onEditItemClick: ((Material) -> (Unit))? = null
    var onDeleteItemClick: ((Material) -> (Unit))? = null
    var adapterList = arrayListOf<MaterialCategory>()
        set (value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MaterialHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.adapter_material, parent, false)
        return MaterialHolder(view)
    }

    override fun onBindViewHolder(holder: MaterialHolder, position: Int) {
        val materialCategory = getItem(position)
        holder.itemView.material_tv.text = materialCategory.material.name
        holder.itemView.category_tv.text = materialCategory.category.name
    }

    private fun getItem(position: Int) = adapterList[position]

    override fun getItemCount() = adapterList.size

    inner class MaterialHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            setContextMenu()
        }

        private fun setContextMenu() {
            itemView.setOnCreateContextMenuListener { contextMenu, _, _ ->
                addEditMenu(contextMenu)
                addDeleteMenu(contextMenu)
            }
        }

        private fun addEditMenu(contextMenu: ContextMenu) {
            val editMenu = contextMenu.add(Menu.NONE, Menu.NONE, 0,
                itemView.context.getString(R.string.edit))
            setEditMenuClickListener(editMenu)
        }

        private fun setEditMenuClickListener(editMenu: MenuItem) {
            editMenu.setOnMenuItemClickListener {
                onEditItemClick?.invoke(getMaterial())
                true
            }
        }

        private fun getMaterial() = adapterList[adapterPosition].material

        private fun addDeleteMenu(contextMenu: ContextMenu) {
            val deleteMenu = contextMenu.add(Menu.NONE, Menu.NONE, 1,
                itemView.context.getString(R.string.delete))
            setDeleteMenuClickListener(deleteMenu)
        }

        private fun setDeleteMenuClickListener(deleteMenu: MenuItem) {
            deleteMenu.setOnMenuItemClickListener {
                onDeleteItemClick?.invoke(getMaterial())
                true
            }
        }
    }

}