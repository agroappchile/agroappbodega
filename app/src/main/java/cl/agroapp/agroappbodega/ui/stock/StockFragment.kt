package cl.agroapp.agroappbodega.ui.stock

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cl.agroapp.agroappbodega.R
import dagger.android.support.DaggerFragment

class StockFragment : DaggerFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_stock, container, false)

        return root
    }

}