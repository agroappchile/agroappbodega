package cl.agroapp.agroappbodega.ui.material

import androidx.annotation.StringRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cl.agroapp.agroappbodega.R
import cl.agroapp.agroappbodega.SingleLiveEvent
import cl.agroapp.agroappbodega.data.model.Category
import cl.agroapp.agroappbodega.data.model.Material
import cl.agroapp.agroappbodega.data.model.MaterialCategory
import cl.agroapp.agroappbodega.data.network.model.CategoryResponse
import cl.agroapp.agroappbodega.data.repository.CategoryRepository
import cl.agroapp.agroappbodega.data.repository.MaterialRepository
import cl.agroapp.agroappbodega.utils.MaterialCategoryUtils.Companion.transformToMaterialCategory
import cl.agroapp.agroappbodega.utils.NetworkUtils
import cl.agroapp.agroappbodega.utils.StringUtils.Companion.containsIgnoreCase
import javax.inject.Inject

class MaterialViewModel @Inject constructor(
    private val materialRepository: MaterialRepository,
    private val categoryRepository: CategoryRepository,
    private val networkUtils: NetworkUtils
) : ViewModel() {

    val materialCategoryListObservable: LiveData<ArrayList<MaterialCategory>>
        get() = _materialCategoryListObservable
    val loadingObservable: LiveData<Boolean>
        get() = _loadingObservable
    val repositoryErrorObservable: LiveData<String>
        get() = _repositoryErrorObservable
    val notificationObservable: LiveData<Int>
        get() = _notificationObservable
    val repositorySuccessObservable: LiveData<Boolean>
        get() = _repositorySuccessObservable
    var categoryList = arrayListOf<Category>()

    private val _materialCategoryListObservable = MutableLiveData<ArrayList<MaterialCategory>>()
    private val _loadingObservable = MutableLiveData<Boolean>()
    private val _repositoryErrorObservable = SingleLiveEvent<String>()
    private val _notificationObservable = SingleLiveEvent<Int>()
    private val _repositorySuccessObservable = SingleLiveEvent<Boolean>()
    private var materialCategoryList = arrayListOf<MaterialCategory>()

    override fun onCleared() {
        super.onCleared()
        networkUtils.dispose()
    }

    init {
        loadCategories()
    }

    private fun loadCategories() {
        networkUtils.addDisposable(categoryRepository.getCategories()
            .subscribeOn(networkUtils.getIoScheduler())
            .observeOn(networkUtils.getUiScheduler())
            .doOnSubscribe { setLoadingObservable(true) }
            .doFinally { setLoadingObservable(false) }
            .subscribe(
                { setListsAndObservables(it.categoryList) },
                { setRepositoryErrorObservable(it) }
            ))
    }

    private fun setLoadingObservable(loading: Boolean) {
        _loadingObservable.value = loading
    }

    private fun setListsAndObservables(categoryList: ArrayList<Category>) {
        this.categoryList = categoryList
        this.materialCategoryList = transformToMaterialCategory(categoryList)
        setRepositorySuccessObservableToTrue()
    }

    private fun setRepositorySuccessObservableToTrue() {
        _repositorySuccessObservable.value = true
    }

    private fun setRepositoryErrorObservable(throwable: Throwable) {
        _repositoryErrorObservable.value = networkUtils.getErrorMessage(throwable)
    }

    fun newMaterial(categoryId: Int, materialName: String) {
        networkUtils.addDisposable(materialRepository.postMaterial(
            createCategoryWithMaterial(categoryId, materialName))
            .subscribeOn(networkUtils.getIoScheduler())
            .observeOn(networkUtils.getUiScheduler())
            .doOnSubscribe { setLoadingObservable(true) }
            .doFinally { setLoadingObservable(false) }
            .subscribe(
                { onRepositorySuccess(it, R.string.material_created) },
                { setRepositoryErrorObservable(it) }
            ))
    }

    private fun createCategoryWithMaterial(categoryId: Int, materialName: String) =
        Category(id = categoryId, materialList = arrayListOf(Material(name = materialName.trim())))

    private fun onRepositorySuccess(categoryResponse: CategoryResponse, @StringRes resId: Int) {
        setNotificationObservable(resId)
        setListsAndObservables(categoryResponse.categoryList)
    }

    private fun setNotificationObservable(@StringRes resId: Int) {
        _notificationObservable.value = resId
    }

    fun updateMaterial(materialId: Int, materialName: String) {
        networkUtils.addDisposable(materialRepository.putMaterial(
            createMaterial(materialId, materialName))
            .subscribeOn(networkUtils.getIoScheduler())
            .observeOn(networkUtils.getUiScheduler())
            .doOnSubscribe { setLoadingObservable(true) }
            .doFinally { setLoadingObservable(false) }
            .subscribe(
                { onRepositorySuccess(it, R.string.material_updated) },
                { setRepositoryErrorObservable(it) }
            ))
    }

    private fun createMaterial(materialId: Int, materialName: String) =
        Material(id = materialId, name = materialName.trim())

    fun deleteMaterial(material: Material) {
        material.setInactive()
        networkUtils.addDisposable(materialRepository.putMaterial(material)
            .subscribeOn(networkUtils.getIoScheduler())
            .observeOn(networkUtils.getUiScheduler())
            .doOnSubscribe { setLoadingObservable(true) }
            .doFinally { setLoadingObservable(false) }
            .subscribe(
                { onRepositorySuccess(it, R.string.material_deleted) },
                { setRepositoryErrorObservable(it) }
            ))
    }

    fun filterMaterialList(query: String) {
        setMaterialCategoryListObservable(ArrayList(filterByCategoryOrMaterialName(query.trim())))
    }

    private fun setMaterialCategoryListObservable(list: ArrayList<MaterialCategory>) {
        _materialCategoryListObservable.value = list
    }

    private fun filterByCategoryOrMaterialName(query: String) =
        materialCategoryList.filter {
            containsIgnoreCase(it.category.name, query) || containsIgnoreCase(it.material.name,
                query)
        }

}