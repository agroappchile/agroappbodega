package cl.agroapp.agroappbodega.ui.category

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import cl.agroapp.agroappbodega.R
import cl.agroapp.agroappbodega.data.model.Category
import cl.agroapp.agroappbodega.ui.dialog.AddDialogFragment
import cl.agroapp.agroappbodega.ui.dialog.BasicDialogFragment
import cl.agroapp.agroappbodega.ui.dialog.EditDialogFragment
import cl.agroapp.agroappbodega.ui.material.MaterialActivity
import cl.agroapp.agroappbodega.utils.Constants
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_category.*
import kotlinx.android.synthetic.main.fragment_category.view.*
import javax.inject.Inject

class CategoryFragment : DaggerFragment(), AddDialogFragment.Listener, EditDialogFragment.Listener {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var basicDialog: BasicDialogFragment
    @Inject
    lateinit var addDialog: AddDialogFragment
    @Inject
    lateinit var editDialog: EditDialogFragment
    @Inject
    lateinit var categoryAdapter: CategoryAdapter

    private val categoryViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(CategoryViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_category, container, false)
        setCategoryRv(root)
        setListeners(root)
        setObservers()
        return root
    }

    private fun setCategoryRv(root: View) {
        root.category_rv.layoutManager = LinearLayoutManager(requireContext())
        root.category_rv.setHasFixedSize(true)
        root.category_rv.addItemDecoration(DividerItemDecoration(requireContext(),
            DividerItemDecoration.VERTICAL))
        root.category_rv.adapter = categoryAdapter
    }

    private fun setListeners(root: View) {
        setCategoryAdapterItemClick()
        setCategoryAdapterEditClick()
        setCategoryAdapterDeleteClick()
        setAddCategoryFabClickListener(root)
    }

    private fun setCategoryAdapterItemClick() {
        categoryAdapter.onItemClick = {
            MaterialActivity.startActivity(requireContext(), it)
        }
    }

    private fun setCategoryAdapterEditClick() {
        categoryAdapter.onEditItemClick = {
            showEditDialog(it)
        }
    }

    private fun showEditDialog(category: Category) {
        editDialog.setTitle(getString(R.string.edit))
        editDialog.setMessage(getString(R.string.enter_category_name))
        editDialog.setInputMaxLength(Constants.MAX_CATEGORY_LENGTH)
        editDialog.setParentViewType(this)
        editDialog.setItemId(category.id)
        editDialog.setValue(category.name)
        editDialog.show(parentFragmentManager, EditDialogFragment.TAG)
    }

    private fun setCategoryAdapterDeleteClick() {
        categoryAdapter.onDeleteItemClick = {
            categoryViewModel.deleteCategory(it)
        }
    }

    private fun setAddCategoryFabClickListener(root: View) {
        root.add_category_fab.setOnClickListener {
            showAddDialog()
        }
    }

    private fun showAddDialog() {
        addDialog.setTitle(getString(R.string.add))
        addDialog.setMessage(getString(R.string.enter_category_name))
        addDialog.setInputMaxLength(Constants.MAX_CATEGORY_LENGTH)
        addDialog.setParentViewType(this)
        addDialog.show(parentFragmentManager, AddDialogFragment.TAG)
    }

    private fun setObservers() {
        setLoadingObserver()
        setRepositoryErrorObserver()
        setCategoryListObserver()
        setNotificationObserver()
    }

    private fun setLoadingObserver() {
        categoryViewModel.loadingObservable.observe(viewLifecycleOwner, Observer {
            if (it) {
                loading_pb.visibility = View.VISIBLE
            } else {
                loading_pb.visibility = View.GONE
            }
        })
    }

    private fun setRepositoryErrorObserver() {
        categoryViewModel.repositoryErrorObservable.observe(viewLifecycleOwner, Observer {
            showBasicDialog(getString(R.string.error), it)
        })
    }

    private fun showBasicDialog(title: String, message: String) {
        basicDialog.setTitle(title)
        basicDialog.setMessage(message)
        basicDialog.show(parentFragmentManager, BasicDialogFragment.TAG)
    }

    private fun setCategoryListObserver() {
        categoryViewModel.categoryListObservable.observe(viewLifecycleOwner, Observer {
            categoryAdapter.categoryList = it
        })
    }

    private fun setNotificationObserver() {
        categoryViewModel.notificationObservable.observe(viewLifecycleOwner, Observer {
            showBasicDialog(getString(R.string.information), getString(it))
        })
    }

    override fun onAddConfirmClick(value: String) {
        categoryViewModel.newCategory(categoryName = value)
    }

    override fun onEditConfirmClick(value: String, itemId: Int) {
        categoryViewModel.updateCategory(categoryId = itemId, categoryName = value)
    }

}