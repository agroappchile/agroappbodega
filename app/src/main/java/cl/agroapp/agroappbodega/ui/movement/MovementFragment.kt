package cl.agroapp.agroappbodega.ui.movement

import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import cl.agroapp.agroappbodega.R
import cl.agroapp.agroappbodega.ui.dialog.BasicDialogFragment
import cl.agroapp.agroappbodega.ui.entry.EntryActivity
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_movement.*
import kotlinx.android.synthetic.main.fragment_movement.view.*
import javax.inject.Inject

class MovementFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var movementAdapter: MovementAdapter
    @Inject
    lateinit var basicDialog: BasicDialogFragment

    private val movementViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(MovementViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_movement, container, false)

        setMovementRv(root)
        setListeners(root)
        setObservers()

        return root
    }

    private fun setMovementRv(root: View) {
        root.movement_rv.layoutManager = LinearLayoutManager(requireContext())
        root.movement_rv.setHasFixedSize(true)
        root.movement_rv.addItemDecoration(DividerItemDecoration(requireContext(),
            DividerItemDecoration.VERTICAL))
        root.movement_rv.adapter = movementAdapter
    }

    private fun setListeners(root: View) {
        setAddMovementFabContextMenu(root)
        setAddMovementFabClickListener(root)
        setMovementAdapterDeleteClick()
    }

    private fun setAddMovementFabContextMenu(root: View) {
        root.add_movement_fab.setOnCreateContextMenuListener { contextMenu, _, _ ->
            setContextMenuTitle(contextMenu)
            addEntryMenu(contextMenu)
            //addWithdrawMenu(contextMenu)
        }
    }

    private fun setContextMenuTitle(contextMenu: ContextMenu) {
        contextMenu.setHeaderTitle(getString(R.string.choose_movement_type))
    }

    private fun addEntryMenu(contextMenu: ContextMenu) {
        val entryMenu = contextMenu.add(Menu.NONE, Menu.NONE, 0,
            requireContext().getString(R.string.title_entry))
        setEntryMenuClickListener(entryMenu)
    }

    private fun setEntryMenuClickListener(entryMenu: MenuItem) {
        entryMenu.setOnMenuItemClickListener {
            EntryActivity.startActivityForResult(this, EntryActivity.REQUEST_CODE)
            true
        }
    }

    private fun setAddMovementFabClickListener(root: View) {
        root.add_movement_fab.setOnClickListener {
            add_movement_fab.showContextMenu()
        }
    }

    private fun setMovementAdapterDeleteClick() {
        movementAdapter.onDeleteItemClick = {
            movementViewModel.deleteMovement(it)
        }
    }

    private fun setObservers() {
        setLoadingObserver()
        setMovementListObserver()
        setRepositoryErrorObserver()
        setNotificationObserver()
    }

    private fun setLoadingObserver() {
        movementViewModel.loadingObservable.observe(viewLifecycleOwner, Observer {
            if (it) {
                loading_pb.visibility = View.VISIBLE
            } else {
                loading_pb.visibility = View.GONE
            }
        })
    }

    private fun setMovementListObserver() {
        movementViewModel.movementListObservable.observe(viewLifecycleOwner, Observer {
            movementAdapter.movementList = it
        })
    }

    private fun setRepositoryErrorObserver() {
        movementViewModel.repositoryErrorObservable.observe(viewLifecycleOwner, Observer {
            showBasicDialog(getString(R.string.error), it)
        })
    }

    private fun showBasicDialog(title: String, message: String) {
        basicDialog.setTitle(title)
        basicDialog.setMessage(message)
        basicDialog.show(parentFragmentManager, BasicDialogFragment.TAG)
    }

    private fun setNotificationObserver() {
        movementViewModel.notificationObservable.observe(viewLifecycleOwner, Observer {
            showBasicDialog(getString(R.string.information), getString(it))
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        movementViewModel.checkIfMovementWasCreated(resultCode)
    }

}
