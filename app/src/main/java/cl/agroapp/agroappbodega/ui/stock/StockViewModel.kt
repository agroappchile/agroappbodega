package cl.agroapp.agroappbodega.ui.stock

import androidx.lifecycle.ViewModel
import cl.agroapp.agroappbodega.data.repository.StockRepository
import cl.agroapp.agroappbodega.utils.NetworkUtils
import javax.inject.Inject

class StockViewModel @Inject constructor(
    private val stockRepository: StockRepository,
    private val networkUtils: NetworkUtils
) : ViewModel() {

    override fun onCleared() {
        super.onCleared()
        networkUtils.dispose()
    }

}