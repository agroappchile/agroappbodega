package cl.agroapp.agroappbodega.ui.login

import android.content.SharedPreferences
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cl.agroapp.agroappbodega.R
import cl.agroapp.agroappbodega.SingleLiveEvent
import cl.agroapp.agroappbodega.data.model.LoginState
import cl.agroapp.agroappbodega.data.model.User
import cl.agroapp.agroappbodega.data.network.interceptor.AuthInterceptor
import cl.agroapp.agroappbodega.data.repository.LoginRepository
import cl.agroapp.agroappbodega.utils.Constants
import cl.agroapp.agroappbodega.utils.NetworkUtils
import javax.inject.Inject

// TODO wrap SharedPreferences & AuthInterceptor into another class
class LoginViewModel @Inject constructor(
    private val loginRepository: LoginRepository,
    private val networkUtils: NetworkUtils,
    private val sharedPreferences: SharedPreferences,
    private val authInterceptor: AuthInterceptor
) : ViewModel() {

    val notificationObservable: LiveData<Int>
        get() = _notificationObservable
    val loadingObservable: LiveData<LoginState>
        get() = _loadingObservable
    val rememberMeObservable: LiveData<User>
        get() = _rememberMeObservable
    val repositoryErrorObservable: LiveData<String>
        get() = _repositoryErrorObservable
    val repositorySuccessObservable: LiveData<Boolean>
        get() = _repositorySuccessObservable

    private val _notificationObservable = SingleLiveEvent<Int>()
    private val _loadingObservable = MutableLiveData<LoginState>()
    private val _rememberMeObservable = SingleLiveEvent<User>()
    private val _repositoryErrorObservable = SingleLiveEvent<String>()
    private val _repositorySuccessObservable = SingleLiveEvent<Boolean>()

    private val sharedPreferencesEditor = sharedPreferences.edit()
    private var rememberMe = false

    override fun onCleared() {
        super.onCleared()
        networkUtils.dispose()
    }

    init {
        setRememberMeObservableIfSaved()
    }

    private fun setRememberMeObservableIfSaved() {
        if (isRememberMeSaved()) {
            setRememberMeObservable(getUserFromSharedPreferences())
        }
    }

    private fun isRememberMeSaved() =
        sharedPreferences.getBoolean(Constants.SHARED_PREFS_REMEMBER_ME, false)

    private fun getUserFromSharedPreferences() = User(
        sharedPreferences.getString(Constants.SHARED_PREFS_USER, Constants.EMPTY_STRING)
            ?.let { it } ?: Constants.EMPTY_STRING,
        sharedPreferences.getString(Constants.SHARED_PREFS_PASSWORD, Constants.EMPTY_STRING)
            ?.let { it } ?: Constants.EMPTY_STRING
    )

    private fun setRememberMeObservable(user: User) {
        _rememberMeObservable.value = user
    }

    fun login(username: String, password: String, rememberMe: Boolean) {
        setRememberMe(rememberMe)
        validateAndLogin(createUser(username, password))
    }

    private fun setRememberMe(rememberMe: Boolean) {
        this.rememberMe = rememberMe
    }

    private fun createUser(username: String, password: String) =
        User(username.trim(), password.trim())

    private fun validateAndLogin(user: User) {
        if (isLoginValid(user)) {
            login(user)
        } else {
            setNotificationObservable()
        }
    }

    private fun isLoginValid(user: User) = user.user.isNotEmpty() && user.password.isNotEmpty()

    private fun login(user: User) {
        networkUtils.addDisposable(loginRepository.login(user)
            .subscribeOn(networkUtils.getIoScheduler())
            .observeOn(networkUtils.getUiScheduler())
            .doOnSubscribe { setLoadingObservable(true) }
            .doFinally { setLoadingObservable(false) }
            .subscribe(
                { onLoginRepositorySuccess(it) },
                { setRepositoryErrorObservable(it) }
            ))
    }

    private fun setLoadingObservable(loading: Boolean) {
        _loadingObservable.value = LoginState(loading, getLoadingMessage(loading))
    }

    private fun getLoadingMessage(loading: Boolean): Int {
        return if (loading) {
            R.string.login_in_progress
        } else {
            R.string.login
        }
    }

    private fun onLoginRepositorySuccess(user: User) {
        saveToSharedPreferences(user)
        authInterceptor.setCredentials(user)
        setRepositorySuccessObservable()
    }

    private fun saveToSharedPreferences(user: User) {
        saveUserToSharedPreferences(user)
        saveRememberMeToSharedPreferences()
        applyChangesToSharedPreferences()
    }

    private fun saveUserToSharedPreferences(user: User) {
        sharedPreferencesEditor.putString(Constants.SHARED_PREFS_USER, user.user)
        sharedPreferencesEditor.putString(Constants.SHARED_PREFS_PASSWORD, user.password)
    }

    private fun saveRememberMeToSharedPreferences() {
        sharedPreferencesEditor.putBoolean(Constants.SHARED_PREFS_REMEMBER_ME, rememberMe)
    }

    private fun applyChangesToSharedPreferences() {
        sharedPreferencesEditor.apply()
    }

    private fun setRepositorySuccessObservable() {
        _repositorySuccessObservable.value = true
    }

    private fun setRepositoryErrorObservable(throwable: Throwable) {
        _repositoryErrorObservable.value = networkUtils.getErrorMessage(throwable)
    }

    private fun setNotificationObservable() {
        _notificationObservable.value = R.string.bad_login_credentials
    }

}