package cl.agroapp.agroappbodega.ui.login

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import cl.agroapp.agroappbodega.R
import cl.agroapp.agroappbodega.ui.dialog.BasicDialogFragment
import cl.agroapp.agroappbodega.ui.menu.MenuActivity
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject

class LoginActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var dialog: BasicDialogFragment

    private val loginViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(LoginViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        setObservers()
        setListeners()
    }

    private fun setObservers() {
        setRepositoryErrorObserver()
        setNotificationObserver()
        setLoadingObserver()
        setRepositorySuccessObserver()
        setRememberMeObserver()
    }

    private fun setRepositoryErrorObserver() {
        loginViewModel.repositoryErrorObservable.observe(this, Observer {
            showDialog(it)
        })
    }

    private fun showDialog(message: String) {
        dialog.setTitle(getString(R.string.error))
        dialog.setMessage(message)
        dialog.show(supportFragmentManager, BasicDialogFragment.TAG)
    }

    private fun setNotificationObserver() {
        loginViewModel.notificationObservable.observe(this, Observer {
            showDialog(getString(it))
        })
    }

    private fun setLoadingObserver() {
        loginViewModel.loadingObservable.observe(this, Observer { loginState ->
            login_btn.isEnabled = !loginState.loading
            login_btn.text = getString(loginState.message)
        })
    }

    private fun setRepositorySuccessObserver() {
        loginViewModel.repositorySuccessObservable.observe(this, Observer {
            MenuActivity.startActivity(this)
        })
    }

    private fun setRememberMeObserver() {
        loginViewModel.rememberMeObservable.observe(this, Observer { user ->
            remember_me_cb.isChecked = true
            user_et.setText(user.user)
            password_et.setText(user.password)
        })
    }

    private fun setListeners() {
        setRememberMeTvClickListener()
        setLoginBtnClickListener()
    }

    private fun setRememberMeTvClickListener() {
        remember_me_tv.setOnClickListener {
            remember_me_cb.performClick()
        }
    }

    private fun setLoginBtnClickListener() {
        login_btn.setOnClickListener {
            login()
        }
    }

    private fun login() {
        loginViewModel.login(
            username = user_et.text.toString(),
            password = password_et.text.toString(),
            rememberMe = remember_me_cb.isChecked
        )
    }

}
