package cl.agroapp.agroappbodega.ui.warehouse

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import cl.agroapp.agroappbodega.R
import cl.agroapp.agroappbodega.data.model.Warehouse
import cl.agroapp.agroappbodega.ui.dialog.AddDialogFragment
import cl.agroapp.agroappbodega.ui.dialog.BasicDialogFragment
import cl.agroapp.agroappbodega.ui.dialog.EditDialogFragment
import cl.agroapp.agroappbodega.utils.Constants
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_warehouse.*
import kotlinx.android.synthetic.main.fragment_warehouse.view.*
import javax.inject.Inject

class WarehouseFragment : DaggerFragment(), AddDialogFragment.Listener, EditDialogFragment.Listener {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var basicDialog: BasicDialogFragment
    @Inject
    lateinit var addDialog: AddDialogFragment
    @Inject
    lateinit var editDialog: EditDialogFragment
    @Inject
    lateinit var warehouseAdapter: WarehouseAdapter

    private val warehouseViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(WarehouseViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_warehouse, container, false)

        setWarehouseRv(root)
        setListeners(root)
        setObservers()

        return root
    }

    private fun setWarehouseRv(root: View) {
        root.warehouse_rv.layoutManager = LinearLayoutManager(requireContext())
        root.warehouse_rv.setHasFixedSize(true)
        root.warehouse_rv.addItemDecoration(DividerItemDecoration(requireContext(),
            DividerItemDecoration.VERTICAL))
        root.warehouse_rv.adapter = warehouseAdapter
    }

    private fun setListeners(root: View) {
        setAddWarehouseFabClickListener(root)
        setWarehouseAdapterEditClick()
        setWarehouseAdapterDeleteClick()
    }

    private fun setAddWarehouseFabClickListener(root: View) {
        root.add_warehouse_fab.setOnClickListener {
            showAddDialog()
        }
    }

    private fun showAddDialog() {
        addDialog.setTitle(getString(R.string.add))
        addDialog.setMessage(getString(R.string.enter_warehouse_name))
        addDialog.setInputMaxLength(Constants.MAX_WAREHOUSE_LENGTH)
        addDialog.setParentViewType(this)
        addDialog.show(parentFragmentManager, AddDialogFragment.TAG)
    }

    private fun setWarehouseAdapterEditClick() {
        warehouseAdapter.onEditItemClick = {
            showEditDialog(it)
        }
    }

    private fun showEditDialog(warehouse: Warehouse) {
        editDialog.setTitle(getString(R.string.edit))
        editDialog.setMessage(getString(R.string.enter_warehouse_name))
        editDialog.setInputMaxLength(Constants.MAX_WAREHOUSE_LENGTH)
        editDialog.setParentViewType(this)
        editDialog.setItemId(warehouse.id)
        editDialog.setValue(warehouse.name)
        editDialog.show(parentFragmentManager, EditDialogFragment.TAG)
    }

    private fun setWarehouseAdapterDeleteClick() {
        warehouseAdapter.onDeleteItemClick = {
            warehouseViewModel.deleteWarehouse(it)
        }
    }

    private fun setObservers() {
        setLoadingObserver()
        setWarehouseListObserver()
        setNotificationObserver()
        setRepositoryErrorObserver()
    }

    private fun setLoadingObserver() {
        warehouseViewModel.loadingObservable.observe(viewLifecycleOwner, Observer {
            if (it) {
                loading_pb.visibility = View.VISIBLE
            } else {
                loading_pb.visibility = View.GONE
            }
        })
    }

    private fun setWarehouseListObserver() {
        warehouseViewModel.warehouseListObservable.observe(viewLifecycleOwner, Observer {
            warehouseAdapter.warehouseList = it
        })
    }

    private fun setNotificationObserver() {
        warehouseViewModel.notificationObservable.observe(viewLifecycleOwner, Observer {
            showBasicDialog(getString(R.string.information), getString(it))
        })
    }

    private fun showBasicDialog(title: String, message: String) {
        basicDialog.setTitle(title)
        basicDialog.setMessage(message)
        basicDialog.show(parentFragmentManager, BasicDialogFragment.TAG)
    }

    private fun setRepositoryErrorObserver() {
        warehouseViewModel.repositoryErrorObservable.observe(viewLifecycleOwner, Observer {
            showBasicDialog(getString(R.string.error), it)
        })
    }

    override fun onAddConfirmClick(value: String) {
        warehouseViewModel.newWarehouse(warehouseName = value)
    }

    override fun onEditConfirmClick(value: String, itemId: Int) {
        warehouseViewModel.updateWarehouse(warehouseId = itemId, warehouseName = value)
    }

}
