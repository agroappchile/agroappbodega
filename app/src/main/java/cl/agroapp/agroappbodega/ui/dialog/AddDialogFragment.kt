package cl.agroapp.agroappbodega.ui.dialog

import android.os.Bundle
import cl.agroapp.agroappbodega.data.model.ParentViewType
import javax.inject.Inject

class AddDialogFragment @Inject constructor() : InputDialogFragment() {

    companion object {
        val TAG = AddDialogFragment::class.java.simpleName
    }

    interface Listener {
        fun onAddConfirmClick(value: String)
    }

    private lateinit var listener: Listener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        tryToSetListener()
    }

    private fun tryToSetListener() {
        try {
            setListener()
        } catch (e: ClassCastException) {
            throw ClassCastException("Activity/Fragment must implement listener")
        }
    }

    private fun setListener() {
        listener = when (inputDialog.parentViewType) {
            ParentViewType.ACTIVITY -> {
                activity as Listener
            }
            ParentViewType.FRAGMENT -> {
                targetFragment as Listener
            }
            null -> throw NullPointerException("parentViewType is null")
        }
    }

    override fun onConfirmClick() {
        listener.onAddConfirmClick(inputDialog.value)
    }

}