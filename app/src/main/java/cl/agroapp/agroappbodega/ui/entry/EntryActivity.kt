package cl.agroapp.agroappbodega.ui.entry

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import cl.agroapp.agroappbodega.R
import cl.agroapp.agroappbodega.data.model.MaterialCategory
import cl.agroapp.agroappbodega.data.model.Warehouse
import cl.agroapp.agroappbodega.ui.dialog.BasicDialogFragment
import cl.agroapp.agroappbodega.utils.Constants
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_entry.*
import javax.inject.Inject

class EntryActivity : DaggerAppCompatActivity() {

    companion object {
        const val REQUEST_CODE = 1

        fun startActivityForResult(fragment: Fragment, requestCode: Int) {
            val intent = Intent(fragment.requireContext(), EntryActivity::class.java)
            fragment.startActivityForResult(intent, requestCode)
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var basicDialog: BasicDialogFragment

    private val entryViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(EntryViewModel::class.java)
    }

    private lateinit var materialAdapter: ArrayAdapter<MaterialCategory>
    private lateinit var warehouseAdapter: ArrayAdapter<Warehouse>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_entry)

        setMaterialSpAdapter()
        setWarehouseSpAdapter()
        setAddEntryFabClickListener()
        setObservers()
    }

    private fun setMaterialSpAdapter() {
        materialAdapter = ArrayAdapter(this, R.layout.list_dropdown_item, arrayListOf())
        material_sp.adapter = materialAdapter
    }

    private fun setWarehouseSpAdapter() {
        warehouseAdapter = ArrayAdapter(this, R.layout.list_dropdown_item, arrayListOf())
        warehouse_sp.adapter = warehouseAdapter
    }

    private fun setAddEntryFabClickListener() {
        add_entry_fab.setOnClickListener {
            newEntry()
        }
    }

    private fun newEntry() {
        entryViewModel.newEntry(
            getSelectedMaterialId(),
            getSelectedWarehouseId(),
            getQuantityText(),
            getObservationText()
        )
    }

    private fun getSelectedMaterialId() =
        material_sp.selectedItem?.let { (it as MaterialCategory).material.id }
            ?: Constants.INVALID_ID

    private fun getSelectedWarehouseId() =
        warehouse_sp.selectedItem?.let { (it as Warehouse).id } ?: Constants.INVALID_ID

    private fun getQuantityText() = quantity_et.text.toString()

    private fun getObservationText() = observation_et.text.toString()

    private fun setObservers() {
        setLoadingObserver()
        setMaterialObserver()
        setWarehouseObserver()
        setNotificationObserver()
        setRepositorySuccessObserver()
        setRepositoryErrorObserver()
    }

    private fun setLoadingObserver() {
        entryViewModel.loadingObservable.observe(this, Observer {
            if (it) {
                loading_pb.visibility = View.VISIBLE
            } else {
                loading_pb.visibility = View.GONE
            }
        })
    }

    private fun setMaterialObserver() {
        entryViewModel.materialListObservable.observe(this, Observer {
            materialAdapter.clear()
            materialAdapter.addAll(it)
        })
    }

    private fun setWarehouseObserver() {
        entryViewModel.warehouseListObservable.observe(this, Observer {
            warehouseAdapter.clear()
            warehouseAdapter.addAll(it)
        })
    }

    private fun setNotificationObserver() {
        entryViewModel.notificationObservable.observe(this, Observer {
            showBasicDialog(getString(it))
        })
    }

    private fun showBasicDialog(message: String) {
        basicDialog.setTitle(getString(R.string.error))
        basicDialog.setMessage(message)
        basicDialog.show(supportFragmentManager, BasicDialogFragment.TAG)
    }

    private fun setRepositorySuccessObserver() {
        entryViewModel.repositorySuccessObservable.observe(this, Observer {
            setResult(Activity.RESULT_OK)
            finish()
        })
    }

    private fun setRepositoryErrorObserver() {
        entryViewModel.repositoryErrorObservable.observe(this, Observer {
            showBasicDialog(it)
        })
    }

}
