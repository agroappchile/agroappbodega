package cl.agroapp.agroappbodega.ui.dialog

import android.os.Bundle
import androidx.fragment.app.FragmentManager
import cl.agroapp.agroappbodega.data.model.ParentViewType
import javax.inject.Inject

class EditDialogFragment @Inject constructor() : InputDialogFragment() {

    companion object {
        val TAG = EditDialogFragment::class.java.simpleName
    }

    interface Listener {
        fun onEditConfirmClick(value: String, itemId: Int)
    }

    private lateinit var listener: Listener

    fun setItemId(itemId: Int) {
        inputDialog.itemId = itemId
    }

    fun setValue(value: String) {
        inputDialog.value = value
    }

    override fun show(manager: FragmentManager, tag: String?) {
        trimInputDialogValue()
        validateInputDialog()
        super.show(manager, tag)
    }

    private fun trimInputDialogValue() {
        inputDialog.value = inputDialog.value.trim()
    }

    private fun validateInputDialog() {
        checkIfInputDialogValueIsSet()
        checkIfItemIdIsSet()
    }

    private fun checkIfInputDialogValueIsSet() {
        check(inputDialog.value.isNotEmpty()) {
            "Must set value"
        }
    }

    private fun checkIfItemIdIsSet() {
        check(inputDialog.itemId != 0) {
            "Must set itemId"
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        tryToSetListener()
    }

    private fun tryToSetListener() {
        try {
            setListener()
        } catch (e: ClassCastException) {
            throw ClassCastException("Activity/Fragment must implement listener")
        }
    }

    private fun setListener() {
        listener = when (inputDialog.parentViewType) {
            ParentViewType.ACTIVITY -> {
                activity as Listener
            }
            ParentViewType.FRAGMENT -> {
                targetFragment as Listener
            }
            null -> throw NullPointerException("parentViewType is null")
        }
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        setValueEtTextIfFirstRun(savedInstanceState)
    }

    private fun setValueEtTextIfFirstRun(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            setValueEtText()
        }
    }

    override fun onConfirmClick() {
        listener.onEditConfirmClick(inputDialog.value, inputDialog.itemId)
    }

}