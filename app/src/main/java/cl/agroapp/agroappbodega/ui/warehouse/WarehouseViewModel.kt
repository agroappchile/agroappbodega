package cl.agroapp.agroappbodega.ui.warehouse

import androidx.annotation.StringRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cl.agroapp.agroappbodega.R
import cl.agroapp.agroappbodega.SingleLiveEvent
import cl.agroapp.agroappbodega.data.model.Warehouse
import cl.agroapp.agroappbodega.data.network.model.WarehouseResponse
import cl.agroapp.agroappbodega.data.repository.WarehouseRepository
import cl.agroapp.agroappbodega.utils.NetworkUtils
import javax.inject.Inject

class WarehouseViewModel @Inject constructor(
    private val warehouseRepository: WarehouseRepository,
    private val networkUtils: NetworkUtils
) : ViewModel() {

    private val _warehouseListObservable = MutableLiveData<ArrayList<Warehouse>>()
    private val _loadingObservable = MutableLiveData<Boolean>()
    private val _repositoryErrorObservable = SingleLiveEvent<String>()
    private val _notificationObservable = SingleLiveEvent<Int>()

    val warehouseListObservable: LiveData<ArrayList<Warehouse>>
        get() = _warehouseListObservable
    val loadingObservable: LiveData<Boolean>
        get() = _loadingObservable
    val repositoryErrorObservable: LiveData<String>
        get() = _repositoryErrorObservable
    val notificationObservable: LiveData<Int>
        get() = _notificationObservable

    override fun onCleared() {
        super.onCleared()
        networkUtils.dispose()
    }

    init {
        loadWarehouses()
    }

    private fun loadWarehouses() {
        networkUtils.addDisposable(warehouseRepository.getWarehouses()
            .subscribeOn(networkUtils.getIoScheduler())
            .observeOn(networkUtils.getUiScheduler())
            .doOnSubscribe { setLoadingObservable(true) }
            .doFinally { setLoadingObservable(false) }
            .subscribe(
                { setWarehouseListObservable(it.warehouseList) },
                { setRepositoryErrorObservable(it) }
            ))
    }

    private fun setLoadingObservable(loading: Boolean) {
        _loadingObservable.value = loading
    }

    private fun setWarehouseListObservable(warehouseList: ArrayList<Warehouse>) {
        _warehouseListObservable.value = warehouseList
    }

    private fun setRepositoryErrorObservable(throwable: Throwable) {
        _repositoryErrorObservable.value = networkUtils.getErrorMessage(throwable)
    }

    fun newWarehouse(warehouseName: String) {
        networkUtils.addDisposable(warehouseRepository.postWarehouse(
            Warehouse(name = warehouseName.trim()))
            .subscribeOn(networkUtils.getIoScheduler())
            .observeOn(networkUtils.getUiScheduler())
            .doOnSubscribe { setLoadingObservable(true) }
            .doFinally { setLoadingObservable(false) }
            .subscribe(
                { onRepositorySuccess(it, R.string.warehouse_created) },
                { setRepositoryErrorObservable(it) }
            ))
    }

    private fun onRepositorySuccess(warehouseResponse: WarehouseResponse, @StringRes resId: Int) {
        setNotificationObservable(resId)
        setWarehouseListObservable(warehouseResponse.warehouseList)
    }

    private fun setNotificationObservable(@StringRes resId: Int) {
        _notificationObservable.value = resId
    }

    fun updateWarehouse(warehouseId: Int, warehouseName: String) {
        networkUtils.addDisposable(warehouseRepository.putWarehouse(
            Warehouse(id = warehouseId, name = warehouseName.trim()))
            .subscribeOn(networkUtils.getIoScheduler())
            .observeOn(networkUtils.getUiScheduler())
            .doOnSubscribe { setLoadingObservable(true) }
            .doFinally { setLoadingObservable(false) }
            .subscribe(
                { onRepositorySuccess(it, R.string.warehouse_updated) },
                { setRepositoryErrorObservable(it) }
            ))
    }

    fun deleteWarehouse(warehouse: Warehouse) {
        warehouse.setInactive()
        networkUtils.addDisposable(warehouseRepository.putWarehouse(warehouse)
            .subscribeOn(networkUtils.getIoScheduler())
            .observeOn(networkUtils.getUiScheduler())
            .doOnSubscribe { setLoadingObservable(true) }
            .doFinally { setLoadingObservable(false) }
            .subscribe(
                { onRepositorySuccess(it, R.string.warehouse_deleted) },
                { setRepositoryErrorObservable(it) }
            ))
    }

}