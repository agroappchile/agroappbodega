package cl.agroapp.agroappbodega.ui.category

import android.view.*
import androidx.recyclerview.widget.RecyclerView
import cl.agroapp.agroappbodega.R
import cl.agroapp.agroappbodega.data.model.Category
import kotlinx.android.synthetic.main.adapter_category.view.*
import javax.inject.Inject

class CategoryAdapter @Inject constructor() :
    RecyclerView.Adapter<CategoryAdapter.CategoryHolder>() {

    var onItemClick: ((String) -> (Unit))? = null
    var onEditItemClick: ((Category) -> (Unit))? = null
    var onDeleteItemClick: ((Category) -> (Unit))? = null
    var categoryList = arrayListOf<Category>()
        set (value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.adapter_category, parent, false)
        return CategoryHolder(view)
    }

    override fun onBindViewHolder(holder: CategoryHolder, position: Int) {
        val category = getCategory(position)
        holder.itemView.category_name_tv.text = category.name
        holder.itemView.total_material_tv.text =
            holder.itemView.context.getString(R.string.total_materials, category.materialList.size)
    }

    private fun getCategory(position: Int) = categoryList[position]

    override fun getItemCount() = categoryList.size

    inner class CategoryHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            setItemClickListener()
            setContextMenu()
        }

        private fun setItemClickListener() {
            itemView.setOnClickListener {
                onItemClick?.invoke(getCategory().name)
            }
        }

        private fun getCategory() = categoryList[adapterPosition]

        private fun setContextMenu() {
            itemView.setOnCreateContextMenuListener { contextMenu, _, _ ->
                addEditMenu(contextMenu)
                addDeleteMenu(contextMenu)
            }
        }

        private fun addEditMenu(contextMenu: ContextMenu) {
            val editMenu = contextMenu.add(Menu.NONE, Menu.NONE, 0,
                itemView.context.getString(R.string.edit))
            setEditMenuClickListener(editMenu)
        }

        private fun setEditMenuClickListener(editMenu: MenuItem) {
            editMenu.setOnMenuItemClickListener {
                onEditItemClick?.invoke(getCategory())
                true
            }
        }

        private fun addDeleteMenu(contextMenu: ContextMenu) {
            val deleteMenu = contextMenu.add(Menu.NONE, Menu.NONE, 1,
                itemView.context.getString(R.string.delete))
            setDeleteMenuClickListener(deleteMenu)
        }

        private fun setDeleteMenuClickListener(deleteMenu: MenuItem) {
            deleteMenu.setOnMenuItemClickListener {
                onDeleteItemClick?.invoke(getCategory())
                true
            }
        }
    }

}