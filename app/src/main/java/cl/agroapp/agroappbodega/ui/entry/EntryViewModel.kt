package cl.agroapp.agroappbodega.ui.entry

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cl.agroapp.agroappbodega.R
import cl.agroapp.agroappbodega.SingleLiveEvent
import cl.agroapp.agroappbodega.data.model.*
import cl.agroapp.agroappbodega.data.network.model.CategoryResponse
import cl.agroapp.agroappbodega.data.network.model.CategoryWarehouseResponse
import cl.agroapp.agroappbodega.data.network.model.WarehouseResponse
import cl.agroapp.agroappbodega.data.repository.CategoryRepository
import cl.agroapp.agroappbodega.data.repository.EntryRepository
import cl.agroapp.agroappbodega.data.repository.WarehouseRepository
import cl.agroapp.agroappbodega.utils.MaterialCategoryUtils.Companion.sortByMaterialName
import cl.agroapp.agroappbodega.utils.MaterialCategoryUtils.Companion.transformToMaterialCategory
import cl.agroapp.agroappbodega.utils.NetworkUtils
import cl.agroapp.agroappbodega.validator.MovementValidator.Companion.getQuantityValue
import cl.agroapp.agroappbodega.validator.MovementValidator.Companion.isQuantityValid
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import javax.inject.Inject

class EntryViewModel @Inject constructor(
    private val warehouseRepository: WarehouseRepository,
    private val categoryRepository: CategoryRepository,
    private val entryRepository: EntryRepository,
    private val networkUtils: NetworkUtils
) : ViewModel() {

    private val _warehouseListObservable = MutableLiveData<ArrayList<Warehouse>>()
    private val _materialListObservable = MutableLiveData<ArrayList<MaterialCategory>>()
    private val _loadingObservable = MutableLiveData<Boolean>()
    private val _repositoryErrorObservable = SingleLiveEvent<String>()
    private val _notificationObservable = SingleLiveEvent<Int>()
    private val _repositorySuccessObservable = SingleLiveEvent<Boolean>()

    val warehouseListObservable: LiveData<ArrayList<Warehouse>>
        get() = _warehouseListObservable
    val materialListObservable: LiveData<ArrayList<MaterialCategory>>
        get() = _materialListObservable
    val loadingObservable: LiveData<Boolean>
        get() = _loadingObservable
    val repositoryErrorObservable: LiveData<String>
        get() = _repositoryErrorObservable
    val notificationObservable: LiveData<Int>
        get() = _notificationObservable
    val repositorySuccessObservable: LiveData<Boolean>
        get() = _repositorySuccessObservable

    override fun onCleared() {
        super.onCleared()
        networkUtils.dispose()
    }

    init {
        loadWarehousesAndMaterials()
    }

    private fun loadWarehousesAndMaterials() {
        networkUtils.addDisposable(Single.zip(
            warehouseRepository.getWarehouses().subscribeOn(networkUtils.getIoScheduler()),
            categoryRepository.getCategories().subscribeOn(networkUtils.getIoScheduler()),
            BiFunction {
                    warehouseResponse: WarehouseResponse,
                    categoryResponse: CategoryResponse ->
                return@BiFunction CategoryWarehouseResponse(categoryResponse, warehouseResponse)
            })
            .observeOn(networkUtils.getUiScheduler())
            .doOnSubscribe { setLoadingObservable(true) }
            .doFinally { setLoadingObservable(false) }
            .subscribe(
                { setWarehouseAndMaterialObservables(it.warehouseResponse, it.categoryResponse)},
                { setRepositoryErrorObservable(it) }
            ))
    }

    private fun setLoadingObservable(loading: Boolean) {
        _loadingObservable.value = loading
    }

    private fun setWarehouseAndMaterialObservables(warehouseResponse: WarehouseResponse,
                                                   categoryResponse: CategoryResponse) {
        setMaterialListObservable(transformAndSort(categoryResponse.categoryList))
        setWarehouseListObservable(warehouseResponse.warehouseList)
    }

    private fun transformAndSort(categoryList: ArrayList<Category>): ArrayList<MaterialCategory> {
        val materialCategoryList = transformToMaterialCategory(categoryList)
        sortByMaterialName(materialCategoryList)
        return materialCategoryList
    }

    private fun setMaterialListObservable(materialCategoryList: ArrayList<MaterialCategory>) {
        _materialListObservable.value = materialCategoryList
    }

    private fun setWarehouseListObservable(warehouseList: ArrayList<Warehouse>) {
        _warehouseListObservable.value = warehouseList
    }

    private fun setRepositoryErrorObservable(throwable: Throwable) {
        _repositoryErrorObservable.value = networkUtils.getErrorMessage(throwable)
    }

    fun newEntry(materialId: Int, warehouseId: Int, quantity: String, observation: String) {
        if (isQuantityValid(quantity)) {
            postEntry(createEntry(materialId, warehouseId, getQuantityValue(quantity), observation))
        } else {
            setNotificationObservable()
        }
    }

    private fun createEntry(materialId: Int, warehouseId: Int, quantity: Double,
                            observation: String): Entry {
        val entry = Entry()
        entry.material = Material(id = materialId)
        entry.warehouse = Warehouse(id = warehouseId)
        entry.quantity = quantity
        entry.observation = observation.trim()
        return entry
    }

    private fun postEntry(entry: Entry) {
        networkUtils.addDisposable(entryRepository.postEntry(entry)
            .subscribeOn(networkUtils.getIoScheduler())
            .observeOn(networkUtils.getUiScheduler())
            .doOnSubscribe { setLoadingObservable(true) }
            .doFinally { setLoadingObservable(false) }
            .subscribe(
                { setRepositorySuccessObservable() },
                { setRepositoryErrorObservable(it) }
            ))
    }

    private fun setRepositorySuccessObservable() {
        _repositorySuccessObservable.value = true
    }

    private fun setNotificationObservable() {
        _notificationObservable.value = R.string.invalid_quantity
    }

}