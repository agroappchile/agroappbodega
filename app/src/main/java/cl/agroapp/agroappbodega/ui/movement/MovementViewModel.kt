package cl.agroapp.agroappbodega.ui.movement

import android.app.Activity.RESULT_OK
import androidx.annotation.StringRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cl.agroapp.agroappbodega.R
import cl.agroapp.agroappbodega.SingleLiveEvent
import cl.agroapp.agroappbodega.data.model.Movement
import cl.agroapp.agroappbodega.data.network.model.MovementResponse
import cl.agroapp.agroappbodega.data.repository.MovementRepository
import cl.agroapp.agroappbodega.utils.NetworkUtils
import javax.inject.Inject

class MovementViewModel @Inject constructor(
    private val movementRepository: MovementRepository,
    private val networkUtils: NetworkUtils
) : ViewModel() {

    private val _movementListObservable = MutableLiveData<ArrayList<Movement>>()
    private val _loadingObservable = MutableLiveData<Boolean>()
    private val _repositoryErrorObservable = SingleLiveEvent<String>()
    private val _notificationObservable = SingleLiveEvent<Int>()

    val movementListObservable: LiveData<ArrayList<Movement>>
        get() = _movementListObservable
    val loadingObservable: LiveData<Boolean>
        get() = _loadingObservable
    val repositoryErrorObservable: LiveData<String>
        get() = _repositoryErrorObservable
    val notificationObservable: LiveData<Int>
        get() = _notificationObservable

    override fun onCleared() {
        super.onCleared()
        networkUtils.dispose()
    }

    init {
        loadMovements()
    }

    private fun loadMovements() {
        networkUtils.addDisposable(movementRepository.getMovements()
            .subscribeOn(networkUtils.getIoScheduler())
            .observeOn(networkUtils.getUiScheduler())
            .doOnSubscribe { setLoadingObservable(true) }
            .doFinally { setLoadingObservable(false) }
            .subscribe(
                { setMovementListObservable(it.movementList) },
                { setRepositoryErrorObservable(it) }
            ))
    }

    private fun setLoadingObservable(loading: Boolean) {
        _loadingObservable.value = loading
    }

    private fun setMovementListObservable(movementList: ArrayList<Movement>) {
        _movementListObservable.value = movementList
    }

    private fun setRepositoryErrorObservable(throwable: Throwable) {
        _repositoryErrorObservable.value = networkUtils.getErrorMessage(throwable)
    }

    fun checkIfMovementWasCreated(resultCode: Int) {
        if (resultCode == RESULT_OK) {
            setNotificationObservable(R.string.movement_created)
            loadMovements()
        }
    }

    private fun setNotificationObservable(@StringRes resId: Int) {
        _notificationObservable.value = resId
    }

    fun deleteMovement(movement: Movement) {
        networkUtils.addDisposable(movementRepository.deleteMovement(movement)
            .subscribeOn(networkUtils.getIoScheduler())
            .observeOn(networkUtils.getUiScheduler())
            .doOnSubscribe { setLoadingObservable(true) }
            .doFinally { setLoadingObservable(false) }
            .subscribe(
                { onDeleteMovementSuccess(it) },
                { setRepositoryErrorObservable(it) }
            ))
    }

    private fun onDeleteMovementSuccess(movementResponse: MovementResponse) {
        setMovementListObservable(movementResponse.movementList)
        setNotificationObservable(R.string.movement_deleted)
    }

}