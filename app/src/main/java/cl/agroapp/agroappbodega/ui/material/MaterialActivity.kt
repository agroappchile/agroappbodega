package cl.agroapp.agroappbodega.ui.material

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import cl.agroapp.agroappbodega.R
import cl.agroapp.agroappbodega.data.model.Material
import cl.agroapp.agroappbodega.ui.dialog.BasicDialogFragment
import cl.agroapp.agroappbodega.ui.dialog.EditDialogFragment
import cl.agroapp.agroappbodega.ui.dialog.MaterialDialogFragment
import cl.agroapp.agroappbodega.utils.Constants
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_material.*
import javax.inject.Inject

class MaterialActivity : DaggerAppCompatActivity(), MaterialDialogFragment.Listener,
    EditDialogFragment.Listener {

    companion object {
        fun startActivity(context: Context, categoryName: String) {
            val intent = Intent(context, MaterialActivity::class.java)
            intent.putExtra(Constants.CATEGORY_NAME, categoryName)
            context.startActivity(intent)
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var materialAdapter: MaterialAdapter
    @Inject
    lateinit var basicDialog: BasicDialogFragment
    @Inject
    lateinit var materialDialog: MaterialDialogFragment
    @Inject
    lateinit var editDialog: EditDialogFragment

    private val materialViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(MaterialViewModel::class.java)
    }

    private val searchViewTextListener = object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String): Boolean {
            return false
        }
        override fun onQueryTextChange(query: String): Boolean {
            materialViewModel.filterMaterialList(query)
            return true
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_material)

        setMaterialRv()
        setListeners()
        setObservers()
        setSearchViewQuery()
    }

    private fun setMaterialRv() {
        material_rv.layoutManager = LinearLayoutManager(this)
        material_rv.setHasFixedSize(true)
        material_rv.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        material_rv.adapter = materialAdapter
    }

    private fun setListeners() {
        setAddMaterialFabClickListener()
        setMaterialAdapterEditClick()
        setMaterialAdapterDeleteClick()
        setSearchViewTextListener()
    }

    private fun setAddMaterialFabClickListener() {
        add_material_fab.setOnClickListener {
            showMaterialDialog()
        }
    }

    private fun showMaterialDialog() {
        materialDialog.categoryList = materialViewModel.categoryList
        materialDialog.show(supportFragmentManager, MaterialDialogFragment.TAG)
    }

    private fun setMaterialAdapterEditClick() {
        materialAdapter.onEditItemClick = {
            showEditDialog(it)
        }
    }

    private fun showEditDialog(material: Material) {
        editDialog.setTitle(getString(R.string.edit))
        editDialog.setMessage(getString(R.string.enter_material_name))
        editDialog.setInputMaxLength(Constants.MAX_MATERIAL_LENGTH)
        editDialog.setParentViewType(this)
        editDialog.setItemId(material.id)
        editDialog.setValue(material.name)
        editDialog.show(supportFragmentManager, EditDialogFragment.TAG)
    }

    private fun setMaterialAdapterDeleteClick() {
        materialAdapter.onDeleteItemClick = {
            materialViewModel.deleteMaterial(it)
        }
    }

    private fun setSearchViewTextListener() {
        search_view.setOnQueryTextListener(searchViewTextListener)
    }

    private fun setObservers() {
        setLoadingObserver()
        setMaterialCategoryListObserver()
        setNotificationObserver()
        setRepositoryErrorObserver()
        setRepositorySuccessObserver()
    }

    private fun setLoadingObserver() {
        materialViewModel.loadingObservable.observe(this, Observer {
            if (it) {
                loading_pb.visibility = View.VISIBLE
            } else {
                loading_pb.visibility = View.GONE
            }
        })
    }

    private fun setMaterialCategoryListObserver() {
        materialViewModel.materialCategoryListObservable.observe(this, Observer {
            materialAdapter.adapterList = it
        })
    }

    private fun setNotificationObserver() {
        materialViewModel.notificationObservable.observe(this, Observer {
            showBasicDialog(getString(R.string.information), getString(it))
        })
    }

    private fun showBasicDialog(title: String, message: String) {
        basicDialog.setTitle(title)
        basicDialog.setMessage(message)
        basicDialog.show(supportFragmentManager, BasicDialogFragment.TAG)
    }

    private fun setRepositoryErrorObserver() {
        materialViewModel.repositoryErrorObservable.observe(this, Observer {
            showBasicDialog(getString(R.string.error), it)
        })
    }

    private fun setRepositorySuccessObserver() {
        materialViewModel.repositorySuccessObservable.observe(this, Observer {
            submitQuery()
        })
    }

    private fun submitQuery() {
        searchViewTextListener.onQueryTextChange(search_view.query.toString())
    }

    private fun setSearchViewQuery() {
        search_view.setQuery(intent.getStringExtra(Constants.CATEGORY_NAME), false)
    }

    override fun onConfirmClick(categoryId: Int, materialName: String) {
        materialViewModel.newMaterial(categoryId, materialName)
    }

    override fun onEditConfirmClick(value: String, itemId: Int) {
        materialViewModel.updateMaterial(materialId = itemId, materialName = value)
    }

}
