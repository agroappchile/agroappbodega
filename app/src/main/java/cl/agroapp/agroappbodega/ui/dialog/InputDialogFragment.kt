package cl.agroapp.agroappbodega.ui.dialog

import android.app.Activity
import android.os.Bundle
import android.text.InputFilter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import cl.agroapp.agroappbodega.R
import cl.agroapp.agroappbodega.data.model.InputDialog
import cl.agroapp.agroappbodega.data.model.ParentViewType
import cl.agroapp.agroappbodega.utils.Constants
import kotlinx.android.synthetic.main.dialog_input.*
import kotlinx.android.synthetic.main.dialog_input.view.*

abstract class InputDialogFragment : DialogFragment() {

    protected var inputDialog = InputDialog()
    private val inputDialogKey = "input_dialog_key"

    fun setTitle(title: String) {
        inputDialog.title = title
    }

    fun setMessage(message: String) {
        inputDialog.message = message
    }

    fun setInputMaxLength(maxLength: Int) {
        inputDialog.inputMaxLength = maxLength
    }

    fun setParentViewType(view: Any) {
        when (view) {
            is Activity -> {
                setParentViewType(ParentViewType.ACTIVITY)
            }
            is Fragment -> {
                setParentViewType(ParentViewType.FRAGMENT)
                setTargetFragment(view, 1)
            }
            else -> {
                throw IllegalArgumentException("Invalid view")
            }
        }
    }

    private fun setParentViewType(parentViewType: ParentViewType) {
        inputDialog.parentViewType = parentViewType
    }

    override fun show(manager: FragmentManager, tag: String?) {
        setArguments()
        super.show(manager, tag)
    }

    private fun setArguments() {
        trimInputDialogValues()
        validateInputDialog()
        val bundle = Bundle()
        bundle.putParcelable(inputDialogKey, inputDialog)
        arguments = bundle
    }

    private fun trimInputDialogValues() {
        inputDialog.title = inputDialog.title.trim()
        inputDialog.message = inputDialog.message.trim()
    }

    private fun validateInputDialog() {
        checkIfTitleIsSet()
        checkIfMessageIsSet()
        checkIfInputMaxLengthIsSet()
        checkIfParentViewTypeIsSet()
    }

    private fun checkIfTitleIsSet() {
        check(inputDialog.title.isNotEmpty()) {
            "Must set title"
        }
    }

    private fun checkIfMessageIsSet() {
        check(inputDialog.message.isNotEmpty()) {
            "Must set message"
        }
    }

    private fun checkIfInputMaxLengthIsSet() {
        check(inputDialog.inputMaxLength > 0) {
            "Must set inputMaxLength"
        }
    }

    private fun checkIfParentViewTypeIsSet() {
        check(inputDialog.parentViewType != null) {
            "Must set parentViewType"
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        restoreInputDialog()
    }

    private fun restoreInputDialog() {
        inputDialog = arguments?.getParcelable<InputDialog>(inputDialogKey)?.let { it }
            ?: throw NullPointerException("Data in arguments is null")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.dialog_input, container, false)
        setTitle(view)
        setMessage(view)
        setInputMaxLength(view)
        setOkBtnClickListener(view)
        setCancelBtnClickListener(view)
        return view
    }

    private fun setTitle(view: View) {
        view.title_tv.text = inputDialog.title
    }

    private fun setMessage(view: View) {
        view.message_tv.text = inputDialog.message
    }

    private fun setInputMaxLength(view: View) {
        view.value_et.filters += InputFilter.LengthFilter(inputDialog.inputMaxLength)
    }

    private fun setOkBtnClickListener(view: View) {
        view.ok_btn.setOnClickListener {
            setInputDialogValue()
            onConfirmClick()
            clearValueEt()
            dismiss()
        }
    }

    private fun setInputDialogValue() {
        inputDialog.value = value_et.text.toString().trim()
    }

    protected abstract fun onConfirmClick()

    private fun clearValueEt() {
        value_et.setText(Constants.EMPTY_STRING)
    }

    private fun setCancelBtnClickListener(view: View) {
        view.cancel_btn.setOnClickListener {
            clearValueEt()
            dismiss()
        }
    }

    protected fun setValueEtText() {
        value_et.setText(inputDialog.value)
    }

}