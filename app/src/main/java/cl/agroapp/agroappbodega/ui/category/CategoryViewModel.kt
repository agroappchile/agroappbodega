package cl.agroapp.agroappbodega.ui.category

import androidx.annotation.StringRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cl.agroapp.agroappbodega.R
import cl.agroapp.agroappbodega.SingleLiveEvent
import cl.agroapp.agroappbodega.data.model.Category
import cl.agroapp.agroappbodega.data.network.model.CategoryResponse
import cl.agroapp.agroappbodega.data.repository.CategoryRepository
import cl.agroapp.agroappbodega.utils.NetworkUtils
import javax.inject.Inject

class CategoryViewModel @Inject constructor(
    private val categoryRepository: CategoryRepository,
    private val networkUtils: NetworkUtils
) : ViewModel() {

    private val _categoryListObservable = MutableLiveData<ArrayList<Category>>()
    private val _loadingObservable = MutableLiveData<Boolean>()
    private val _repositoryErrorObservable = SingleLiveEvent<String>()
    private val _notificationObservable = SingleLiveEvent<Int>()

    val categoryListObservable: LiveData<ArrayList<Category>>
        get() = _categoryListObservable
    val loadingObservable: LiveData<Boolean>
        get() = _loadingObservable
    val repositoryErrorObservable: LiveData<String>
        get() = _repositoryErrorObservable
    val notificationObservable: LiveData<Int>
        get() = _notificationObservable

    override fun onCleared() {
        super.onCleared()
        networkUtils.dispose()
    }

    init {
        loadCategories()
    }

    private fun loadCategories() {
        networkUtils.addDisposable(categoryRepository.getCategories()
            .subscribeOn(networkUtils.getIoScheduler())
            .observeOn(networkUtils.getUiScheduler())
            .doOnSubscribe { setLoadingObservable(true) }
            .doFinally { setLoadingObservable(false) }
            .subscribe(
                { setCategoryListObservable(it.categoryList) },
                { setRepositoryErrorObservable(it) }
            ))
    }

    private fun setLoadingObservable(loading: Boolean) {
        _loadingObservable.value = loading
    }

    private fun setCategoryListObservable(categoryList: ArrayList<Category>) {
        _categoryListObservable.value = categoryList
    }

    private fun setRepositoryErrorObservable(throwable: Throwable) {
        _repositoryErrorObservable.value = networkUtils.getErrorMessage(throwable)
    }

    fun newCategory(categoryName: String) {
        networkUtils.addDisposable(categoryRepository
            .postCategory(Category(name = categoryName.trim()))
            .subscribeOn(networkUtils.getIoScheduler())
            .observeOn(networkUtils.getUiScheduler())
            .doOnSubscribe { setLoadingObservable(true) }
            .doFinally { setLoadingObservable(false) }
            .subscribe(
                { onRepositorySuccess(it, R.string.category_created) },
                { setRepositoryErrorObservable(it) }
            ))
    }

    private fun onRepositorySuccess(categoryResponse: CategoryResponse, @StringRes resId: Int) {
        setNotificationObservable(resId)
        setCategoryListObservable(categoryResponse.categoryList)
    }

    private fun setNotificationObservable(@StringRes resId: Int) {
        _notificationObservable.value = resId
    }

    fun updateCategory(categoryId: Int, categoryName: String) {
        networkUtils.addDisposable(categoryRepository
            .putCategory(Category(categoryId, categoryName.trim()))
            .subscribeOn(networkUtils.getIoScheduler())
            .observeOn(networkUtils.getUiScheduler())
            .doOnSubscribe { setLoadingObservable(true) }
            .doFinally { setLoadingObservable(false) }
            .subscribe(
                { onRepositorySuccess(it, R.string.category_updated) },
                { setRepositoryErrorObservable(it) }
            ))
    }

    fun deleteCategory(category: Category) {
        category.setInactive()
        networkUtils.addDisposable(categoryRepository.putCategory(category)
            .subscribeOn(networkUtils.getIoScheduler())
            .observeOn(networkUtils.getUiScheduler())
            .doOnSubscribe { setLoadingObservable(true) }
            .doFinally { setLoadingObservable(false) }
            .subscribe(
                { onRepositorySuccess(it, R.string.category_deleted) },
                { setRepositoryErrorObservable(it) }
            ))
    }

}