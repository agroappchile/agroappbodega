package cl.agroapp.agroappbodega.ui.warehouse

import android.view.*
import androidx.recyclerview.widget.RecyclerView
import cl.agroapp.agroappbodega.R
import cl.agroapp.agroappbodega.data.model.Warehouse
import kotlinx.android.synthetic.main.adapter_warehouse.view.*
import javax.inject.Inject

class WarehouseAdapter @Inject constructor() :
    RecyclerView.Adapter<WarehouseAdapter.WarehouseHolder>() {

    var onEditItemClick: ((Warehouse) -> (Unit))? = null
    var onDeleteItemClick: ((Warehouse) -> (Unit))? = null
    var warehouseList = arrayListOf<Warehouse>()
        set (value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WarehouseHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.adapter_warehouse, parent, false)
        return WarehouseHolder(view)
    }

    override fun onBindViewHolder(holder: WarehouseHolder, position: Int) {
        holder.itemView.warehouse_name_tv.text = warehouseList[position].name
    }

    override fun getItemCount() = warehouseList.size

    inner class WarehouseHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            setContextMenu()
        }

        private fun setContextMenu() {
            itemView.setOnCreateContextMenuListener { contextMenu, _, _ ->
                addEditMenu(contextMenu)
                addDeleteMenu(contextMenu)
            }
        }

        private fun addEditMenu(contextMenu: ContextMenu) {
            val editMenu = contextMenu.add(Menu.NONE, Menu.NONE, 0,
                itemView.context.getString(R.string.edit))
            setEditMenuClickListener(editMenu)
        }

        private fun setEditMenuClickListener(editMenu: MenuItem) {
            editMenu.setOnMenuItemClickListener {
                onEditItemClick?.invoke(getWarehouse())
                true
            }
        }

        private fun getWarehouse() = warehouseList[adapterPosition]

        private fun addDeleteMenu(contextMenu: ContextMenu) {
            val deleteMenu = contextMenu.add(Menu.NONE, Menu.NONE, 1,
                itemView.context.getString(R.string.delete))
            setDeleteMenuClickListener(deleteMenu)
        }

        private fun setDeleteMenuClickListener(deleteMenu: MenuItem) {
            deleteMenu.setOnMenuItemClickListener {
                onDeleteItemClick?.invoke(getWarehouse())
                true
            }
        }
    }

}