package cl.agroapp.agroappbodega.data.repository

import cl.agroapp.agroappbodega.data.network.service.StockService
import javax.inject.Inject

class StockRepository @Inject constructor(private val stockService: StockService) {

    fun getStock() = stockService.getStock()

}