package cl.agroapp.agroappbodega.data.network.interceptor

import cl.agroapp.agroappbodega.data.model.User
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AuthInterceptor @Inject constructor() : Interceptor {

    private lateinit var user: User

    fun setCredentials(user: User) {
        this.user = user
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        val requestBuilder = request.newBuilder()
            //.addHeader("Connection", "close")
            //.addHeader("Accept", "application/json")
            //.addHeader("Content-Type", "application/json")
        if (request.header("No-Authentication") == null) {
            requestBuilder.addHeader("usuario", user.user)
                .addHeader("clave", user.password)
        }
        request = requestBuilder.build()
        return chain.proceed(request)
    }

}