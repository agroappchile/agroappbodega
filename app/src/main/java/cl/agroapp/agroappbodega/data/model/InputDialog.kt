package cl.agroapp.agroappbodega.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class InputDialog(
    var title: String = "",
    var message: String = "",
    var value: String = "",
    var itemId: Int = 0,
    var inputMaxLength: Int = 0,
    var parentViewType: ParentViewType? = null
) : Parcelable