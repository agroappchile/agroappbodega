package cl.agroapp.agroappbodega.data.network.model

import cl.agroapp.agroappbodega.data.model.Stock
import com.google.gson.annotations.SerializedName

data class StockResponse(
    @SerializedName("results")
    val stockList: ArrayList<Stock>
)