package cl.agroapp.agroappbodega.data.model

import cl.agroapp.agroappbodega.utils.Constants
import com.google.gson.annotations.SerializedName

data class Warehouse(
    @SerializedName("almacen_id")
    val id: Int = 0,
    @SerializedName("name")
    val name: String = "",
    @SerializedName("isactive")
    var active: String = Constants.ACTIVE
) {

    fun setInactive() {
        active = Constants.INACTIVE
    }

    override fun toString() = name

}