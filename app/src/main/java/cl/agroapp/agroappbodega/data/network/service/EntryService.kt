package cl.agroapp.agroappbodega.data.network.service

import cl.agroapp.agroappbodega.data.model.Entry
import io.reactivex.Completable
import retrofit2.http.Body
import retrofit2.http.POST

interface EntryService {

    @POST("Ingreso")
    fun postEntry(@Body entry: Entry): Completable

}