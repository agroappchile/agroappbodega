package cl.agroapp.agroappbodega.data.model

data class MaterialCategory(
    val category: Category,
    val material: Material
) {

    override fun toString() = material.name

}