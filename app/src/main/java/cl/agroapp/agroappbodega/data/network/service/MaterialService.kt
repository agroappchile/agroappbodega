package cl.agroapp.agroappbodega.data.network.service

import cl.agroapp.agroappbodega.data.model.Category
import cl.agroapp.agroappbodega.data.model.Material
import cl.agroapp.agroappbodega.data.network.model.CategoryResponse
import cl.agroapp.agroappbodega.utils.Constants
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.PUT

interface MaterialService {

    @POST(Constants.MATERIAL)
    fun postMaterial(@Body category: Category): Single<CategoryResponse>

    @PUT(Constants.MATERIAL)
    fun putMaterial(@Body material: Material): Single<CategoryResponse>

}