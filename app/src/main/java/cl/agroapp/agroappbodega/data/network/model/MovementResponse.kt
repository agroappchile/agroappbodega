package cl.agroapp.agroappbodega.data.network.model

import cl.agroapp.agroappbodega.data.model.Movement
import com.google.gson.annotations.SerializedName

data class MovementResponse(
    @SerializedName("results")
    val movementList: ArrayList<Movement>
)