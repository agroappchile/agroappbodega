package cl.agroapp.agroappbodega.data.network.service

import cl.agroapp.agroappbodega.data.model.Movement
import cl.agroapp.agroappbodega.data.network.model.MovementResponse
import cl.agroapp.agroappbodega.utils.Constants
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.PUT

interface MovementService {

    @GET(Constants.MOVEMENT)
    fun getMovements(): Single<MovementResponse>

    @PUT(Constants.MOVEMENT)
    fun deleteMovement(@Body movement: Movement): Single<MovementResponse>

}