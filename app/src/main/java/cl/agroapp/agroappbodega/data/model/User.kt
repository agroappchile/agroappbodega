package cl.agroapp.agroappbodega.data.model

import com.google.gson.annotations.SerializedName

data class User (
    @SerializedName("usuario")
    val user: String,
    @SerializedName("clave")
    val password: String,
    @SerializedName("nombre")
    val name: String = ""
)