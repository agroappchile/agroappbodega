package cl.agroapp.agroappbodega.data.model

import cl.agroapp.agroappbodega.utils.Constants
import com.google.gson.annotations.SerializedName

data class Stock(
    @SerializedName("stock_id")
    val id: Int = 0,
    @SerializedName("isactive")
    val active: String = Constants.ACTIVE,
    @SerializedName("stock")
    val stock: Double = 0.0,
    @SerializedName("almacen")
    val warehouse: Warehouse = Warehouse(),
    @SerializedName("material")
    val material: Material = Material()
)