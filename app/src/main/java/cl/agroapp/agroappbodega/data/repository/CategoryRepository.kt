package cl.agroapp.agroappbodega.data.repository

import cl.agroapp.agroappbodega.data.model.Category
import cl.agroapp.agroappbodega.data.network.service.CategoryService
import javax.inject.Inject

class CategoryRepository @Inject constructor(private val categoryService: CategoryService) {

    fun getCategories() = categoryService.getCategories()

    fun postCategory(category: Category) = categoryService.postCategory(category)

    fun putCategory(category: Category) = categoryService.putCategory(category)

}