package cl.agroapp.agroappbodega.data.repository

import cl.agroapp.agroappbodega.data.model.Entry
import cl.agroapp.agroappbodega.data.network.service.EntryService
import javax.inject.Inject

class EntryRepository @Inject constructor(private val entryService: EntryService) {

    fun postEntry(entry: Entry) = entryService.postEntry(entry)

}