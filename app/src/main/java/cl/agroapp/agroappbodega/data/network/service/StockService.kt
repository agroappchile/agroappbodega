package cl.agroapp.agroappbodega.data.network.service

import cl.agroapp.agroappbodega.data.network.model.StockResponse
import io.reactivex.Single
import retrofit2.http.GET

interface StockService {

    @GET("Stock")
    fun getStock(): Single<StockResponse>

}