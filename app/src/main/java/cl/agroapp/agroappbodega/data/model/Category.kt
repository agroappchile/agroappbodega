package cl.agroapp.agroappbodega.data.model

import android.os.Parcelable
import cl.agroapp.agroappbodega.utils.Constants
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Category(
    @SerializedName("categoria_id")
    val id: Int = 0,
    @SerializedName("name")
    val name: String = "",
    @SerializedName("isactive")
    var active: String = Constants.ACTIVE,
    @SerializedName("material_list")
    val materialList: ArrayList<Material> = ArrayList()
) : Parcelable {

    fun setInactive() {
        active = Constants.INACTIVE
    }

    override fun toString() = name

}