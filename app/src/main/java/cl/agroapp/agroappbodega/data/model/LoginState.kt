package cl.agroapp.agroappbodega.data.model

import androidx.annotation.StringRes

data class LoginState(
    val loading: Boolean,
    @StringRes val message: Int
)