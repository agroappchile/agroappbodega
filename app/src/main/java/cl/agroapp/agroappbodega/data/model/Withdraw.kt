package cl.agroapp.agroappbodega.data.model

import android.content.Context
import cl.agroapp.agroappbodega.R
import cl.agroapp.agroappbodega.utils.Constants
import com.google.gson.annotations.SerializedName

data class Withdraw(
    @SerializedName("salida_id")
    val id: Int = 0,
    @SerializedName("isactive")
    val active: String = Constants.ACTIVE,
    @SerializedName("descripcion")
    val observation: String = "",
    @SerializedName("almacen")
    val warehouse: Warehouse = Warehouse()
) : Movement() {

    override fun getSummary(context: Context) = "Withdraw summary"

    override fun getMovementType(context: Context) = context.getString(R.string.title_withdraw)

}