package cl.agroapp.agroappbodega.data.model

import android.content.Context
import cl.agroapp.agroappbodega.R
import com.google.gson.annotations.SerializedName

data class Entry(
    @SerializedName("ingreso_id")
    val id: Int = 0,
    @SerializedName("descripcion")
    var observation: String = "",
    @SerializedName("almacen")
    var warehouse: Warehouse = Warehouse()
) : Movement() {

    override fun getSummary(context: Context) =
        context.getString(R.string.title_warehouse) + ": ${warehouse.name}. " +
        context.getString(R.string.title_material) + ": ${material.name}. " +
        context.getString(R.string.quantity) + ": $quantity. " +
        context.getString(R.string.observation) + ": $observation. " +
        context.getString(R.string.user) + ": $createdBy"

    override fun getMovementType(context: Context) = context.getString(R.string.title_entry)

}