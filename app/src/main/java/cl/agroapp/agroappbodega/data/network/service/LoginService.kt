package cl.agroapp.agroappbodega.data.network.service

import cl.agroapp.agroappbodega.data.model.User
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface LoginService {

    @POST("Usuarios")
    @Headers("No-Authentication: true")
    fun login(@Body user: User): Single<User>

}