package cl.agroapp.agroappbodega.data.model

import android.content.Context
import cl.agroapp.agroappbodega.R
import cl.agroapp.agroappbodega.utils.Constants
import com.google.gson.annotations.SerializedName

data class Transfer(
    @SerializedName("traslado_id")
    val id: Int = 0,
    @SerializedName("isactive")
    val active: String = Constants.ACTIVE,
    @SerializedName("descripcion")
    val observation: String = "",
    @SerializedName("almacen_origen")
    val sourceWarehouse: Warehouse = Warehouse(),
    @SerializedName("almacen_destino")
    val destinationWarehouse: Warehouse = Warehouse()
) : Movement() {

    override fun getSummary(context: Context) = "Transfer summary"

    override fun getMovementType(context: Context) = context.getString(R.string.title_transfer)

}