package cl.agroapp.agroappbodega.data.repository

import cl.agroapp.agroappbodega.data.model.Category
import cl.agroapp.agroappbodega.data.model.Material
import cl.agroapp.agroappbodega.data.network.service.MaterialService
import javax.inject.Inject

class MaterialRepository @Inject constructor(private val materialService: MaterialService) {

    fun postMaterial(category: Category) = materialService.postMaterial(category)

    fun putMaterial(material: Material) = materialService.putMaterial(material)

}