package cl.agroapp.agroappbodega.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BasicDialog(
    var title: String = "",
    var message: String = ""
) : Parcelable