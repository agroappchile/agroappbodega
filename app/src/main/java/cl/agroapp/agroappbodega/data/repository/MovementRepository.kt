package cl.agroapp.agroappbodega.data.repository

import cl.agroapp.agroappbodega.data.model.Movement
import cl.agroapp.agroappbodega.data.network.service.MovementService
import javax.inject.Inject

class MovementRepository @Inject constructor(private val movementService: MovementService) {

    fun getMovements() = movementService.getMovements()

    fun deleteMovement(movement: Movement) = movementService.deleteMovement(movement)

}