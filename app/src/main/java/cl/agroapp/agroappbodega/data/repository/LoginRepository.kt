package cl.agroapp.agroappbodega.data.repository

import cl.agroapp.agroappbodega.data.model.User
import cl.agroapp.agroappbodega.data.network.service.LoginService
import javax.inject.Inject

class LoginRepository @Inject constructor(private val loginService: LoginService) {

    fun login(user: User) = loginService.login(user)

}