package cl.agroapp.agroappbodega.data.network.model

import cl.agroapp.agroappbodega.data.model.Category
import com.google.gson.annotations.SerializedName

data class CategoryResponse (
    @SerializedName("results")
    val categoryList: ArrayList<Category>
)