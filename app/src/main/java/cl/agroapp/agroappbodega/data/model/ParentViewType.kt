package cl.agroapp.agroappbodega.data.model

enum class ParentViewType {
    ACTIVITY, FRAGMENT
}