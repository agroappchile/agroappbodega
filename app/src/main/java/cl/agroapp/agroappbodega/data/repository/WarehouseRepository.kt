package cl.agroapp.agroappbodega.data.repository

import cl.agroapp.agroappbodega.data.model.Warehouse
import cl.agroapp.agroappbodega.data.network.service.WarehouseService
import javax.inject.Inject

class WarehouseRepository @Inject constructor(private val warehouseService: WarehouseService) {

    fun getWarehouses() = warehouseService.getWarehouses()

    fun postWarehouse(warehouse: Warehouse) = warehouseService.postWarehouse(warehouse)

    fun putWarehouse(warehouse: Warehouse) = warehouseService.putWarehouse(warehouse)

}