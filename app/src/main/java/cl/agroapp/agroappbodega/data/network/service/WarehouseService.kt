package cl.agroapp.agroappbodega.data.network.service

import cl.agroapp.agroappbodega.data.model.Warehouse
import cl.agroapp.agroappbodega.data.network.model.WarehouseResponse
import cl.agroapp.agroappbodega.utils.Constants
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT

interface WarehouseService {

    @GET(Constants.WAREHOUSE)
    fun getWarehouses(): Single<WarehouseResponse>

    @POST(Constants.WAREHOUSE)
    fun postWarehouse(@Body warehouse: Warehouse): Single<WarehouseResponse>

    @PUT(Constants.WAREHOUSE)
    fun putWarehouse(@Body warehouse: Warehouse): Single<WarehouseResponse>

}