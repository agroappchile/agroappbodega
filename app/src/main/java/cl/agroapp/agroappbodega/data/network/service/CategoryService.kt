package cl.agroapp.agroappbodega.data.network.service

import cl.agroapp.agroappbodega.data.model.Category
import cl.agroapp.agroappbodega.data.network.model.CategoryResponse
import cl.agroapp.agroappbodega.utils.Constants
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT

interface CategoryService {

    @GET(Constants.CATEGORY)
    fun getCategories(): Single<CategoryResponse>

    @POST(Constants.CATEGORY)
    fun postCategory(@Body category: Category): Single<CategoryResponse>

    @PUT(Constants.CATEGORY)
    fun putCategory(@Body category: Category): Single<CategoryResponse>

}