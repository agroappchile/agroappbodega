package cl.agroapp.agroappbodega.data.model

import android.content.Context
import cl.agroapp.agroappbodega.utils.Constants
import com.google.gson.annotations.SerializedName
import java.util.*

abstract class Movement(
    @SerializedName("movimiento_id")
    val movementId: Int = 0,
    @SerializedName("created")
    val created: Date = Date(),
    @SerializedName("createdby")
    val createdBy: String = "",
    @SerializedName("movimiento_isactive")
    val movementActive: String = Constants.ACTIVE,
    @SerializedName("cantidad")
    var quantity: Double = 0.0,
    @SerializedName("material")
    var material: Material = Material()
) {

    abstract fun getSummary(context: Context): String

    abstract fun getMovementType(context: Context): String

}