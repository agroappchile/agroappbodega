package cl.agroapp.agroappbodega.data.network.model

import cl.agroapp.agroappbodega.data.model.Warehouse
import com.google.gson.annotations.SerializedName

data class WarehouseResponse (
    @SerializedName("results")
    val warehouseList: ArrayList<Warehouse>
)