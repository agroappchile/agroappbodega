package cl.agroapp.agroappbodega.data.network.model

data class CategoryWarehouseResponse(
    val categoryResponse: CategoryResponse,
    val warehouseResponse: WarehouseResponse
)