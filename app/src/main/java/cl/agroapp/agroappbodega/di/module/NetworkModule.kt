package cl.agroapp.agroappbodega.di.module

import cl.agroapp.agroappbodega.RuntimeTypeAdapterFactory
import cl.agroapp.agroappbodega.data.model.Entry
import cl.agroapp.agroappbodega.data.model.Movement
import cl.agroapp.agroappbodega.data.model.Transfer
import cl.agroapp.agroappbodega.data.model.Withdraw
import cl.agroapp.agroappbodega.data.network.interceptor.AuthInterceptor
import cl.agroapp.agroappbodega.data.network.service.*
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class NetworkModule {

    @Singleton
    @Provides
    fun provideOkHttpClient(authInterceptor: AuthInterceptor): OkHttpClient =
        OkHttpClient.Builder()
            .addInterceptor(authInterceptor)
            .connectTimeout(5, TimeUnit.SECONDS)
            .readTimeout(5, TimeUnit.SECONDS)
            .build()

    @Singleton
    @Provides
    fun provideRetrofit(baseUrl: String, okHttpClient: OkHttpClient, gson: Gson): Retrofit =
        Retrofit.Builder()
            .baseUrl(baseUrl)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(okHttpClient)
            .build()

    @Singleton
    @Provides
    fun provideGson(): Gson {
        val runtimeTypeAdapterFactory = RuntimeTypeAdapterFactory
            .of(Movement::class.java, "tipo_movimiento")
            .registerSubtype(Entry::class.java, "Ingreso")
            .registerSubtype(Withdraw::class.java, "Salida")
            .registerSubtype(Transfer::class.java, "Traslado")

        return GsonBuilder()
            .registerTypeAdapterFactory(runtimeTypeAdapterFactory)
            .setDateFormat("yyyy-MM-dd HH:mm:ss")
            .create()
    }

    @Provides
    fun provideCompositeDisposable(): CompositeDisposable = CompositeDisposable()

    @Singleton
    @Provides
    fun provideLoginService(retrofit: Retrofit): LoginService =
        retrofit.create(LoginService::class.java)

    @Singleton
    @Provides
    fun provideCategoryService(retrofit: Retrofit): CategoryService =
        retrofit.create(CategoryService::class.java)

    @Singleton
    @Provides
    fun provideMaterialService(retrofit: Retrofit): MaterialService =
        retrofit.create(MaterialService::class.java)

    @Singleton
    @Provides
    fun provideWarehouseService(retrofit: Retrofit): WarehouseService =
        retrofit.create(WarehouseService::class.java)

    @Singleton
    @Provides
    fun provideEntryService(retrofit: Retrofit): EntryService =
        retrofit.create(EntryService::class.java)

    @Singleton
    @Provides
    fun provideMovementService(retrofit: Retrofit): MovementService =
        retrofit.create(MovementService::class.java)

    @Singleton
    @Provides
    fun provideStockService(retrofit: Retrofit): StockService =
        retrofit.create(StockService::class.java)

}