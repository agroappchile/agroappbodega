package cl.agroapp.agroappbodega.di.component

import android.content.Context
import android.content.SharedPreferences
import cl.agroapp.agroappbodega.BaseApplication
import cl.agroapp.agroappbodega.data.network.interceptor.AuthInterceptor
import cl.agroapp.agroappbodega.di.Application
import cl.agroapp.agroappbodega.di.module.ApplicationModule
import cl.agroapp.agroappbodega.di.module.ViewModelModule
import cl.agroapp.agroappbodega.di.module.ViewModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule

@Application
@Component(dependencies = [NetworkComponent::class],
    modules = [
        ViewModelModule::class,
        AndroidSupportInjectionModule::class,
        ViewModule::class,
        ApplicationModule::class
    ])
interface ApplicationComponent: AndroidInjector<BaseApplication> {

    fun getSharedPreferences(): SharedPreferences
    fun getAuthInterceptor(): AuthInterceptor

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun context(context: Context): Builder
        fun networkComponent(networkComponent: NetworkComponent): Builder
        fun build(): ApplicationComponent
    }

}