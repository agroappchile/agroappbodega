package cl.agroapp.agroappbodega.di

import cl.agroapp.agroappbodega.di.component.DaggerNetworkComponent
import cl.agroapp.agroappbodega.di.component.NetworkComponent
import cl.agroapp.agroappbodega.utils.Constants

class NetworkComponentFactory {

    companion object {
        private const val BASE_URL_PROD = "http://agroapp.cl:8080/AgroAppBodega/"
        private const val BASE_URL_TEST = "http://agroapp.cl:8080/AgroAppBodegaTest/"

        fun newProductionNetworkComponent() = getNetworkComponent(BASE_URL_PROD)

        fun newTestingNetworkComponent(): NetworkComponent {
            val networkComponent = getNetworkComponent(BASE_URL_TEST)
            networkComponent.getAuthInterceptor().setCredentials(Constants.VALID_USER_TEST)
            return networkComponent
        }

        private fun getNetworkComponent(baseUrl: String) = DaggerNetworkComponent.builder()
            .baseUrl(baseUrl)
            .build()
    }

}