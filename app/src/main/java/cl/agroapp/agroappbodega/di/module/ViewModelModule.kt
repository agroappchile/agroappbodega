package cl.agroapp.agroappbodega.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import cl.agroapp.agroappbodega.di.ViewModelFactory
import cl.agroapp.agroappbodega.di.ViewModelKey
import cl.agroapp.agroappbodega.ui.category.CategoryViewModel
import cl.agroapp.agroappbodega.ui.entry.EntryViewModel
import cl.agroapp.agroappbodega.ui.login.LoginViewModel
import cl.agroapp.agroappbodega.ui.material.MaterialViewModel
import cl.agroapp.agroappbodega.ui.movement.MovementViewModel
import cl.agroapp.agroappbodega.ui.stock.StockViewModel
import cl.agroapp.agroappbodega.ui.warehouse.WarehouseViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindLoginViewModel(loginViewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CategoryViewModel::class)
    abstract fun bindCategoryViewModel(categoryViewModel: CategoryViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MaterialViewModel::class)
    abstract fun bindMaterialViewModel(materialViewModel: MaterialViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(WarehouseViewModel::class)
    abstract fun bindWarehouseViewModel(warehouseViewModel: WarehouseViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MovementViewModel::class)
    abstract fun bindMovementViewModel(movementViewModel: MovementViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EntryViewModel::class)
    abstract fun bindEntryViewModel(entryViewModel: EntryViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(StockViewModel::class)
    abstract fun bindStockViewModel(stockViewModel: StockViewModel): ViewModel

}