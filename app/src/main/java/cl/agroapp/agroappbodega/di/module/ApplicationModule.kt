package cl.agroapp.agroappbodega.di.module

import android.content.Context
import android.content.SharedPreferences
import cl.agroapp.agroappbodega.utils.Constants
import dagger.Module
import dagger.Provides

@Module
class ApplicationModule {

    @Provides
    fun provideSharedPreferences(context: Context): SharedPreferences =
        context.getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE)

}