package cl.agroapp.agroappbodega.di.component

import cl.agroapp.agroappbodega.BaseSchedulerProvider
import cl.agroapp.agroappbodega.data.network.interceptor.AuthInterceptor
import cl.agroapp.agroappbodega.data.network.service.*
import cl.agroapp.agroappbodega.di.module.NetworkModule
import cl.agroapp.agroappbodega.di.module.SchedulerModule
import dagger.BindsInstance
import dagger.Component
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Singleton

@Singleton
@Component(modules = [
    SchedulerModule::class,
    NetworkModule::class
])
interface NetworkComponent {

    fun getLoginService(): LoginService
    fun getCategoryService(): CategoryService
    fun getMaterialService(): MaterialService
    fun getAuthInterceptor(): AuthInterceptor
    fun getBaseSchedulerProvider(): BaseSchedulerProvider
    fun getCompositeDisposable(): CompositeDisposable
    fun getWarehouseService(): WarehouseService
    fun getEntryService(): EntryService
    fun getMovementService(): MovementService
    fun getStockService(): StockService

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun baseUrl(baseUrl: String): Builder
        fun build(): NetworkComponent
    }

}