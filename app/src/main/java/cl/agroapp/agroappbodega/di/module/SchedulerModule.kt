package cl.agroapp.agroappbodega.di.module

import cl.agroapp.agroappbodega.BaseSchedulerProvider
import cl.agroapp.agroappbodega.SchedulerProvider
import dagger.Binds
import dagger.Module

@Module
abstract class SchedulerModule {

    @Binds
    abstract fun bindSchedulerProvider(schedulerProvider: SchedulerProvider): BaseSchedulerProvider

}