package cl.agroapp.agroappbodega.di.module

import cl.agroapp.agroappbodega.ui.category.CategoryFragment
import cl.agroapp.agroappbodega.ui.entry.EntryActivity
import cl.agroapp.agroappbodega.ui.login.LoginActivity
import cl.agroapp.agroappbodega.ui.material.MaterialActivity
import cl.agroapp.agroappbodega.ui.movement.MovementFragment
import cl.agroapp.agroappbodega.ui.stock.StockFragment
import cl.agroapp.agroappbodega.ui.warehouse.WarehouseFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ViewModule {

    @ContributesAndroidInjector
    abstract fun contributeLoginActivity(): LoginActivity

    @ContributesAndroidInjector
    abstract fun contributeCategoryFragment(): CategoryFragment

    @ContributesAndroidInjector
    abstract fun contributeMaterialActivity(): MaterialActivity

    @ContributesAndroidInjector
    abstract fun contributeWarehouseFragment(): WarehouseFragment

    @ContributesAndroidInjector
    abstract fun contributeMovementFragment(): MovementFragment

    @ContributesAndroidInjector
    abstract fun contributeEntryActivity(): EntryActivity

    @ContributesAndroidInjector
    abstract fun contributeStockFragment(): StockFragment

}