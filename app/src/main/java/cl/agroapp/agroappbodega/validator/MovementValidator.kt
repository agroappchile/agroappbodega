package cl.agroapp.agroappbodega.validator

class MovementValidator {

    companion object {
        fun isQuantityValid(quantity: String): Boolean {
            return try {
                isQuantityValid(quantity.toDouble())
            } catch (e: NumberFormatException) {
                false
            }
        }

        private fun isQuantityValid(quantity: Double) = quantity > 0.0

        fun getQuantityValue(quantity: String) = quantity.toDouble()
    }

}