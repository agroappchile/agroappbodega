package cl.agroapp.agroappbodega

import cl.agroapp.agroappbodega.data.model.User
import cl.agroapp.agroappbodega.di.NetworkComponentFactory
import cl.agroapp.agroappbodega.di.component.ApplicationComponent
import cl.agroapp.agroappbodega.di.component.DaggerApplicationComponent
import cl.agroapp.agroappbodega.utils.Constants
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

open class BaseApplication : DaggerApplication() {

    private lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        setAuthInterceptorCredentials()
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication>? {
        applicationComponent = DaggerApplicationComponent.builder()
            .context(this)
            .networkComponent(getNetworkComponent())
            .build()
        return applicationComponent
    }

    open fun getNetworkComponent() = NetworkComponentFactory.newProductionNetworkComponent()

    private fun setAuthInterceptorCredentials() {
        applicationComponent.getAuthInterceptor().setCredentials(getUserFromSharedPreferences())
    }

    private fun getUserFromSharedPreferences(): User {
        val sharedPrefs = applicationComponent.getSharedPreferences()
        return User(
            sharedPrefs.getString(Constants.SHARED_PREFS_USER, Constants.EMPTY_STRING)
                ?.let { it } ?: Constants.EMPTY_STRING,
            sharedPrefs.getString(Constants.SHARED_PREFS_PASSWORD, Constants.EMPTY_STRING)
                ?.let { it } ?: Constants.EMPTY_STRING
        )
    }

}